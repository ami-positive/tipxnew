/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;

import android.util.Log;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.util.DateUtil;



/**
 * @author natiapplications
 *
 */
public class ChatMessage implements Serializable{
	
	
	
	private static final long serialVersionUID = 1L;
	
	public static final int GROUP_MESSAGE = 1;
	public static final int PRIVATE_MESSAGE = 2;
	
	public static final int TYPE_TEXT = 1;
	public static final int TYPE_LOCATION = 2;
	public static final int TYPE_IMAGE = 3;
	public static final int TYPE_VIDEO = 4;
	public static final int TYPE_AUDIO = 5;
	public static final int TYPE_MESSAGE_ACCEPT_IN_SERVER = 21;
	public static final int TYPE_MESSAGE_ACCEPT_IN_USER = 41;
	public static final int TYPE_MESSAGE_READED = 42;
	public static final int TYPE_CONFIRM_DELETED = 43;
	public static final int TYPE_MESSAGE_DELETED = 23;
	public static final int TYPE_GROUP_DELETED = 24;
	public static final int TYPE_ME_JOIN = 15;
	public static final int TYPE_ME_LEAVE = 16;
	public static final int TYPE_MEMBER_JOIN = 25;
	public static final int TYPE_MEMBER_LEAVE = 26;
	public static final int TYPE_DELET_CONVERSATION = 27;
	public static final int TYPE_DELET_CONTACT = 29;
	public static final int TYPE_PARTNER_START_TYPING = 99;
	public static final int TYPE_PARTNER_STOP_TYPING = 100;
	
	public static final String MEDIA_CODE_IMAGE = "3";
	public static final String MEDIA_CODE_AUDIO = "5";
	public static final String MEDIA_CODE_VIDEO = "4";
	
	public static final int STATUS_SENT = 0;
	public static final int STATUS_RECIVED_IN_SERVER = 2;
	public static final int STATUS_RECIVED_IN_RECIPIENT = 3;
	public static final int STATUS_READED_BY_RECIPIENT = 4;
	public static final int STATUS_DELETED = 5;
	public static final int STATUS_CONFIRM_DELETATION = 6;
	
	public static final int SIDE_LEFT = 1;
	public static final int SIDE_RIGHT = 2;
	public static final int PHAT_FROM = 1;
	public static final int PHAT_TO = 2;
	
	public static final int READ_STATUS_WAIT = 0;
	public static final int READ_STATUS_SENT = 1;
	public static final int READ_STATUS_CANCEL = 2;
	
	public static final int DOWNLOAD_STATUS_WAIT = 0;
	public static final int DOWNLOAD_STATUS_IN_PROGRESS = 1;
	public static final int DOWNLOAD_STATUS_COMPLETED = 2;
	public static final int DOWNLOAD_STATUS_CANCEL = 3;
	
	private int messageId;
	private String tempId;
	private String newMessageId;
	
	
	private String senderID;
	private int recipientId;
	private int groupId;
	
	private int readConfirmationStatus;
	private int downloadStatus;
	private int status;
	private int type;
	private int side;
	private int chatType;
	private int messagePhat;
	
	
	
	private String userName;
	private String senderPhone;
	
	private String created;
	private long dateInMillis;
	
	
	
	private boolean autoDelete;
	private boolean fileLoaded;
	private boolean uploudFileCanceled;
	
	private double lat;
	private double lng;
	private String link;
	private String filePath;
	private String content;
	
	public ChatMessage (){}
	
	public void update(ChatMessage source) {
		
		this.messageId = source.getMessageId();
		this.recipientId = source.getRecipientId();
		this.groupId = source.getGroupId();
		this.setStatus(source.getStatus());
		this.type = source.getType();
		this.lat = source.getLat();
		this.lng = source.getLng();
		this.content = source.getContent();
		this.link = source.getLink();
		this.created = source.getCreated();
		this.chatType = source.getChatType();
		this.dateInMillis = source.getDateInMillis();
		this.userName = source.getUserName();
		this.senderPhone = source.getSenderPhone();
		this.tempId = source.getTempId();
		this.newMessageId = source.getNewMessageId();
		this.senderID = source.getSenderId();
		this.autoDelete = source.isAutoDelete();
		this.readConfirmationStatus = source.getReadConfirmationStatus();
		this.fileLoaded = source.isFileLoaded();
		this.uploudFileCanceled = source.isUploudFileCanceled();
		this.filePath = source.getFilePath();
		this.downloadStatus = source.getDownloadStatus();
	}

	
	
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	/**
	 * @return the userId
	 */
	public String getSenderId() {
		return senderID;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setSenderId(String userId) {
		this.senderID = userId;
	}

	/**
	 * @return the messageId
	 */
	public int getMessageId() {
		if (messageId == 0){
			int re = 0;
			try {
				re = Integer.parseInt(tempId);
			} catch (Exception e) {}
			return re;
		}
		return messageId;
	}

	public String getCurrentMessageID () {
		if (messageId != 0){
			return messageId+"";
		}
		if(tempId != null){
			return tempId;
		}
		return "0";
	}
	
	
	/**
	 * @return the fileLoaded
	 */
	public boolean isFileLoaded() {
		return fileLoaded;
	}

	/**
	 * @param fileLoaded the fileLoaded to set
	 */
	public void setFileLoaded(boolean fileLoaded) {
		this.fileLoaded = fileLoaded;
	}

	
	
	/**
	 * @return the uploudFileCanceled
	 */
	public boolean isUploudFileCanceled() {
		return uploudFileCanceled;
	}

	/**
	 * @param uploudFileCanceled the uploudFileCanceled to set
	 */
	public void setUploudFileCanceled(boolean uploudFileCanceled) {
		this.uploudFileCanceled = uploudFileCanceled;
	}

	/**
	 * @return the readConfirmationStatus
	 */
	public int getReadConfirmationStatus() {
		return readConfirmationStatus;
	}

	/**
	 * @param readConfirmationStatus the readConfirmationStatus to set
	 */
	public void setReadConfirmationStatus(int readConfirmationStatus) {
		this.readConfirmationStatus = readConfirmationStatus;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}


	/**
	 * @return the recipientId
	 */
	public int getRecipientId() {
		return recipientId;
	}

	/**
	 * @param recipientId the recipientId to set
	 */
	public void setRecipientId(int recipientId) {
		this.recipientId = recipientId;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		Log.e("newMessageStatuslog", "this.status = " + this.status + " newStatus = " + status);
		if (status == STATUS_RECIVED_IN_SERVER){
			if (this.status != STATUS_RECIVED_IN_RECIPIENT && this.status != STATUS_READED_BY_RECIPIENT){
				this.status = status;
			}
			else{
				return;
			}
		}
		if (status == STATUS_READED_BY_RECIPIENT||status == STATUS_RECIVED_IN_RECIPIENT){
			if (this.status == STATUS_DELETED||this.status == STATUS_CONFIRM_DELETATION){
				return;
			}
		}
		if (this.status == STATUS_READED_BY_RECIPIENT){
			if (status != STATUS_DELETED&&status!=STATUS_CONFIRM_DELETATION){
				Log.e("confirmlog", "status - " + status);
				return;
			}else{
				this.status = status;
			}
		}
		this.status = status;
		Log.d("newMessageStatuslog", "this.status = " + this.status );
	}

	

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	
	
	/**
	 * @return the downloadStatus
	 */
	public int getDownloadStatus() {
		return downloadStatus;
	}

	/**
	 * @param downloadStatus the downloadStatus to set
	 */
	public void setDownloadStatus(int downloadStatus) {
		this.downloadStatus = downloadStatus;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public double getLng() {
		return lng;
	}

	/**
	 * @param lng the lng to set
	 */
	public void setLng(double lng) {
		this.lng = lng;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the created
	 */
	public String getCreated() {
		return DateUtil.formatServerDateWithTime(created);
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	

	/**
	 * @param chatType the chatType to set
	 */
	public void setChatType(int chatType) {
		this.chatType = chatType;
	}

	/**
	 * @return the dateInMillis
	 */
	public long getDateInMillis() {
		long milli = DateUtil.getDateMillisByServerDate(created,true);
		Log.e("millilog", "milli = " + milli);
		return milli;
	}

	/**
	 * @param dateInMillis the dateInMillis to set
	 */
	public void setDateInMillis(long dateInMillis) {
		this.dateInMillis = dateInMillis;
	}


	/**
	 * @return the tempId
	 */
	public String getTempId() {
		return tempId;
	}

	/**
	 * @param tempId the tempId to set
	 */
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * @return the newMessageId
	 */
	public String getNewMessageId() {
		return newMessageId;
	}

	/**
	 * @param newMessageId the newMessageId to set
	 */
	public void setNewMessageId(String newMessageId) {
		this.newMessageId = newMessageId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the senderPhone
	 */
	public String getSenderPhone() {
		return senderPhone;
	}

	
	
	/**
	 * @return the messagePhat
	 */
	public int getMessagePhat() {
		return messagePhat;
	}

	/**
	 * @param messagePhat the messagePhat to set
	 */
	public void setMessagePhat(int messagePhat) {
		this.messagePhat = messagePhat;
	}

	/**
	 * @param senderPhone the senderPhone to set
	 */
	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}
	
	
	
	/**
	 * @return the autoDelete
	 */
	public boolean isAutoDelete() {
		
		return autoDelete;
	}

	/**
	 * @param autoDelete the autoDelete to set
	 */
	public void setAutoDelete(boolean autoDelete) {
		this.autoDelete = autoDelete;
	}

	public String getDisplayName(){
		String result = "";
		if(senderPhone != null){
			AppContact temp = TipxApp.contactsManager.getContactByContactPhone(senderPhone);
			if (temp != null&&temp.getContactName()!= null&&!temp.getContactName().isEmpty()){
				return temp.getContactName();
			}
		}
		
		if(result.isEmpty()){
			result = userName;
		}
		if(result == null||result.isEmpty()){
			result = senderPhone;
		}
		return result;
	}
	
	
	public void setSide (int side){
		this.side = side;
	}
	
	public int getSide () {
		return side;
	}
	
	/**
	 * @return the type
	 */
	public int getType() {
		if (type == 1 || type == 11){
			return TYPE_TEXT;
		}else if(type ==2 || type == 12){
			return TYPE_LOCATION;
		}else if (type == 3 || type == 13){
			return TYPE_IMAGE;
		}else if (type == 4 || type == 14){
			return TYPE_VIDEO;
		}else if (type == 5 || type == 15){
			return TYPE_AUDIO;
		}else {
			return type;
		}
	}

	/**
	 * @return the chatType
	 */
	public int getChatType() {
		if (groupId > 0){
			return GROUP_MESSAGE;
		}
		return PRIVATE_MESSAGE;
	}
	
	public String getConversationId(){
		String id = "";
		if (getChatType() == ChatMessage.GROUP_MESSAGE){
			id = getGroupId()+"";
		}else{
			if (getMessagePhat() == ChatMessage.PHAT_TO){
				id = getRecipientId()+"";
			}else{
				if (getSenderId() != null&&!getSenderId().isEmpty()){
					id = getSenderId();
				}else{
					id = TipxApp.contactsManager.getContactIdByContactPhone(getSenderPhone());
				}
			}
		}
		return id;
	}
	
	
	
	public void startRemoveTimer () {
		Log.e("autoDeletelog", "start remove time");
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.e("autoDeletelog", "remove message");
				TipxApp.chatManager.handelRemoveMessage(ChatMessage.this);
			}
		}).start();
	}
	
	
	public String createFileName (){
		return getMessageId()+System.currentTimeMillis()+"";
	}
	

	

}
