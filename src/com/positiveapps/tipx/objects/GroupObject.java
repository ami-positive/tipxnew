/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.util.DateUtil;


/**
 * @author natiapplications
 *
 */
public class GroupObject extends ResponseObject implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	
	private final String MEMBERS = "Members";
	private final String MESSAGES = "Messages";
	
	private final String GROP_ID = "GroupID";
	private final String GRPUP_CODE = "Code";
	private final String ID = "ID";
	private final String NAME = "Name";
	private final String TAG_LINE = "TagLine";
	private final String ICON = "Icon";
	private final String IS_PUBLIC = "IsPublic";
	private final String ADMIN_ID = "AdminID";
	private final String CREATED_AT = "Created";
	private final String CODE = "Code";
	private final String STATUS = "Status";
	
	private String index;
	private String groupId;
	private String groupCode;
	private String name;
	private String tagLine;
	private String imageUrl;
	private String imagePath;
	private boolean isPublic;
	private String adminID;
	private String createdAt;
	private int Status;
	private boolean isSelected;
	private ArrayList<MemberObject> members;
	private ArrayList<ChatMessage> messages;
	private int groupColor;
	private boolean backgroundIsImage;
	private String backgroundPath;
	
	
	/**
	 * @return the groupColor
	 */
	public int getGroupColor() {
		return groupColor;
	}

	/**
	 * @param groupColor the groupColor to set
	 */
	public void setGroupColor(int groupColor) {
		this.groupColor = groupColor;
	}

	public GroupObject (){
		
	}
	
   
	
	public GroupObject(JSONObject toParse,boolean fullDetails) {
		super(toParse);
		
			String id = String.valueOf(toParse.optInt(ID,0));
			if (id.equals("0")){
				id = toParse.optString(ID);
			}
			this.groupId = id;
			this.groupCode = toParse.optString(CODE);
			this.name = toParse.optString(NAME);
			this.tagLine = toParse.optString(TAG_LINE);
			this.imageUrl = toParse.optString(ICON);
			this.adminID = toParse.optString(ADMIN_ID);
			this.createdAt = toParse.optString(CREATED_AT);
			this.Status = Integer.parseInt(toParse.optString(STATUS,"0"));
			int isPub = Integer.parseInt(toParse.optString(IS_PUBLIC,"1"));
			if (isPub > 0){
				this.isPublic = true;
			}else{
				this.isPublic = false;
			}
			JSONArray membersJsonArray = toParse.optJSONArray(MEMBERS);
			members = new ArrayList<MemberObject>();
			StringBuilder s = new StringBuilder();
			s.append("group id - " + groupId + " groupName = " + name + "\n");
			for (int i = 0; i < membersJsonArray.length(); i++) {
				MemberObject temp = new MemberObject(membersJsonArray.optJSONObject(i));
				s.append("        memberName - " + temp.getUserName() + "\n");
				this.members.add(temp);
			}
			s.append("======================================");
			Log.d("memberdesclog", "groupMembers: " + "\n" + s.toString());
			Log.e("memberslog", "mem ja size = " + membersJsonArray.length() + " mem size = " + members.size());
			
			
		
		
	}
	
	public void updateByGroup (GroupObject sourse){
		this.index = sourse.getIndex();
		this.groupId = sourse.getGroupId();
		this.groupCode = sourse.getGroupCode();
		this.name = sourse.getName();
		this.tagLine = sourse.getTagLine();
		this.imageUrl = sourse.getIcon();
		this.imagePath = sourse.getImagePath();
		this.isPublic = sourse.isPublic();
		this.adminID = sourse.getAdminID();
		this.createdAt = sourse.getCreatedAt();
		this.Status = sourse.getStatus();
		this.isSelected = sourse.isSelected;
		this.members = sourse.getMembers();
		this.groupColor = sourse.getGroupColor();
		this.backgroundIsImage = sourse.isBackgroundIsImage();
		this.backgroundPath = sourse.getBackgroundPath();
	}
	
	
	public GroupObject (JSONObject toParse,boolean fullDetails,String index){
		this(toParse,fullDetails);
		this.index = index;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupCode
	 */
	public String getGroupCode() {
		return groupCode;
	}

	
	/**
	 * @return the backgroundIsImage
	 */
	public boolean isBackgroundIsImage() {
		return backgroundIsImage;
	}

	/**
	 * @param backgroundIsImage the backgroundIsImage to set
	 */
	public void setBackgroundIsImage(boolean backgroundIsImage) {
		this.backgroundIsImage = backgroundIsImage;
	}

	/**
	 * @return the backgroundPath
	 */
	public String getBackgroundPath() {
		return backgroundPath;
	}

	/**
	 * @param backgroundPath the backgroundPath to set
	 */
	public void setBackgroundPath(String backgroundPath) {
		this.backgroundPath = backgroundPath;
	}

	/**
	 * @param groupCode the groupCode to set
	 */
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the tagLine
	 */
	public String getTagLine() {
		return tagLine;
	}

	/**
	 * @param tagLine the tagLine to set
	 */
	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return imageUrl;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.imageUrl = icon;
	}

	/**
	 * @return the isPublic
	 */
	public boolean isPublic() {
		return isPublic;
	}

	/**
	 * @param isPublic the isPublic to set
	 */
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	/**
	 * @return the adminID
	 */
	public String getAdminID() {
		return adminID;
	}

	/**
	 * @param adminID the adminID to set
	 */
	public void setAdminID(String adminID) {
		this.adminID = adminID;
	}

	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		Status = status;
	}

	/**
	 * @return the isSelected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * @param isSelected the isSelected to set
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * @return the members
	 */
	public ArrayList<MemberObject> getMembers() {
		return members;
	}

	/**
	 * @param members the members to set
	 */
	public void setMembers(ArrayList<MemberObject> members) {
		this.members = members;
	}

	/**
	 * @return the messages
	 */
	public ArrayList<ChatMessage> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(ArrayList<ChatMessage> messages) {
		this.messages = messages;
	}

	/**
	 * @return the index
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(String index) {
		this.index = index;
	}
	
	public String getCreatedAtMillis (){
		return DateUtil.getDateMillisByServerDate(getCreatedAt(),true)+"";
	}
	
	
	public String getLastMessages () {
		if (messages == null || messages.size() == 0){
			return "";
		}
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0 ; i < messages.size(); i++) {
			ChatMessage temp = messages.get(i);
			
			switch (temp.getType()) {
			case ChatMessage.TYPE_TEXT:
				builder.append(temp.getContent());
				Log.e("grouplistlog", i + " temp is text. contetn + " + temp.getContent());
				break;
			case ChatMessage.TYPE_IMAGE:
				builder.append("Image");
				Log.i("grouplistlog", i + " temp is image.");
				break;
			case ChatMessage.TYPE_LOCATION:
				builder.append("Location");
				Log.d("grouplistlog", i + " temp is location.");
				break;
			}
			
			
			if (builder.toString().length() > 30){
				String result = builder.toString().substring(0,30) + "...";
				return result;
			}
			
			builder.append(", ");
			
			
		}
		String result = builder.toString();
		if (result.length() > 30){
			result = result.substring(0,30) + "...";
		}
		return result;
	}
	
	public int getAnReadCount () {
		if (messages == null || messages.size() == 0){
			return 0;
		}
		int result = messages.size() - 
				TipxApp.generalSettings.getLastReadMessageCount(getGroupId()+"");
		return result;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public ArrayList<AppContact> getMembersAsContacts () {
		ArrayList<AppContact> result = new ArrayList<AppContact>();
		if (members == null){
			Log.e("memberslog", "members is null");
			return result;
		}
		for (int i = 0; i < members.size(); i++) {
			AppContact temp = new AppContact();
			temp.createByMember(members.get(i));
			result.add(temp);
		}
		return result;
	}
	
	public String getMemnbersAsString () {
		StringBuilder  result = new StringBuilder();
		if (members == null){
			Log.e("memberslog", "members is null");
			return "Me";
		}
		for (int i = 0; i < members.size(); i++) {
			if (members.get(i).getUserName() != null&&!members.get(i).getUserName().isEmpty()){
				result.append(members.get(i).getUserName());
				if (i < members.size()-1){
					result.append(", ");
				}
			}
		}
		return result.toString();
	}
	
	public HashMap<String, String> getTempColors() {
		HashMap<String, String> tempColors = new HashMap<String, String>();
		for (int i = 0; i < members.size(); i++) {
			tempColors
					.put(members.get(i).getId() + "",
							MainActivity.colors[(int) (Math.random() * MainActivity.colors.length)]);
		}
		return tempColors;
	}
	
	
}
