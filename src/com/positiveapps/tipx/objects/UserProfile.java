/**
 * 
 */
package com.positiveapps.tipx.objects;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.network.ResponseObject;

/**
 * @author NATI GABAY
 *
 */
public class UserProfile extends ResponseObject implements Serializable{
	
	public static final String APP_USER = "Appuser";
	
	public static final String STATUS = "Status";
	public static final String USER_NAME = "Username";
	public static final String FIRST_NAME =  "FirstName";
	public static final String LAST_NAME = "LastName";
	public static final String MODEL = "Model";
	public static final String PASSWORD = "Password";
	public static final String SECRET = "Secret";
	public static final String ICON_URL = "IconUrl";
	public static final String DESPLAY_LAST_LOGIN = "DisplayLastLogin";
	public static final String SHOW_PROFILE_IMAGE = "ShowProfileImage";
	public static final String PRESENT_STATUS = "PresentStatus";
	public static final String REMOVE_MESSAGE = "RemoveMessages";
	public static final String COUNTRY_CODE = "CountryCode";
	public static final String COUNTRY_NAME = "CountryName";
	public static final String CITY = "City";
	public static final String STATE = "State";
	public static final String ABOUT = "About";
	public static final String GCM_TOCKEN = "GCM";
	public static final String PHONE = "Phone";
	public static final String ID = "ID";
	public static final String CREATED_AT = "Created";
    public static final String ONLINE = "Online";
  

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserProfile(JSONObject toParse) {
		super(toParse);
		try {
			
			//TipxApp.userProfil.setUserStatus(user.optString(STATUS));
			if (!isHasError()){
				JSONObject user = new JSONObject(getData()).optJSONObject(APP_USER);
				String firstName = user.optString(FIRST_NAME);
				String lastName = user.optString(LAST_NAME);
				String userName = firstName + " " + lastName;
				if (userName.replace(" ", "").isEmpty()){
					userName = user.optString(USER_NAME);
				}
				TipxApp.userProfil.setUserNaem(userName);
				TipxApp.userProfil.setUserPass(user.optString(PASSWORD));
				TipxApp.userProfil.setUserSecret(user.optString(SECRET));
				TipxApp.userProfil.setUserId(user.optString(ID));
				TipxApp.userProfil.setUserIconUrl(user.optString(ICON_URL));
				TipxApp.userProfil.setUserPhone(user.optString(PHONE));
				TipxApp.userProfil.setUserCountry(user.optString(COUNTRY_NAME));
				TipxApp.userProfil.setUserCreatedAt(user.getString(CREATED_AT));
				
				
				String displayLastLocationTime = user.optString(DESPLAY_LAST_LOGIN,"0");
				if (!displayLastLocationTime.isEmpty()){
					int set = Integer.parseInt(displayLastLocationTime);
					if (set > 0){
						TipxApp.userSettings.setDisplayLastLoginTime(true);
					}else{
						TipxApp.userSettings.setDisplayLastLoginTime(false);
					}
				}
				
				
				String showProfileImage = user.optString(SHOW_PROFILE_IMAGE,"0");
				if (!showProfileImage.isEmpty()){
					int set = Integer.parseInt(showProfileImage);
					if (set > 0){
						TipxApp.userSettings.setShowProfilePicture(true);
					}else{
						TipxApp.userSettings.setShowProfilePicture(false);
					}
				}
				
				
				String precentStatus = user.optString(PRESENT_STATUS,"0");
				if (!precentStatus.isEmpty()){
					int set = Integer.parseInt(precentStatus);
					if (set > 0){
						TipxApp.userSettings.setPresentStatus(true);
					}else{
						TipxApp.userSettings.setPresentStatus(false);
					}
				}
				
				
				String removeMessages = user.optString(REMOVE_MESSAGE,"0");
				if (!removeMessages.isEmpty()){
					int set = Integer.parseInt(removeMessages);
					if (set > 0){
						TipxApp.userSettings.setRemoveMessages(true);
					}else{
						TipxApp.userSettings.setRemoveMessages(false);
					}
				}
			}
			
			
			
		} catch (JSONException e) {}
	}


	
	
	
	

}
