/**
 * 
 */
package com.positiveapps.tipx.dialogs;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * @author Maor
 *
 */
public class DialogManager {



	
	public static void showDialogChooser(Fragment parentFragment, String[] optionsText,
			DialogCallback callback){
		
		CooserDialog dialog = new CooserDialog(parentFragment,optionsText,callback);
		dialog.show(parentFragment.getFragmentManager(), CooserDialog.DIALOG_NAME);
	}
	
	public static void showAppDialog(Fragment parent,String title,
			String message,int iconBg, int icon,AppDialogButton[] buttons,boolean cancelable,
			DialogCallback callback){
		
		AppDialog dialog = new AppDialog(parent, title, message, iconBg, icon, buttons, callback);
		dialog.setCancelable(cancelable);
		dialog.show(parent.getFragmentManager(), AppDialog.DIALOG_NAME);
	}
	
	public static void showAppDialog(FragmentManager fm,String title,
			String message,int iconBg, int icon,AppDialogButton[] buttons,boolean cancelable,
			DialogCallback callback){
		
		AppDialog dialog = new AppDialog(title, message, iconBg, icon, buttons, callback);
		dialog.setCancelable(cancelable);
		dialog.show(fm, AppDialog.DIALOG_NAME);
	}
	
	public static void showLockPasswordDialog(FragmentManager fm,int mode,DialogCallback callback){
		
		LockPasswordDialog dialog = new LockPasswordDialog(mode, callback);
		dialog.show(fm, LockPasswordDialog.DIALOG_NAME);
	}
	
	
    public static void showCreateNewContactDialog(FragmentManager fm,DialogCallback callback){
		
		CreateNewContactDialog dialog = new CreateNewContactDialog(callback);
		dialog.show(fm, CreateNewContactDialog.DIALOG_NAME);
	}
    
    public static void showColorPickerDialog(int previousColor,FragmentManager fm,DialogCallback callback){
		
  		ColorsPickerDialog dialog = new ColorsPickerDialog(previousColor,callback);
  		dialog.show(fm, ColorsPickerDialog.DIALOG_NAME);
  	}

	public static void showErrorDialog(Fragment parentFragment ,String desc,
			DialogCallback callback){
		
		ErrorDialog dialog = new ErrorDialog(parentFragment,desc,callback);
		
		dialog.show(parentFragment.getFragmentManager(), ErrorDialog.DIALOG_NAME);
	}
	
	
	
}
