/**
 * 
 */
package com.positiveapps.tipx.dialogs;


import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;

import android.graphics.AvoidXfermode.Mode;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * @author natiapplications
 *
 */
public class LockPasswordDialog extends BaseDialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "LockPasswordDialog";
	
	public static final int MAIN_SCREEN = 1;
	public static final int SET_NEW_PASSCODE = 2;
	
	
	
	private EditText passcodeEt;
	private TextView invalidPasscodeTxt;
	private Button confirmBtn;
	private Button closeBtn;
	private int cuurentMode;
	
	
	private DialogCallback callback;
	
	/**
	 * 
	 * @param description
	 * @param credits
	 * @param callback
	 */
	public LockPasswordDialog(int mode,DialogCallback callback){
		//this.mDescription = description;
		this.callback = callback;
		this.cuurentMode = mode;
	}
	

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_lock_passcode, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        passcodeEt = (EditText)view.findViewById(R.id.passcode_et);
        confirmBtn = (Button)view.findViewById(R.id.confirm_btn);
        closeBtn = (Button)view.findViewById(R.id.close_app_btn);
        invalidPasscodeTxt = (TextView)view.findViewById(R.id.invalid_passcod_txt);
        passcodeEt.requestFocus();
        invalidPasscodeTxt.setVisibility(View.GONE);
        
        this.setCancelable(false);
        
        confirmBtn.setOnClickListener(this);
        closeBtn.setOnClickListener(this);
 
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }
    
    

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.confirm_btn:
	    	if (cuurentMode == MAIN_SCREEN){
	    		if(callback != null){
		    		if (TipxApp.userSettings.getLockPasswordNumber().equals(passcodeEt.getText().toString())){
		    			TipxApp.isFaceMood = false;
		    		}else{
		    			TipxApp.isFaceMood = true;
		    		}
		    		dismissDialog();
	    			callback.onDialogButtonPressed(v.getId(), this);
		    	}
			}else if (cuurentMode == SET_NEW_PASSCODE){
				if(callback != null){
		    		if (TipxApp.userSettings.getLockPasswordNumber().equals(passcodeEt.getText().toString())){
		    			dismissDialog();
		    			callback.onDialogButtonPressed(v.getId(), this);
		    		}else{
		    			passcodeEt.setSelected(true);
		    			invalidPasscodeTxt.setVisibility(View.VISIBLE);
		    		}
		    	}
			}
			break;
		case R.id.close_app_btn:
			if (cuurentMode == MAIN_SCREEN){
				dismissDialog();
				if (callback != null){
					callback.onDialogButtonPressed(v.getId(), this);
				}
			}else if (cuurentMode == SET_NEW_PASSCODE){
				dismissDialog();
			}
			break;
		}
		
	}


	

	
}

