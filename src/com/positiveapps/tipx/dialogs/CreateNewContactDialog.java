/**
 * 
 */
package com.positiveapps.tipx.dialogs;

import java.util.Locale;


import com.positiveapps.tipx.R;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.util.TextUtil;
import com.positiveapps.tipx.util.ToastUtil;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;



/**
 * @author natiapplications
 *
 */
public class CreateNewContactDialog extends BaseDialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "CreateNewContactDialog";
	
	
	private EditText nameEt;
	private EditText phoneEt;
	private Button cancelBtn;
	private Button createBtn;
	private String phoneNumber;
	
	private DialogCallback callback;
	
	/**
	 * 
	 * @param description
	 * @param credits
	 * @param callback
	 */
	public CreateNewContactDialog(DialogCallback callback){
		//this.mDescription = description;
		this.callback = callback;
		
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_create_new_contact, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        nameEt = (EditText)view.findViewById(R.id.et_add_name);
        phoneEt = (EditText)view.findViewById(R.id.et_add_phone);
        cancelBtn = (Button)view.findViewById(R.id.btn_cancel);
        createBtn = (Button)view.findViewById(R.id.btn_create);
        cancelBtn.setOnClickListener(this);
        createBtn.setOnClickListener(this);
       
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }
    
    

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_cancel:
			dismissDialog();
			break;
		case R.id.btn_create:
			if (TextUtil.baseFieldsValidation(nameEt,phoneEt)){
				if (isValidPhoneNumber(phoneEt.getText().toString())){
					AppContact appContact = new AppContact();
					appContact.setContactName(nameEt.getText().toString());
					appContact.setContactPhone(phoneNumber);
					dismissDialog();
					if(callback != null){
			    		callback.onDialogButtonPressed(v.getId(), this,appContact);
			    	}
					
				}else{
					ToastUtil.toster("Invalid phone number", false);
				}
			}else{
				ToastUtil.toster("There is empty fields", false);
			}
			break;
		}
    	
    	
    	
	}


	/**
	 * @return
	 */
	@SuppressLint("NewApi")
	private boolean isValidPhoneNumber(String phone) {
		phoneNumber = TextUtil.normalizePhoneNumber(phone);
		if (phoneNumber == null){
			return false;
		}
		return true;
	}


	
	

	
}
