/**
 * 
 */
package com.positiveapps.tipx.dialogs;

/**
 * @author natiapplications
 *
 */
public class AppDialogButton {
	
	public int id;
	public int color;
	public String text;
	
	public AppDialogButton (int id,int color,String text){
		this.id = id;
		this.color = color;
		this.text = text;
	}

}
