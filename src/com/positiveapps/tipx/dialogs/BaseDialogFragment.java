/**
 * 
 */
package com.positiveapps.tipx.dialogs;

import com.positiveapps.tipx.util.AppUtil;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;


/**
 * @author natiapplications
 *
 */
public class BaseDialogFragment extends DialogFragment{
	
	
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		AppUtil.setTextFonts(getActivity(), view);

	}

}
