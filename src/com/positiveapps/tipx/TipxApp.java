/**
 * 
 */
package com.positiveapps.tipx;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.positiveapps.tipx.chat.ConversationManager;
import com.positiveapps.tipx.chat.SmackManager;
import com.positiveapps.tipx.chat.TipxChatManager;
import com.positiveapps.tipx.contacts.ContactsManager;
import com.positiveapps.tipx.database.AppPreference;
import com.positiveapps.tipx.database.AppPreference.PreferenceGeneralSettings;
import com.positiveapps.tipx.database.AppPreference.PreferenceUserProfil;
import com.positiveapps.tipx.database.AppPreference.PreferenceUserSettings;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.database.StorageManager;
import com.positiveapps.tipx.network.NetworkManager;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.DeviceUtil;

/**
 * @author natiapplications
 *
 */
public class TipxApp extends Application {

	public static boolean appIsOn;
	public static Context appContext;
	public static String appVersion;
	public static AppPreference appPreference;
	public static PreferenceUserSettings userSettings;
	public static PreferenceGeneralSettings generalSettings;
	public static PreferenceUserProfil userProfil;
	public static NetworkManager networkManager;
	public static StorageManager storageManager;
	public static SmackManager smackManager;
	public static ContactsManager contactsManager;
	public static ConversationManager conversationManager;
	public static TipxChatManager chatManager;
	public static boolean isFaceMood;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Crashlytics.start(this);
		appIsOn = true;
		appContext = getApplicationContext();
		appVersion = AppUtil.getApplicationVersion(appContext);
		loadPreferences();
		
		networkManager = NetworkManager.getInstance(appContext);
		storageManager = StorageManager.getInstance();
		
		contactsManager = ContactsManager.getInstance();
		conversationManager = ConversationManager.getInstance();
		chatManager = TipxChatManager.getInstance();
		
	}
	
	
	private void loadPreferences() {
		appPreference = AppPreference.getInstans(appContext);
		userSettings = appPreference.getUserSettings();
		userProfil = appPreference.getUserProfil();
		generalSettings = appPreference.getGeneralSettings();
	}
	
}
