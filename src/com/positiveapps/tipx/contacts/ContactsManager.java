/**
 * 
 */
package com.positiveapps.tipx.contacts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONObject;


import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.database.StorageManager;

/**
 * @author natiapplications
 *
 */
public class ContactsManager {
	
	
	private static ContactsManager contactsManager;
	private ArrayList<AppContact> contacts;
	
	private ContactsManager () {
		readContactsList(null);
	}
	
	public static ContactsManager getInstance(){
		if (contactsManager == null){
			contactsManager = new ContactsManager();
		}
		return contactsManager;
	}
	
	public static void removeInstance (){
		contactsManager = null;
	}
	
	public void deletAllContact(){
		contacts.clear();
		SaveContactsToStorage();
	}
	
	public void readContactsList (final StorageListener callback){
		if (contacts != null){
			if (callback != null){
				callback.onDataStorageRcived("", getAppContactsList());
			}
			return;
		}
		TipxApp.storageManager.readFromStorage(StorageManager.APP_CONTACTS_LIST_FILE_NAME, new StorageListener() {
			
			@Override
			public void onDataStorageRcived(String type, Object object) {
				contacts = (ArrayList<AppContact>)object;
				if (callback != null){
					callback.onDataStorageRcived(type, contacts);
				} 
			}
			
			@Override
			public void DataNotFound(String type) {
				contacts = new ArrayList<AppContact>();
				if (callback != null){
					callback.onDataStorageRcived(type, contacts);
				}
			}
		});
	}
	
	public void SaveContactsToStorage(){
		TipxApp.storageManager.writeToStorage(StorageManager.APP_CONTACTS_LIST_FILE_NAME, contacts);
	}
	
	public ArrayList<AppContact> getAppContactsList (){
		Collections.sort(contacts,new Comparator<AppContact>() {

			@Override
			public int compare(AppContact lhs, AppContact rhs) {
				
				return lhs.getContactName().toUpperCase().compareTo(rhs.getContactName().toUpperCase());
			}
		});
		return this.contacts;
	}
	
	public ArrayList<AppContact> addContact (final AppContact toAdd){
		if (contacts == null){
			contacts = new ArrayList<AppContact>();
		}
		if (!isContactAllradyAdded(toAdd)){
			contacts.add(toAdd);
			SaveContactsToStorage();
		}
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> addMultipleContacts (ArrayList<AppContact> toAdd){
		if (contacts == null){
			contacts = toAdd;
		}else{
			for (int i = 0; i < toAdd.size(); i++) {
				if (!isContactAllradyAdded(toAdd.get(i))){
					contacts.add(toAdd.get(i));
				}
			}
		} 
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> updateContactByContactId(String id,AppContact source){
		if (contacts == null){
			contacts = new ArrayList<AppContact>();
			SaveContactsToStorage();
		}else{
			for (int i = 0; i < contacts.size(); i++) {
				if (contacts.get(i).getContactId().equals(id)){
					contacts.get(i).update(source);
					
					break;
				}
			}
		}
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> updateContactByContactPhone(String phone,AppContact source){
		if (contacts == null){
			contacts = new ArrayList<AppContact>();
			SaveContactsToStorage();
		}else{
			for (int i = 0; i < contacts.size(); i++) {
				if (contacts.get(i).getContactPhone().equals(phone)){
					contacts.get(i).update(source);
					
					break;
				}
			}
		}
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> removeContactByContactId (String id){
		if (contacts == null){
			contacts = new ArrayList<AppContact>();
			SaveContactsToStorage();
		}else{
			for (int i = 0; i < contacts.size(); i++) {
				if (contacts.get(i).getContactId().equals(id)){
					contacts.remove(i);
					break;
				}
			}
		}
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> removeContactByContactPhone (String phone){
		if (contacts == null){
			contacts = new ArrayList<AppContact>();
			SaveContactsToStorage();
		}else{
			for (int i = 0; i < contacts.size(); i++) {
				if (contacts.get(i).getContactPhone().equals(phone)){
					contacts.remove(i);
					break;
				}
			}
		}
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> updateContactsStatus (JSONArray toUpdate){
		for (int i = 0; i < toUpdate.length(); i++) {
			try {
				JSONObject temp = toUpdate.optJSONObject(i);
				String phone = temp.optString("Phone");
				int hasApp = temp.optInt("HasApp");
				if (hasApp > 0){
					for (int j = 0; j < contacts.size(); j++) {
						if (contacts.get(j).getContactPhone().replace("+", "").equals(phone)){
							contacts.get(j).setContactId(temp.optString("ID"));
							contacts.get(j).setContactImgaeUrl(temp.optString("IconUrl"));
							contacts.get(j).setHasApplication(true);
							contacts.get(j).setLastSeen(temp.optString("LastSeen"));
							contacts.get(j).setContactStatusLine(temp.optString("Status"));
							TipxApp.conversationManager.updateConversationProperteisByContact(contacts.get(j));
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	
	public ArrayList<AppContact> updateContactsPresentStatus (JSONArray toUpdate){
		
		for (int j = 0; j < contacts.size(); j++) {
			String id = contacts.get(j).getContactId();
			if (id != null&&!id.isEmpty()){
				boolean set = false;
				for (int i = 0; i < toUpdate.length(); i++) {
					String temp = toUpdate.optString(i);
					if (id.equals(temp)){
						contacts.get(j).setContactStatus(AppContact.STATUS_ONLINE);
						set = true;
						break;
					}
				}
				if (!set){
					contacts.get(j).setContactStatus(AppContact.STATUS_OFFLINE);
				}
			}
		}
		
		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public ArrayList<AppContact> updateContactPresentStatusById(String contactId) {

		for (int j = 0; j < contacts.size(); j++) {
			String id = contacts.get(j).getContactId();
			if (id != null && !id.isEmpty()) {
				if (id.equals(contactId)) {
					contacts.get(j).setContactStatus(AppContact.STATUS_ONLINE);
					break;
				}
			}
		}

		SaveContactsToStorage();
		return getAppContactsList();
	}
	
	public String getContactIdByContactPhone (String phone) {
		if (contacts == null){
			return "";
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contacts.get(i).getContactPhone().replace("+", "").equalsIgnoreCase(phone)){
				return contacts.get(i).getContactId();
			}
		}
		return "";
	}
	
	public String getContactImageByContactPhone (String phone) {
		if (contacts == null){
			return "";
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contacts.get(i).getContactPhone().replace("+", "").equalsIgnoreCase(phone)){
				String imagePath = contacts.get(i).getContactImagePath();
				if (imagePath != null&&!imagePath.isEmpty()){
					return imagePath;
				}else{
					return contacts.get(i).getContactImgaeUrl();
				}
			}
		}
		return "";
	}
	
	public AppContact getContactByContactId(String id) {
		if (contacts == null){
			return null;
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contacts.get(i).getContactId() ==  null){
				continue;
			}
			if (contacts.get(i).getContactId().equalsIgnoreCase(id)){
				return contacts.get(i);
			}
		}
		return null;
	}
	
	public AppContact getContactByContactPhone(String phone) {
		if (contacts == null){
			return null;
		}
		for (int i = 0; i < contacts.size(); i++) {
			
			if (contacts.get(i).getContactPhone().replace("+", "").equalsIgnoreCase(phone.replace("+", ""))){
				return contacts.get(i);
			}
		}
		return null;
	}
	
	public ArrayList<AppContact> getHidenContacts () {
		ArrayList<AppContact> result = new ArrayList<AppContact>();
		if (contacts == null){
			return result;
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contacts.get(i).isHidenContact()){
				result.add(contacts.get(i));
			}
		}
		return result;
	}
	
	public void setHidenContact (String contactPhone,boolean hiden,boolean save){
		if (contacts == null){
			return;
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contactPhone.replace("+", "").equalsIgnoreCase(contacts.get(i).getContactPhone().replace("+", ""))){
				contacts.get(i).setHidenContact(hiden);
				break;
			}
		}
		if (save){
			SaveContactsToStorage();
		}
		
	}
	
	public boolean isContactHiden (String contactPhone){
		if (contacts == null){
			return false;
		}
		if (contactPhone == null){
			return false;
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contactPhone.replace("+", "").equalsIgnoreCase(contacts.get(i).getContactPhone().replace("+", ""))){
				return contacts.get(i).isHidenContact();
			}
		}
		return false;
	}
	
	
	public boolean isContactHidenById (String contactID){
		if (contacts == null){
			return false;
		}
		if (contactID == null){
			return false;
		}
		for (int i = 0; i < contacts.size(); i++) {
			if (contactID.equalsIgnoreCase(contacts.get(i).getContactId())){
				return contacts.get(i).isHidenContact();
			}
		}
		return false;
	}
	
	public boolean isContactAllradyAdded (AppContact contact){
		for (int i = 0; i < TipxApp.contactsManager.getAppContactsList().size(); i++) {
			if (contact.getContactPhone().replace("+", "")
					.equals(TipxApp.contactsManager.getAppContactsList().get(i).getContactPhone().replace("+", ""))){
				return true;
			}
		}
		return false;
	}

}
