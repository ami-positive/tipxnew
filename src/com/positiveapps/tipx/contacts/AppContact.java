/**
 * 
 */
package com.positiveapps.tipx.contacts;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;

import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.objects.MemberObject;
import com.positiveapps.tipx.util.DateUtil;

import android.graphics.Color;
import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class AppContact implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	public static final int STATUS_ONLINE = 1;
	public static final int STATUS_OFFLINE = 0;
	public static final int STATUS_TYPEING = 2;
	
	public static final int[] contactsColor = {R.color.chat_bg_gray,R.color.chat_bg_pink,R.color.chat_bg_paper,R.color.chat_bg_blue};
	public static final String[] STATUS_DISPLAY = {"Offline","Online","Typing..."};
	
	private String contactId;
	private String contactName;
	private String contactPhone;
	private String contactFullPhone;
	private int contactColor;
	private int contactStatus;
	private String contactStatusLine;
	private boolean isHasApplication;
	private String contactImagePath;
	private String contactImgaeUrl;
	private boolean isHidenContact;
	private String lastSeen;
	private String backgroundPath;
	private boolean backgroundIsImage;
	
	
	public void update (AppContact source){
		this.contactId = source.getContactId();
		this.contactName = source.getContactName();
		this.contactPhone = source.getContactPhone();
		this.contactFullPhone = source.getContactFullPhone();
		this.contactColor = source.getContactColor();
		this.contactStatus = source.getContactStatus();
		this.isHasApplication = source.isHasApplication();
		this.contactImagePath = source.getContactImagePath();
		this.contactImgaeUrl = source.getContactImgaeUrl();
		this.isHidenContact = source.isHidenContact();
		this.lastSeen = source.getLastSeen();
		this.contactStatusLine = source.getContactStatusLine();
		this.backgroundPath = source.getBackgroundPath();
		this.backgroundIsImage = source.isBackgroundIsImage();
	}
	
	public void createByMember (MemberObject source){
		this.contactId = source.getId()+"";
		this.contactName = source.getUserName();
		this.contactPhone = "";
		this.contactFullPhone = "";
		this.contactColor = 0;
		this.contactStatus = 0;
		this.isHasApplication = true;
		this.contactImagePath = "";
		this.contactImgaeUrl = "";
	}
	
	



	/**
	 * @return the contactId
	 */
	public String getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the contactName
	 */
	public String getContactName() {
		if (contactName == null){
			return "Unknow";
		}
		return contactName;
	}
	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}
	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	/**
	 * @return the contactStatusLine
	 */
	public String getContactStatusLine() {
		if (contactStatusLine != null&&!contactStatusLine.isEmpty()){
			return contactStatusLine;
		}else{
			if (isHasApplication){
				return "Hey there! I am using Tipx";
			}else{
				return "Tap and hold to invite me";
			}
		}
	}

	/**
	 * @param contactStatusLine the contactStatusLine to set
	 */
	public void setContactStatusLine(String contactStatusLine) {
		this.contactStatusLine = contactStatusLine;
	}

	
	/**
	 * @return the backgroundIsImage
	 */
	public boolean isBackgroundIsImage() {
		return backgroundIsImage;
	}

	/**
	 * @param backgroundIsImage the backgroundIsImage to set
	 */
	public void setBackgroundIsImage(boolean backgroundIsImage) {
		this.backgroundIsImage = backgroundIsImage;
	}

	/**
	 * @return the contactFullPhone
	 */
	public String getContactFullPhone() {
		return contactFullPhone;
	}
	/**
	 * @param contactFullPhone the contactFullPhone to set
	 */
	public void setContactFullPhone(String contactFullPhone) {
		this.contactFullPhone = contactFullPhone;
	}
	/**
	 * @return the contactColor
	 */
	public int getContactColor() {
		return contactColor;
	}
	/**
	 * @param contactColor the contactColor to set
	 */
	public void setContactColor(int contactColor) {
		this.contactColor = contactColor;
	}
	/**
	 * @return the contactStatus
	 */
	public int getContactStatus() {
		return contactStatus;
	}
	/**
	 * @param contactStatus the contactStatus to set
	 */
	public void setContactStatus(int contactStatus) {
		this.contactStatus = contactStatus;
	}
	/**
	 * @return the isHasApplication
	 */
	public boolean isHasApplication() {
		return isHasApplication;
	}
	/**
	 * @param isHasApplication the isHasApplication to set
	 */
	public void setHasApplication(boolean isHasApplication) {
		this.isHasApplication = isHasApplication;
	}
	
	
	
	/**
	 * @return the contactImagePath
	 */
	public String getContactImagePath() {
		return contactImagePath;
	}



	/**
	 * @param contactImagePath the contactImagePath to set
	 */
	public void setContactImagePath(String contactImagePath) {
		this.contactImagePath = contactImagePath;
	}



	/**
	 * @return the contactImgaeUrl
	 */
	public String getContactImgaeUrl() {
		return contactImgaeUrl;
	}



	/**
	 * @param contactImgaeUrl the contactImgaeUrl to set
	 */
	public void setContactImgaeUrl(String contactImgaeUrl) {
		this.contactImgaeUrl = contactImgaeUrl;
	}


	

	/**
	 * @return the isHidenContact
	 */
	public boolean isHidenContact() {
		return isHidenContact;
	}

	/**
	 * @param isHidenContact the isHidenContact to set
	 */
	public void setHidenContact(boolean isHidenContact) {
		this.isHidenContact = isHidenContact;
	}
	
	

	/**
	 * @return the backgroundPath
	 */
	public String getBackgroundPath() {
		return backgroundPath;
	}

	/**
	 * @param backgroundPath the backgroundPath to set
	 */
	public void setBackgroundPath(String backgroundPath) {
		this.backgroundPath = backgroundPath;
	}

	/**
	 * @return the lastSeen
	 */
	public String getLastSeen() {
		if (lastSeen != null){
			
			if (lastSeen.contains("0000")||lastSeen.isEmpty()){
				return STATUS_DISPLAY[STATUS_OFFLINE];
			}
			return "Last seen at: " + DateUtil.formatServerDateForLastSeen(lastSeen);
		}
		else{
			return STATUS_DISPLAY[STATUS_OFFLINE];  
		}
	}

	/**
	 * @param lastSeen the lastSeen to set
	 */
	public void setLastSeen(String lastSeen) { 
		this.lastSeen = lastSeen;
	}

	public static void checkIfContactHasApplication (ArrayList<AppContact> toCheck,NetworkCallback callback){
		if (toCheck == null){
			if (callback != null){
				callback.onDataRecived(null, true, "No contact set yet");
			}
			return;
		}
		if (toCheck.size() == 0){
			if (callback != null){
				callback.onDataRecived(null, true, "No contact set yet");
			}
			return;
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < toCheck.size(); i++) {
			try {
				builder.append(toCheck.get(i).getContactPhone().replace("+", ""));
				
				if (i < toCheck.size()-1){
					builder.append(",");
				}
			} catch (Exception e) {
				continue;
			}
			
		}
		builder.append("]"); 
		System.out.println("-------------->" +builder.toString().replace(",,",","));
		TipxApp.networkManager.getContactsAppInfo(builder.toString().replace(",,",","), callback);
	}
	
	public static void getOnlineContacts (ArrayList<AppContact> toCheck,NetworkCallback callback){
		if (toCheck == null){
			if (callback != null){
				callback.onDataRecived(null, true, "No contact set yet");
			}
			return;
		}
		if (toCheck.size() == 0){
			if (callback != null){
				callback.onDataRecived(null, true, "No contact set yet");
			}
			return;
		}
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < toCheck.size(); i++) {
			String id = toCheck.get(i).getContactId();
			if (id != null && !id.isEmpty()) {
				//builder.append("\"");
				builder.append(toCheck.get(i).getContactId());
				//builder.append("\"");
				if (i < toCheck.size() - 1) {
					builder.append(",");
				}
			}
		}
		builder.append("]");
		
		ArrayList<String> phones = new ArrayList<String>();
		for (int i = 0; i < toCheck.size(); i++) {
			try {
				String id = toCheck.get(i).getContactId();
				if (id != null && !id.isEmpty()) {
					phones.add(id);
				}
			} catch (Exception e) {
				continue;
			}
			
		}
		JSONArray phonesJA = new JSONArray(phones);
		Log.e("hasapplog", "contacs - " + builder.toString().replace(",]", "]"));
		TipxApp.networkManager.getOnlineUserFriends(builder.toString().replace(",]", "]"), callback);
	}
	
}
