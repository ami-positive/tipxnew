package com.positiveapps.tipx.parser;

public interface GetTagContent {
	
	public void getContent (String tag,String content);
	public void getAttribute(String tagName,String attrName, String attrValue);
	public void getEndTag (String tag);
	public void getStartTag (String tag);
	public void onEndAttribute(String attrName);
	public void onStartDucoment();
	public void onEndDucoment();
	

}
