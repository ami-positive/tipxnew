package com.positiveapps.tipx;



import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.positiveapps.tipx.chat.ChatMessageJsonParser;
import com.positiveapps.tipx.chat.ChatMessageXMLParser;
import com.positiveapps.tipx.chat.SmackManager;
import com.positiveapps.tipx.chat.SmackManager.ChatListener;
import com.positiveapps.tipx.chat.TipxChatListener;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.AppContactsFragment;
import com.positiveapps.tipx.fragments.BaseFragment;
import com.positiveapps.tipx.fragments.ChatContainerFragment;
import com.positiveapps.tipx.gcm.GcmManager;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.services.LocationService;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;



public class MainActivity extends FragmentActivity implements OnClickListener,ChatListener,
PacketListener{

	
	
	public static final String EXTRA_FROM_NOTIFICATION = "FromNotification";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static String[] colors;
	public static MainActivity mainInstance;
	public static BaseFragment currentFragment;
	public static TextView screenTitle;
	public static TextView statusTxt;
	public static ProgressBar progressBar;
	public static LinearLayout buttonsContainer;
	public static boolean isFromNotification;
	public static Conversation notificationConversation;
	public static boolean activityISsOn;
	public static boolean appSentToBackground;
	public static TipxChatListener tipxChatListener;
	public static TipxChatListener conversationListener;
	
	
	private BroadcastReceiver updateIUReceiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeNoTitleActivity(this);
		AppUtil.setPerventFromUserTakingScreenShote(this);
		AppUtil.saveScreenDimention(this);
		setContentView(R.layout.activity_main);
		activityISsOn = true;
		mainInstance = this;
		colors = getResources().getStringArray(R.array.colors);
		screenTitle = (TextView)findViewById(R.id.screen_title);
		statusTxt = (TextView)findViewById(R.id.status_txt);
		buttonsContainer = (LinearLayout)findViewById(R.id.action_bar_buttons_container);
		progressBar = (ProgressBar)findViewById(R.id.progress);
		TipxApp.smackManager = SmackManager.getInstance(this, TipxApp.userProfil.getUserId(),
				TipxApp.userProfil.getUserSecret(), this);
		TipxApp.smackManager.connect();
		updateUserApplicationStatus(1);
		checkForGPS();
		if (checkPlayServices()){
			checkForGCMRegistrationID();
		}
		checkForLockPasscode();
		
	}

	
	
	private void checkForUpdate() {
		String oldVersion = TipxApp.appVersion;
		String newVersion = TipxApp.generalSettings.getServerNewVerstion();
		
		double oldV = Double.parseDouble(oldVersion);
		double newV = Double.parseDouble(newVersion);
		if (((int)newV) > ((int)oldV)){
			// must update
			showNeedUpdateDialog(true);
		}else{
			if (newV > oldV){
				// can update
				showNeedUpdateDialog(false);
			}
			//all is rite !
		}
		
	}
	
	
	private void showNeedUpdateDialog (final boolean mast) {
		
		String cancel = getString(R.string.not_now);
		String message = getString(R.string.need_update_txt);
		if (mast){
			cancel = getString(R.string.close_app);
			message = getString(R.string.must_updte_txt);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(!mast);
		builder.setMessage(message);
		builder.setTitle(getString(R.string.update_app))
				.setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
						try {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
						    		("https://play.google.com/store/apps/details?id=" + appPackageName)));
						}
					}
				}).setNegativeButton(cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (mast){
							finish();
						}
					}
				}).show();
	}


	/**
	 * 
	 */
	private void checkForQueuedMessages() {
		TipxApp.networkManager.getQueuedMessages(new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				if (!isHasError){
					Log.e("queuedMessageResponse = " , response.getJsonContent());
					try {
						JSONObject jsonObject = new JSONObject(response.getData());
						JSONArray toAdd = jsonObject.optJSONArray("QueuedMessages");
						JSONArray toDelete = jsonObject.optJSONArray("DeleteRequests");
						TipxApp.conversationManager.removeMultypleMessages(toDelete);
						for (int i = 0; i < toAdd.length(); i++) {
							ChatMessage temp = 
									new ChatMessageJsonParser(toAdd.optJSONObject(i)).parse();
							temp.setSide(ChatMessage.SIDE_LEFT);
							temp.setMessagePhat(ChatMessage.PHAT_FROM);
							TipxApp.chatManager.handleReceivedMessage(temp);
							
							if (conversationListener != null){
								conversationListener.onChatMessageReceived(temp);
							}
						}
						
					} catch (Exception e) {
						Log.e("ex", "ex = " + e.getMessage());
						e.printStackTrace();
					}
				}
			}
		});
	}


	/*private void updateQueeudMessages (JSONArray messages) {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[");
		for (int i = 0; i < messages.length(); i++) {
			stringBuilder.append(messages.optJSONObject(i).optString("message_id"));
			if (i < messages.length()-1){
				stringBuilder.append(",");
			}
		}
		stringBuilder.append("]");
		TipxApp.networkManager.updateQueuedMessages(stringBuilder.toString(), new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(
					JSONObject toParse) {
				return  new ResponseObject(toParse);
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				if (!isHasError){
					Log.e("updateQueuedMessagelog", "response = " + response.getJsonContent());
				}
			}
		});
		
	}*/
	
	private void checkForLockPasscode() {
		if (!TipxApp.userSettings.getLockPasswordNumber().isEmpty()){
			showLockPassDialog();
		}else{
			FragmentsUtil.addFragment(getSupportFragmentManager(),
					  AppContactsFragment.newInstance(), R.id.main_container);	  
		}
		
	}
	
	private void showLockPassDialog(){
		DialogManager.showLockPasswordDialog(getSupportFragmentManager(), LockPasswordDialog.MAIN_SCREEN,
				new DialogCallback(){
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog) {
				super.onDialogButtonPressed(buttonID, dialog);
				if (buttonID == R.id.close_app_btn){
					finish();
				}else{
					FragmentsUtil.addFragment(getSupportFragmentManager(),
							  AppContactsFragment.newInstance(), R.id.main_container);
				}
			}
		});
	}
	
	private static boolean openChatOnConneceted;
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkForUpdate();
		Log.d("notificationlog", "on resume");
		appSentToBackground = false;
		updateUserApplicationStatus(1);
		try {
			if (isFromNotification){
				Log.d("notificationlog", "is from notification");
				isFromNotification = false;
				if (notificationConversation != null){
					Log.d("notificationlog", "conversation not null !");
					if (TipxApp.smackManager.isConnected()){
						Log.d("notificationlog", "isConnected!" );
						getSupportFragmentManager().beginTransaction().replace(R.id.main_container,
								ChatContainerFragment.newInstance(notificationConversation)).addToBackStack(null)
								.commitAllowingStateLoss();
					}else{
						openChatOnConneceted = true;
						Log.e("notificationlog", "open chat on connected  " +  openChatOnConneceted);
					}
					
				}
			}	
		} catch (Exception e) {
			Log.e("notificationlog", "on resume exception "  + e.getMessage());
			e.printStackTrace();
		}
	}
	

	
	@Override
	protected void onPause() {
		super.onPause();
		appSentToBackground = true;
		updateUserApplicationStatus(0);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("chatloglog","ondistro");
	
		super.onDestroy();
		updateUserApplicationStatus(0);
		notificationConversation = null;
		activityISsOn = false;
		TipxApp.appIsOn = false;
		AppUtil.stopLocationService(this);
		TipxApp.smackManager.getConnection().removePacketListener(this);
		//TipxApp.smackManager.disconnect();
		conversationListener = null;
		tipxChatListener = null;
		LocalBroadcastManager.getInstance(this).unregisterReceiver(updateIUReceiver);
	}
	
	
	
	@Override
	public void onBackPressed() {
		
		if (currentFragment != null){
			if (!currentFragment.onBackPressed()){
				super.onBackPressed();
				
			}
		}else{
			super.onBackPressed();
		}
	}
	
	public static TextView setScreenTitle (String text,String status,OnClickListener listener){
		if (text != null){
			screenTitle.setText(text);
		}else{
			screenTitle.setText(TipxApp.appContext.getResources().getString(R.string.app_title));
		}
		screenTitle.setOnClickListener(listener);
		if (status == null){
			statusTxt.setVisibility(View.GONE);
		}else{
			statusTxt.setVisibility(View.VISIBLE); 
			statusTxt.setText(status);
		}
		return screenTitle;
	}
	
	
	
	
	public static void setStatusTxt (String txt){
		statusTxt.setText(txt);
	}
	
	public static void addButtonToActionBar (ActionBarButton barButton){
		buttonsContainer.addView(barButton.getView());
	}
	
	public static void removeAllActionBarButtons(){
		buttonsContainer.removeAllViews();
	}
	
	public static void removeButtonFromActionBar (View toRemove){
		buttonsContainer.removeView(toRemove);
	}
	
	public static void showProgressBar (){
		progressBar.setVisibility(View.VISIBLE);
	}
	
	public static void hideProgressBar(){
		progressBar.setVisibility(View.GONE);
	}
	
	public static void setChatListener (TipxChatListener listener){
		tipxChatListener = listener;
	}
	public static void setConversationListener (TipxChatListener listener){
		conversationListener = listener;
	}
	
	private void checkForGPS () {
		
		LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        final String PROVIDER = locationManager.getBestProvider(c, false);
    	boolean isGpsEnabled = locationManager.isProviderEnabled(PROVIDER);
    	if (!isGpsEnabled){
    		//showNoGpsBar();
    	}else{
        	//hideNoGpsBar();
    	}
    	AppUtil.startLocationService(this);
    	
    	updateIUReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					// TODO Auto-generated method stub
    	            String s = intent.getStringExtra(LocationService.COPA_MESSAGE);
    	            Log.e(LocationService.TAG, "message = " + s);
    	            if (s != null){
    	            	if (s.equalsIgnoreCase(LocationService.HIDE_NO_GPS_BAR)){
    	            		//hideNoGpsBar();
    	            	}else if(s.equalsIgnoreCase(LocationService.SHOW_NO_GPS_BAR)){
    	            		//showNoGpsBar();
    	            	}
    	            }

				}
    	 };
    	LocalBroadcastManager.getInstance(this).registerReceiver((updateIUReceiver),
	    		new IntentFilter(LocationService.COPA_RESULT));
	}
	
	
	/**
	 * check if current courier has GCM registration id saved in preference
	 * in case that is has not registration id, perform registration process in background
	 */
	private void checkForGCMRegistrationID() {
		//if(GcmManager.getInstace(this).isRegistrationIDEmpty()){
			GcmManager.getInstace(this).registerInBackground();
		//}
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	        	ToastUtil.toster("the google play service is not supported in current device !", false);
	            finish();
	        }
	        return false;
	    }
	    return true;
	}

	

	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	public void updateUserApplicationStatus (int status){
		TipxApp.networkManager.updateUserApplicationStatus(status);
	}
	
	@Override
	public void onConnectionEstablished(boolean success,
			XMPPConnection connection, Exception ex) {
		if (success){
			TipxApp.smackManager.login();
		}else{
			Log.e("smacklog", "connection exception: " + ex.getMessage() );
		}
		TipxApp.smackManager.setXmmpProperteis(TipxApp.generalSettings.getServerIP(), 
				TipxApp.generalSettings.getServerXmppPort());
		
	}

	
	@Override
	public void onLogin(boolean success, XMPPConnection connection, Exception ex) {
		if (success){
			PacketTypeFilter filter = new PacketTypeFilter(Packet.class);
			connection.addPacketListener(this, filter);
			checkForQueuedMessages();
			Log.d("notificationlog", "open chat on connected ? " + openChatOnConneceted);
			if (openChatOnConneceted){
				openChatOnConneceted = false;
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						Log.i("notificationlog","open chat fragment");
						getSupportFragmentManager().beginTransaction().replace(R.id.main_container,
								ChatContainerFragment.newInstance(notificationConversation)).addToBackStack(null)
								.commitAllowingStateLoss();
					}
				});
				
			}
		}else{
			Log.e("smacklog", "login exception: " + ex.getMessage() );
			TipxApp.smackManager.setXmmpProperteis(TipxApp.generalSettings.getServerIP(), 
					TipxApp.generalSettings.getServerXmppPort());
		}
		
	}

	
	@Override
	public void onSendMessage(XMPPConnection connection, Packet pak) {
		
		ChatMessage temp = new ChatMessageXMLParser(pak.toXML()).parse();
		
		Log.e("smackLog", "**************************************");
		Log.i("smackLog", "outgoing message. type = " 
		+ temp.getType() + "\n message: " + pak.toXML());
		Log.e("smackLog", "**************************************");
		
		
		temp.setSide(ChatMessage.SIDE_RIGHT);
		temp.setMessagePhat(ChatMessage.PHAT_TO);
		temp.setFileLoaded(true);
		TipxApp.chatManager.handleReceivedMessage(temp);
		if (tipxChatListener != null) {
			tipxChatListener.onChatMessageReceived(temp);
		}
	}

	
	@Override
	public void processPacket(Packet pak) {
		
		ChatMessage temp = new ChatMessageXMLParser(pak.toXML()).parse();
		
		Log.e("smackLog", "**************************************");
		Log.d("smackLog", "incoming message type = " +
		temp.getType() + "\n message: " + pak.toXML());
		Log.e("smackLog", "**************************************");
		
		temp.setSide(ChatMessage.SIDE_LEFT);
		temp.setMessagePhat(ChatMessage.PHAT_FROM);
		TipxApp.chatManager.handleReceivedMessage(temp);
		
		if (tipxChatListener != null) {
			if (!TipxApp.contactsManager.isContactHiden(temp.getSenderPhone())){
				tipxChatListener.onChatMessageReceived(temp);
			}
		}else{
			if (!TipxApp.contactsManager.isContactHiden(temp.getSenderPhone())){
				TipxApp.chatManager.notifyUserNewChatMessage(temp);
			}
		}
		if (conversationListener != null){
			conversationListener.onChatMessageReceived(temp);
		}
		/*if (temp.isAutoDelete()){
			Log.e("autoDeletelog", "is autoDelete ? " + temp.isAutoDelete());
			temp.startRemoveTimer();
		}*/

	}
	
	public void refreshApplication () {
		getSupportFragmentManager().popBackStack(null,
				FragmentManager.POP_BACK_STACK_INCLUSIVE);
		FragmentTransaction transition = getSupportFragmentManager()
				.beginTransaction();
		transition.replace(R.id.main_container, AppContactsFragment.newInstance());
		transition.commit();
		ToastUtil.toster("You have been removed from the group/conversation or The group/conversation has been remonved", false);
	}
	
}
