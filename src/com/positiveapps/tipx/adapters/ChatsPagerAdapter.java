/**
 * 
 */
package com.positiveapps.tipx.adapters;


import java.util.ArrayList;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.fragments.ChatFragment;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;



/**
 * @author natiapplications
 *
 */
public class ChatsPagerAdapter extends FragmentStatePagerAdapter {
	
	
	ArrayList<Conversation> conversations = new ArrayList<Conversation>();
	public ChatsPagerAdapter(FragmentManager fm) {
		super(fm);
		conversations.addAll(TipxApp.conversationManager.getConversionsAsList()) ;
	}
	
	

	@Override
	public Fragment getItem(int i) {
		
		ChatFragment chatFragment = ChatFragment.newInstance
				(conversations.get(i));
		chatFragment.setConversation(conversations.get(i));
		return chatFragment;
		
	}

	// count of pages in pager
	@Override
	public int getCount() {
		return conversations.size();
	}

	
	
}