/**
 * 
 */
package com.positiveapps.tipx.chat;


import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.json.*;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.objects.ChatMessage;

import android.app.Activity;
import android.util.Log;

/**
 * @author natiapplications
 * 
 */
public class SmackManager {
	
	public static final String TAG = "smacklog";

	public   String HOST = TipxApp.generalSettings.getServerIP();
	private  int PORT = TipxApp.generalSettings.getServerXmppPort();
	public static final String SERVICE = "@localhost";
	
	private static SmackManager instance;
	
	private Activity activity;
	private ChatListener listener;
	private XMPPConnection connection;
	private String userName;
	private String password;
	private boolean isConnected;
	private boolean tryConnectApfterUpdateFailed;
	

	
	private SmackManager (Activity activity,String userName,String password,ChatListener listener){
		this.activity = activity;
		this.userName = userName;
		this.password = password;
		this.listener = listener;
	}

	public static SmackManager getInstance(Activity activity,String userName,String password,ChatListener listener){
		if (instance == null){
			instance = new SmackManager(activity, userName, password, listener);
			return instance;
		}
		instance.setManagerProperteis(activity, listener);
		return instance;
	}
	
	private void setManagerProperteis (Activity activity,ChatListener listener){
		this.activity = activity;
		this.listener = listener;
	}
	
	public void connect() {
		ConnectionConfiguration connConfig = new ConnectionConfiguration(HOST,PORT);
		/*connConfig.setSecurityMode(SecurityMode.disabled);
		connConfig.setKeystorePath("/system/etc/security/cacerts.bks");
		connConfig.setDebuggerEnabled(true);
		connConfig.setKeystoreType("bks");*/
		//connConfig.setDebuggerEnabled(true);
		connection = new XMPPConnection(connConfig); 
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Log.e(TAG, "smack connect" );
                       
					connection.connect();
					
					if (listener != null){
						Log.e(TAG, "connection establish" +" id = "+ userName + " pass " +  password );
						listener.onConnectionEstablished(true, connection, null);
					}
				} catch (Exception ex) {
					connection = null;
					if (listener != null){
						Log.e(TAG, "smack connect ex = " +  ex.getMessage());
						listener.onConnectionEstablished(false, connection, ex);
					}
				}
			}
		}).start();
	}
	
	public  void disconnect (){
		if (connection != null){
			try {
				Log.e(TAG, "disconnect");
				connection.disconnect();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				isConnected = false;
			}
			isConnected = false;
			connection = null;
			instance = null;
		}
	}
	
	public  void login (){
		if (connection != null){
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						connection.login(userName+SERVICE,password);
						Log.e(TAG, "smack login" );
						isConnected = true;
						if (listener != null){
							Log.e(TAG, "lodin establish" );
							listener.onLogin(true, connection, null);
						}
					} catch (Exception e) {
						Log.e(TAG, "connection lodin ex = " + e.getMessage() );
						if (listener != null){
							listener.onLogin(false, connection, e);
						}
					}
				}
			}).start();
		}
	}
	
	
	public  void setPresence (boolean set){
		Presence presence = null;
		if (set){
			presence = new Presence(Presence.Type.unavailable);
			presence.setStatus("I’m unavailable");
		}else{
			presence = new Presence(Presence.Type.available);
			presence.setStatus("I’m available");
		}
		if (connection != null){
			try {
				connection.sendPacket(presence);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendMessage(Packet packet) {

		if (connection != null) {
			try {
				connection.sendPacket(packet);
			} catch (Exception e) {
				isConnected = false;
				
				e.printStackTrace();
				Log.e("smackLog", "send message ex" + e.getMessage());
			}
		}
		listener.onSendMessage(connection, packet);
	}
	
	
	public boolean isConnected(){
		if (this.connection == null){
			return false;
		}
		if (!this.isConnected){
			return false;
		}
		return connection.isConnected();
	}
	
	public XMPPConnection getConnection ( ){
		return this.connection;
	}
	
	public  void setXmmpProperteis (String ip,int port) {
		this.HOST = ip;
		this.PORT = port;
		if (!isConnected()&&!tryConnectApfterUpdateFailed){
			tryConnectApfterUpdateFailed = true;
			connect();
		}
	}
	
	public interface ChatListener {
		public void onConnectionEstablished (boolean success,XMPPConnection connection,Exception ex);
		public void onLogin (boolean success,XMPPConnection connection,Exception ex);
		public void onSendMessage(XMPPConnection connection,Packet pak);
	}

}
