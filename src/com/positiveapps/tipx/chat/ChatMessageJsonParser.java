/**
 * 
 */
package com.positiveapps.tipx.chat;

import org.json.JSONObject;

import android.util.Log;

import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.util.DateUtil;

/**
 * @author natiapplications
 *
 */
public class ChatMessageJsonParser {
	
	private static final String DATE = "date";
	private final String CODE = "code";					
	private final String MESSAGE_ID = "message_id";			
	private final String MESSAGE = "message"; 				
	private final String LAT = "lat";					
	private final String LNG = "lng";					
	private final String GROUP_ID = "to_group_id";			
	private final String GROUP_NAME = "to_group_name";			
	private final String GROUP_CODE = "to_group_code";			
	private final String GROUP_ICON = "to_group_icon";			
	private final String GROUP_ACCESS_LEVEL = "to_group_is_public";	
	private final String GROUP_ADMIN_ID = "to_group_admin_id";		
	private final String GROUP_CREATED_AT = "to_group_creates";		
	private final String MEMBER_ID = "new_member_id";		
	private final String MEMBER_FIRST_NAME = "new_member_first_name";	
	private final String MEMBER_LAST_NAME = "new_member_last_name";	
	private final String USER_ID = "to_user_id";			
	private final String FROM_USER = "from";
	private final String SENDER_ID = "id";			
	private final String SENDER_FBID = "fbid";
	private final String FIRST_NAME = "first_name";	
	private final String LAST_NAME = "last_name";	
	private final String SENDER_PHONE = "phone";
	private final String MEDIA_URL = "media_url";
	private final String AUTO_DELETE = "auto_delete";
	private final String FROM_ID = "from_id";
	
	
	private JSONObject toParse;
	
	public ChatMessageJsonParser (JSONObject toParse){
		this.toParse = toParse;
	}
	
	public ChatMessage parse (){
		
		String messgae_code = toParse.optString(CODE);
		String message_id = toParse.optString(MESSAGE_ID);
		String messgae_content = toParse.optString("text");
		String group_id = toParse.optString(GROUP_ID);
		String userId = toParse.optString(USER_ID);
		String lat = toParse.optString(LAT);
		String lng = toParse.optString(LNG);
		String mediaUrl = toParse.optString(MEDIA_URL);
		String autoDelete = toParse.optString(AUTO_DELETE);
		String fromId = toParse.optString(FROM_ID);
		String date = toParse.optString(DATE);
		Log.e("aoutodlele", "autodlelete = " + autoDelete);
		
		JSONObject fromUserJsonObject = null;
		
		
		ChatMessage temp = new ChatMessage();
		if (autoDelete != null){
			temp.setAutoDelete(!autoDelete.equals("0"));
		}
		
		
		temp.setMessagePhat(ChatMessage.PHAT_FROM);
		temp.setSide(ChatMessage.SIDE_LEFT);
		if (message_id != null && !message_id.isEmpty()){
			temp.setMessageId(Integer.parseInt(message_id));
		}
		if (messgae_code != null && !messgae_code.isEmpty()){
			temp.setType(Integer.parseInt(messgae_code));
		}
		if (group_id != null && !group_id.isEmpty()){
			temp.setGroupId(Integer.parseInt(group_id));
			temp.setChatType(ChatMessage.GROUP_MESSAGE);
		}
		if (userId != null && !userId.isEmpty()){
			temp.setSenderId(userId);
			temp.setChatType(ChatMessage.PRIVATE_MESSAGE);
		}
		if (lat != null && !lat.isEmpty()){
			temp.setLat(Double.parseDouble(lat));
		}
		if (lng != null && !lng.isEmpty()){
			temp.setLng(Double.parseDouble(lng));
		}
		temp.setFilePath(messgae_content);
		temp.setLink(mediaUrl);
		temp.setContent(messgae_content);
		//TODO  change time stamp
		Log.d("messageDateLog", "date = " + date);
		temp.setCreated(date);
		
		fromUserJsonObject = toParse.optJSONObject(FROM_USER);
		if (fromUserJsonObject != null) {
			temp.setSenderId(fromUserJsonObject.optString(SENDER_ID));
			temp.setSenderId(fromUserJsonObject.optString(SENDER_ID));
			temp.setUserName(fromUserJsonObject.optString("username"));
			temp.setSenderPhone(fromUserJsonObject.optString(SENDER_PHONE));
		}
			
		
		if (fromId != null&&!fromId.isEmpty()){
			temp.setSenderId(fromId);
		}
		return temp;
	}

}
