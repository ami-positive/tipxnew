/**
 * 
 */
package com.positiveapps.tipx.chat;

import android.util.Log;

import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.parser.BaseXMLParser;
import com.positiveapps.tipx.parser.GetTagContent;
import com.positiveapps.tipx.util.DateUtil;





/**
 * @author natiapplications
 *
 */
public class ChatMessageXMLParser implements GetTagContent {

	
	private final String MESSGAE = "message";
	private final String MESSAGE_ID = "message_id";
	private final String CODE = "code";
	private final String FROME = "from_id";
	private final String ID = "id";
	private final String LAST_NAME = "last_name";
	private final String FIRST_NAME = "first_name";
	private final String TO_GROUP_ID = "to_group_id";
	private final String GROUP_ID = "group_id";
	private final String TEXT = "text";
	private final String LAT = "lat";
	private final String LNG = "lng";
	private final String DESC = "desc";
	private final String MEDIA_URL = "media_url";
	private final String TEMP_ID = "tmp_id";
	private final String NEW_MESSAGE_ID = "new_message_id";
	private final String USER_NAME = "username";
	private final String PHONE = "phone";
	private final String USER_ID = "user_id";
	private final String TO_USER_ID = "to_user_id";
	private final String DATE = "date";
	private final String AUTO_DELETE = "auto_delete";
	  
  
	
	private String xml;
	private ChatMessage chateMessage;
	
	
	public ChatMessageXMLParser (String xml){
		this.xml = xml;
	}
	
	public ChatMessage parse (){
		if (this.xml == null){
			return null;
		}
		this.chateMessage = new ChatMessage();
		this.chateMessage.setCreated(DateUtil.getCurrentTimeAsServerString());
		BaseXMLParser baseXMLParser = new BaseXMLParser(xml, this);
		baseXMLParser.parse();
		return this.chateMessage;
	}
	
	


	
	@Override
	public void getContent(String tag, String content) {
		// TODO Auto-generated method stub
		if (tag == null){
			return;
		}
		
		if (tag.equalsIgnoreCase(MESSAGE_ID)) {
			chateMessage.setMessageId(Integer.parseInt(content));
		} else if (tag.equalsIgnoreCase(TO_GROUP_ID)) {
			chateMessage.setGroupId(Integer.parseInt(content));
			if (chateMessage.getGroupId() != 0){
				chateMessage.setChatType(ChatMessage.GROUP_MESSAGE);
			}
		} else if (tag.equalsIgnoreCase(GROUP_ID)) {
			chateMessage.setGroupId(Integer.parseInt(content));
		} else if (tag.equalsIgnoreCase(TEXT)) {
			chateMessage.setContent(content);
		} else if (tag.equalsIgnoreCase(DESC)) {
			Log.e("desclog", "filePath = " + content);
			chateMessage.setFilePath(content);
		} else if (tag.equalsIgnoreCase(CODE)) {
			chateMessage.setType(Integer.parseInt(content));
		} else if (tag.equalsIgnoreCase(ID)) {
			chateMessage.setSenderId(content);
		} else if (tag.equalsIgnoreCase(LAT)) {
			chateMessage.setLat(Double.parseDouble(content));
		} else if (tag.equalsIgnoreCase(LNG)) {
			chateMessage.setLng(Double.parseDouble(content));
		} else if (tag.equalsIgnoreCase(MEDIA_URL)) {
			chateMessage.setLink(content);
		}else if (tag.equalsIgnoreCase(TEMP_ID)) {
			chateMessage.setTempId(content);
		}else if (tag.equalsIgnoreCase(NEW_MESSAGE_ID)) {
			chateMessage.setNewMessageId(content);
		}else if (tag.equalsIgnoreCase(USER_NAME)) {
			chateMessage.setUserName(content);
		}else if (tag.equalsIgnoreCase(USER_ID)) {
			chateMessage.setSenderId(content);
		}else if (tag.equalsIgnoreCase(PHONE)) {
			chateMessage.setSenderPhone(content);
		}else if (tag.equalsIgnoreCase(DATE)) {
			Log.e("messageDateLog", "created at = " + content);
			chateMessage.setCreated(content);
		}else if (tag.equalsIgnoreCase(FROME)) {
			chateMessage.setSenderId(content);
		}else if (tag.equalsIgnoreCase(TO_USER_ID)) {
			try {
				chateMessage.setRecipientId(Integer.parseInt(content));
			} catch (Exception e) {}
		}else if (tag.equalsIgnoreCase(AUTO_DELETE)) {
			chateMessage.setAutoDelete(!content.equals("0"));
		}
		
	}

	
	@Override
	public void getAttribute(String tagName, String attrName, String attrValue) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void getEndTag(String tag) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onEndAttribute(String attrName) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onStartDucoment() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onEndDucoment() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getStartTag(String tag) {
		// TODO Auto-generated method stub
		
	}

}

