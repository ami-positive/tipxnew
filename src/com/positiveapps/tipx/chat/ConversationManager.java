/**
 * 
 */
package com.positiveapps.tipx.chat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.database.StorageManager;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.objects.GroupsList;
import com.positiveapps.tipx.objects.MemberObject;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.ToastUtil;

/**
 * @author natiapplications
 *
 */
public class ConversationManager {
	
	public static final String TAG = "ConversationManagerLog";
	
	private static ConversationManager instance;
	private ConversationManager(){
		loadConversations(null);
	};
	
	
	private HashMap<String, Conversation> conversations;
	private ArrayList<Conversation> conversationsAsList;
	
	public static ConversationManager getInstance (){
		if (instance == null){
			instance = new ConversationManager();
		}
		return instance;
	}
	
	public void loadConversations (final StorageListener callback){
		Log.d("loadConversation", "load conversaions. file name - " + TipxApp.userProfil.getUserId() + StorageManager.CONVERSATION_FILE_NAME);
		if (callback != null && conversations != null ){
			callback.onDataStorageRcived(null, getConversionsAsList());
			return;
		}
		if (TipxApp.userProfil.getUserId().isEmpty()
				||TipxApp.userProfil.getUserId().equals("0")){
			Log.e(TAG, "id is empty");
		}
		TipxApp.storageManager.readFromStorage(TipxApp.userProfil.getUserId() + StorageManager.CONVERSATION_FILE_NAME, new StorageListener() {
			
			@Override
			public void onDataStorageRcived(String type, Object object) {
				conversations = (HashMap<String, Conversation>)object;
				Log.i("loadConversation", "data found !. size = " + ((HashMap<String, Conversation>)object).size());
				if (callback != null){
					callback.onDataStorageRcived(null, getConversionsAsList());
				}
			}
			
			@Override
			public void DataNotFound(String type) {
				Log.e("loadConversation", "dataNotFound");
				conversations = new HashMap<String,Conversation>();
				if (callback != null){
					callback.onDataStorageRcived(null, getConversionsAsList());
				}
			}
		});
	}
	
	public ArrayList<Conversation> getConversionsAsList (){
		if (conversations == null){
			return new ArrayList<Conversation>();
		}
		if (conversationsAsList == null){
			conversationsAsList = new ArrayList<Conversation>();
		}
		conversationsAsList.clear();
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			if (/*conversations.get(key).getMessages() != null&&
					conversations.get(key).getMessages().size() > 0*/true){
				if (!TipxApp.contactsManager.isContactHidenById(conversations.get(key).getConversationID()))
				conversationsAsList.add(conversations.get(key));
			}
			
		}
		Collections.sort(conversationsAsList, new Comparator<Conversation>() {

			@Override
			public int compare(Conversation lhs, Conversation rhs) {
				
				Long a = rhs.getLastMessageDate();
				Long b = lhs.getLastMessageDate();
				int reult = a.compareTo(b);
				
				return reult;
			}
		});
		for (int i = 0; i < conversationsAsList.size(); i++) {
			conversationsAsList.get(i).setPostion(i);
		}
		return conversationsAsList;
		
	}
	
	
	public ArrayList<Conversation> addConversation (Conversation toAdd){
		if (conversations == null){
			return null;
		}
		conversations.put(toAdd.getConversationID(), toAdd);
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return getConversionsAsList();
	}
	
	
	public Conversation getConversation (String id){
		if (conversations == null){
			return null;
		}
		return conversations.get(id);
	}
	
	public Conversation openConversationWithContact (AppContact contact,
			ArrayList<ChatMessage> messages){
		if (conversations == null){
			return null;
		}
		Conversation result = conversations.get(contact.getContactId());
		if (result == null){
			Log.e("chatmangarlog", "contactId - " + contact.getContactId());
			result = new Conversation(ChatMessage.PRIVATE_MESSAGE, contact.getContactId(),
					messages, contact.getContactName(), contact.getContactImgaeUrl(),contact.getContactImagePath()
					,contact.getContactColor(),contact.getBackgroundPath(),contact.isBackgroundIsImage());
			addConversation(result);
		}
		return result;
	}
	
	public Conversation openGroupConversation (GroupObject group,
			ArrayList<ChatMessage> messages){
		Log.e("chatmangarlog", "groupid - " + group.getGroupId());
		if (conversations == null){
			return null;
		}
		Conversation result = conversations.get(group.getGroupId());
		Log.d("chatmangarlog", "result is null ?  - " + (result == null));
		if (result != null){
			group.setGroupColor(result.getColor());
			group.setBackgroundIsImage(result.isBackgroundIsImage());
			group.setBackgroundPath(result.getBackroundPath());
			updateConversationProperteisByGroup(group);
		}else{
			Log.e("chatmangarlog", "groupid - " + group.getImagePath());
			result = new Conversation(ChatMessage.GROUP_MESSAGE, group.getGroupId()+"",
					messages, group.getName(), group.getIcon(),group.getImagePath()
					,group.getGroupColor(),group,group.getBackgroundPath(),group.isBackgroundIsImage());
			addConversation(result);
		}
		
		return result;
	}
	
	public void updateConversationProperteisByContact (AppContact contact){
		if (conversations == null){
			return ;
		}
		
		if (conversations.get(contact.getContactId()) != null){
			conversations.get(contact.getContactId()).updateConversaionPropertyByContact(contact);
			
		}
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
	}
	
	public void updateConversationProperteisByGroup (GroupObject group){
		if (conversations == null){
			return ;
		}
		
		if (conversations.get(group.getGroupId()) != null){
			
			conversations.get(group.getGroupId()).updateConversaionPropertyByGroup(group);
			
		}
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
	}
	
	
	
	public Conversation openConversationWithConversationID (String conveID){
		if (conversations == null){
			return null;
		}
		
		return conversations.get(conveID);
	}
	
	
	
	public ArrayList<Conversation> removeConversation (String toRemoveID){
		if (conversations == null){
			return null;
		}
		Log.e("toremovelog", "toremoveid = " + toRemoveID);
		if (conversations.get(toRemoveID) != null){
			for (int i = 0; i < conversations.get(toRemoveID).getMessages().size(); i++) {
				final ChatMessage temp = conversations.get(toRemoveID).getMessages().get(i);
				if (temp.getType() == ChatMessage.TYPE_AUDIO||temp.getType() == ChatMessage.TYPE_VIDEO){
					ContentProviderUtil.deleteFileFromSdcard(temp.getFilePath());
				}
			}
		}
		conversations.remove(toRemoveID);
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return getConversionsAsList();
	}
	
	public ArrayList<Conversation> removeMessagesFromConversaionByRemovedId (String converId,String removedID){
		if (conversations == null){
			return null;
		}
		if(conversations.get(converId) == null){
			return null;
		}
		ArrayList<ChatMessage> newMessages = new ArrayList<ChatMessage>();
		for (int i = 0; i < conversations.get(converId).getMessages().size(); i++) {
			ChatMessage temp = conversations.get(converId).getMessages().get(i);
			String id = temp.getSenderId();
			if (!id.equalsIgnoreCase(removedID)){
				newMessages.add(temp);
			}else{
				if (temp.getType() == ChatMessage.TYPE_AUDIO||temp.getType() == ChatMessage.TYPE_VIDEO){
					ContentProviderUtil.deleteFileFromSdcard(temp.getFilePath());
				}
			}
		}
		conversations.get(converId).setMessages(newMessages);
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return getConversionsAsList();
	}
	
	public void updateConversationLastVissibleCountById (String conveID){
		if (conversations.get(conveID) == null){
			Log.e("countlog", " conversation is null " + conveID);
			return;
		}
		if (conversations.get(conveID).getMessages() == null){
			return;
		}
		Log.e("countlog", " idl " + conveID);
		conversations.get(conveID).setLastvisableMessagesCount
		(conversations.get(conveID).getMessages().size());
		Log.e("countlog", " idl " + conveID + " result - " + conversations.get(conveID).getUnReadMessagesCount());
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
	}
	
	public ArrayList<ChatMessage> updateMessageById (String conveID,String messageId,ChatMessage source){
		if (conversations == null){
			return null;
		}
		if (conversations.get(conveID) == null){
			return null;
		}
		
		for (int i = 0; i < conversations.get(conveID).getMessages().size(); i++) {
			if(messageId.equals(conversations.get(conveID).getMessages().get(i).getMessageId())){
				conversations.get(conveID).getMessages().get(i).update(source);
				break;
			}
		}
		TipxApp.storageManager.writeToStorage
		     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return conversations.get(conveID).getMessages();
	}
	
	public synchronized void updateMessageStatusByTempId (String tempID,ChatMessage source){
		if (conversations == null){
			return;
		}
		Log.d("sccessLog", "update by temp " + tempID);
	
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(tempID.equals(conversations.get(key).getMessages().get(i).getTempId())){
					try {
						String newID = source.getNewMessageId();
						if (newID == null){
							newID = source.getMessageId()+"";
						}
						conversations.get(key).getMessages().get(i).setMessageId(Integer.parseInt(newID));
						conversations.get(key).getMessages().get(i).setStatus(source.getStatus());
						
						Log.i("sccessLog", "update by temp" + tempID+ " new id = " + newID);
						
						TipxApp.storageManager.writeToStorage(TipxApp.userProfil.getUserId() + StorageManager.CONVERSATION_FILE_NAME,
										conversations);
						
					} catch (Exception e) {
						Log.e("sccessLog", "ex = " + e.getMessage());
						e.printStackTrace();
					}
					
					return;
				}
			}
		}
		
	}
	
	
	public boolean updateMessageStatusById (String messageID,ChatMessage source){
		if (conversations == null){
			return false;
		}
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(messageID.equals(conversations.get(key).getMessages().get(i).getMessageId()+"")){
					
					if (TipxApp.userSettings.getRemoveMessages()&&
							source.getStatus() 
							== ChatMessage.STATUS_CONFIRM_DELETATION){
						TipxApp.chatManager.handelRemoveMessage(conversations.get(key).getMessages().get(i));
					}else{
					  conversations.get(key).getMessages().get(i)
					      .setStatus(source.getStatus());
					  TipxApp.storageManager.writeToStorage
				      (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
					}
					return true;
				}
			}
		}
		return false;
	}
	
	
	public void updateMessageReadStatusById (String messageID,ChatMessage source){
		if (conversations == null){
			return;
		}
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(messageID.equals(conversations.get(key).getMessages().get(i).getMessageId()+"")){
					
					conversations.get(key).getMessages().get(i)
					      .setReadConfirmationStatus(source.getReadConfirmationStatus());
					Log.d("confirmlog", "id = " + conversations.get(key).getMessages().get(i).getMessageId() +
							" type = " + conversations.get(key).getMessages().get(i).getStatus() + " sourceStatus = " + source.getStatus());
					TipxApp.storageManager.writeToStorage
				     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
					return;
				}
			}
		}
	}
	
	public void updateMessageUploadStatusByContent (String content){
		if (conversations == null){
			return;
		}
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(content.equals(conversations.get(key).getMessages().get(i).getContent())){
					conversations.get(key).getMessages().get(i).setUploudFileCanceled(true);
					TipxApp.storageManager.writeToStorage
				     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
					return;
				}
			}
		}
	}
	
	public void removeMessageByContent (String content){
		if (conversations == null){
			return;
		}
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(content.equals(conversations.get(key).getMessages().get(i).getContent())){
					conversations.get(key).getMessages().remove(i);
					TipxApp.storageManager.writeToStorage
				     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
					return;
				}
			}
		}
	}
	
	public ArrayList<ChatMessage> addMessageByConversationID(
			final String converstionID, final ChatMessage message) {
		Log.d("addMessageLog", "add message by id" + message.getMessageId());
		if (conversations == null) {
			Log.e("addMessageLog", "conversations is null");
			loadConversations(new StorageListener() {
				@Override
				public void onDataStorageRcived(String type, Object object) {
					if (conversations != null){
						addMessageByConversationID(converstionID, message);
					}
				}
				@Override
				public void DataNotFound(String type) {
					if (conversations != null){
						addMessageByConversationID(converstionID, message);
					}
				}
			});
			return null;
		}
		Log.e("addMessageLog", "search conversation: " + (conversations.get(converstionID) == null));
		if (conversations.get(converstionID) == null) {

			if (message.getChatType() == ChatMessage.GROUP_MESSAGE) {
				TipxApp.networkManager.getMemberGroups(new NetworkCallback() {

					@Override
					public ResponseObject parseResponse(JSONObject toParse) {
						GroupsList responseObject = new GroupsList(toParse);
						return responseObject;
					}

					@Override
					public void onDataRecived(ResponseObject response,
							boolean isHasError, String erroDescription) {
						super.onDataRecived(response, isHasError,
								erroDescription);
						Log.i("createuserlog",
								"response = " + response.getJsonContent());
						if (!isHasError) {
							GroupsList result = (GroupsList) response;
							Log.i("createuserlog", "size = "
									+ result.getGroupsArray().size());
							try {
								for (int i = 0; i < result.getGroupsArray()
										.size(); i++) {
									if (result.getGroupsArray().get(i)
											.getGroupId()
											.equalsIgnoreCase(converstionID)) {
										ArrayList<ChatMessage> messages = new ArrayList<ChatMessage>();
										messages.add(message);
										TipxApp.conversationManager
												.openGroupConversation(result
														.getGroupsArray()
														.get(i), messages);
										break;
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								Log.e("createExlog", "ex = " + e.getMessage());
							}
						} else {
							ToastUtil.toster(erroDescription, false);
						}

					}
				});
			} else {

				Log.d("chatmangarlog", "conversation not found !. conveId = "
						+ converstionID);
				AppContact appContact = TipxApp.contactsManager
						.getContactByContactId(converstionID);
				if (appContact != null) {
					ArrayList<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
					chatMessages.add(message);
					openConversationWithContact(appContact, chatMessages);
					TipxApp.storageManager.writeToStorage(
							TipxApp.userProfil.getUserId()
									+ StorageManager.CONVERSATION_FILE_NAME,
							conversations);
				} else {
					if (message.getSenderPhone() != null
							&& !message.getSenderPhone().isEmpty()) {
						String senderPhone = "["
								+ message.getSenderPhone().replace("+", "")
								+ "]";
						TipxApp.networkManager.getContactsAppInfo(senderPhone,
								new NetworkCallback() {

									@Override
									public ResponseObject parseResponse(
											JSONObject toParse) {
										ResponseObject result = new ResponseObject(
												toParse);
										return result;
									}

									@Override
									public void onDataRecived(
											ResponseObject response,
											boolean isHasError,
											String erroDescription) {
										super.onDataRecived(response,
												isHasError, erroDescription);
										if (!isHasError) {
											try {
												JSONObject data = new JSONObject(
														response.getData());
												JSONArray toUpdate = data
														.optJSONArray("Contacts");

												for (int i = 0; i < toUpdate
														.length(); i++) {
													try {
														JSONObject temp = toUpdate
																.optJSONObject(i);
														int hasApp = temp
																.optInt("HasApp");
														if (hasApp > 0) {
															AppContact appContact = new AppContact();
															appContact
																	.setContactName(message
																			.getUserName());
															appContact
																	.setContactPhone(temp
																			.optString("Phone"));
															appContact
																	.setHasApplication(true);
															appContact
																	.setContactId(temp
																			.optString("ID"));
															appContact
																	.setContactImgaeUrl(temp
																			.optString("IconUrl"));
															appContact
																	.setHasApplication(true);
															appContact
																	.setLastSeen(temp
																			.optString("LastSeen"));
															appContact
																	.setContactStatusLine(temp
																			.optString("Status"));
															TipxApp.contactsManager
																	.addContact(appContact);
															ArrayList<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
															chatMessages
																	.add(message);
															openConversationWithContact(
																	appContact,
																	chatMessages);
														}
													} catch (Exception e) {
														e.printStackTrace();
													}
												}
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
								});
					}
				}
			}
			return null;
		}
		if (!isMessageAllradyExist(conversations.get(converstionID)
				.getMessages(), message.getMessageId())) {
			conversations.get(converstionID).getMessages().add(message);
			
		}

		TipxApp.storageManager.writeToStorage(TipxApp.userProfil.getUserId()
				+ StorageManager.CONVERSATION_FILE_NAME, conversations);
		return conversations.get(converstionID).getMessages();
	}
	
	public boolean isMessageAllradyExist (ArrayList<ChatMessage> messages,int idToCheck){
		for (int i = 0; i < messages.size(); i++) {
			Log.d("addMessageLog", "idToCheck = " + idToCheck + " messageID = " + messages.get(i).getMessageId());
			if (messages.get(i).getMessageId() == idToCheck){
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<ChatMessage> removeMessageByID (String conveID,String messageId){
		Log.e("removeMessageLog", "removeMessage");
		if (conversations == null){
			Log.e("removeMessageLog", "conversation is null");
			return null;
		}
		if (conversations.get(conveID) == null){
			Log.e("removeMessageLog", "id not found + " + conveID);
			return null;
		}
		
		for (int i = 0; i < conversations.get(conveID).getMessages().size(); i++) {
			if(messageId.equals(conversations.get(conveID).getMessages().get(i).getMessageId()+"")){
				if (conversations.get(conveID).getMessages().get(i).getType() 
						== ChatMessage.TYPE_AUDIO||conversations.get(conveID).getMessages().get(i)
						.getType() == ChatMessage.TYPE_VIDEO){
					ContentProviderUtil.deleteFileFromSdcard
					(conversations.get(conveID).getMessages().get(i).getFilePath());
				}
				conversations.get(conveID).getMessages().remove(i);
				Log.e("removeMessageLog", "message removed");
				break;
			}
		}
		TipxApp.storageManager.writeToStorage
		     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return conversations.get(conveID).getMessages();
	} 
	
	public void removeMessageByID (String messageId){
		if (conversations == null){
			return;
		}
		Set<String> keys = conversations.keySet();
		for (String key : keys) {
			for (int i = 0; i < conversations.get(key).getMessages().size(); i++) {
				if(messageId.equals(conversations.get(key).getMessages().get(i).getMessageId()+"")){
					if (conversations.get(key).getMessages().get(i).getType() 
							== ChatMessage.TYPE_AUDIO||conversations.get(key).getMessages().get(i)
							.getType() == ChatMessage.TYPE_VIDEO){
						ContentProviderUtil.deleteFileFromSdcard
						(conversations.get(key).getMessages().get(i).getFilePath());
					}
					conversations.get(key).getMessages().remove(i);
					TipxApp.storageManager.writeToStorage
				     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
					return;
				}
			}
		}
	} 
	
	public void removeMultypleMessages (final JSONArray toRemove){
		if (conversations == null){
			return;
		}
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < toRemove.length(); i++) {
					String temp = toRemove.optString(i);
					Set<String> keys = conversations.keySet();
					for (String key : keys) {
						for (int j = 0; j < conversations.get(key).getMessages().size(); j++) {
							if(temp.equals(conversations.get(key).getMessages().get(j).getMessageId()+"")){
								if (conversations.get(key).getMessages().get(j).getType() 
										== ChatMessage.TYPE_AUDIO||conversations.get(key).getMessages().get(j)
										.getType() == ChatMessage.TYPE_VIDEO){
									ContentProviderUtil.deleteFileFromSdcard
									(conversations.get(key).getMessages().get(j).getFilePath());
								}
								conversations.get(key).getMessages().remove(j);
								TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_CONFIRM_DELETED_MESSAGE,temp,"",true);
								TipxApp.networkManager.confirmDeletation(temp,key);
							}
						}
					}
				}
				TipxApp.storageManager.writeToStorage
			     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
			}
		}).start();
		
	}
	
	
	public ArrayList<ChatMessage> getMessagesListByConvarsationID (String conveID){
		if (conversations == null){
			return null;
		}
		if (conversations.get(conveID) == null){
			return null;
		}
		return conversations.get(conveID).getMessages();
	}
	
	public ArrayList<String> getConversationNames (){
		ArrayList<String> result = new ArrayList<String>();
		conversationsAsList = getConversionsAsList();
		if (conversationsAsList != null){
			for (int i = 0; i < conversationsAsList.size(); i++) {
				result.add(conversationsAsList.get(i).getConversationName());
			}
		}
		return result;
	}
	
	public ArrayList<AppContact> removeMemberFromGroupConversation (String conveID,String memberID){
		if (conversations == null){
			return null;
		}
		if (conversations.get(conveID) == null){
			return null;
		}
		for (int i = 0; i < conversations.get(conveID).getGroupObject().getMembers().size(); i++) {
			Log.e("removelog", "remove = " + memberID + " currernt = " + conversations.get(conveID).getGroupObject().getMembers().get(i).getId());
			if (memberID.equals(conversations.get(conveID).getGroupObject().getMembers().get(i).getId()+"")){
				Log.e("removelog", "remove = " + i);
				conversations.get(conveID).getGroupObject().getMembers().remove(i);
			}
		}
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return conversations.get(conveID).getGroupObject().getMembersAsContacts();
		
	}
	public ArrayList<AppContact> addMembersToGroupConversation (String conveID,ArrayList<MemberObject> toAdd){
		if (conversations == null){
			return null;
		}
		if (conversations.get(conveID) == null){
			return null;
		}
		conversations.get(conveID).getGroupObject().getMembers().addAll(toAdd);
		TipxApp.storageManager.writeToStorage
	     (TipxApp.userProfil.getUserId()+StorageManager.CONVERSATION_FILE_NAME, conversations);
		return conversations.get(conveID).getGroupObject().getMembersAsContacts();
		
	}
	
	public ChatMessage getMessageByConversationIdAndMessageId (String converID,String messageID){
		if (conversations == null){
			return null;
		}
		if (conversations.get(converID) == null){
			return null;
		}
		for (int i = 0; i < conversations.get(converID).getMessages().size(); i++) {
			if (conversations.get(converID).getMessages().get(i).getCurrentMessageID().equals(messageID)){
				return conversations.get(converID).getMessages().get(i);
			}
		}
		return null;
	}

}
