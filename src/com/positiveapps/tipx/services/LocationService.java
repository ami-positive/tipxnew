/**
 * 
 */
package com.positiveapps.tipx.services;





import com.positiveapps.tipx.TipxApp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * @author Nati Gabay
 *
 */
public class LocationService extends Service {

	public static final String TAG = "Location Srvice Log";
    static final public String COPA_RESULT = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED";
    static final public String COPA_MESSAGE = "UpdateMessage";
    public static final String HIDE_NO_GPS_BAR = "HideNoGpsBar";
    public static final String SHOW_NO_GPS_BAR = "ShowNoGpsBar";
    public static final String UPDATE_LOCATION = "UpdateLocation";
    public static final String NEW_LOCATION = "NewLocation";
	
	private LocationManager locationManager;
	private LocationListener locationListener;
	private LocalBroadcastManager updateMainUi;
	
	
	
	
	@Override
	public void onCreate() {
		super.onCreate();
		updateMainUi = LocalBroadcastManager.getInstance(this);
	}
	@Override
	public IBinder onBind(Intent intent) {
	
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d(TAG, "location service start");
	   
		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		addLocationListener();
		return super.onStartCommand(intent, flags, startId);
	}
	
	
	

	private void addLocationListener()
	{
	    new Thread(new Runnable(){
	        public void run(){
	            try{
	                Looper.prepare();
	               
	                while (true){
	                	Log.d(TAG, "trying to get location service!");
	                	Criteria c = new Criteria();
		                c.setAccuracy(Criteria.ACCURACY_FINE);
		                final String PROVIDER = locationManager.getBestProvider(c, false);
	                	boolean isGpsEnabled = locationManager.isProviderEnabled(PROVIDER);
	                	if (isGpsEnabled){
		                	Log.e(TAG, "location service found and started");
		                	try {
		                		locationManager.removeUpdates(locationListener);
							} catch (Exception e) {}
		                	locationListener = new MyLocationListener();
			                locationManager.requestLocationUpdates(PROVIDER, 30000*60, 0, locationListener);
			                Location location = locationManager.getLastKnownLocation(PROVIDER);
			                updateLocation(location);
			                break;
		                }
	                	Thread.sleep(30000);
	                }
	                Looper.loop();
	            }catch(Exception ex){
	                ex.printStackTrace();
	            }
	            Log.e(TAG, "add listenr thread finished");
	        }
	    }, "LocationThread").start();
	    
	}

	public static void updateLocation(Location location)
	{
		if (location == null){
			return;
		}
	    double latitude, longitude;
	    latitude = location.getLatitude();
	    longitude = location.getLongitude();
	    Log.i(TAG, "location change" + latitude+","+longitude);
	    TipxApp.generalSettings.setLat(latitude+"");
	    TipxApp.generalSettings.setLon(longitude+"");
	}


	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e(TAG, "location service stoped");
		try {
			locationManager.removeUpdates(locationListener);
		} catch (Exception e) {}
	}

	class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			Log.e(TAG, "location change");
			updateLocation(location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.i(TAG, "location status change" + status);
		}

		@Override
		public void onProviderEnabled(String provider) {
			Log.i(TAG, "location enabled" + provider);
			sendResult(HIDE_NO_GPS_BAR);
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.i(TAG, "location disabled");
			sendResult(SHOW_NO_GPS_BAR);
			addLocationListener();
		}

	}
	
	public void sendResult(String message) {
	    Intent intent = new Intent(COPA_RESULT);
	    if(message != null)
	        intent.putExtra(COPA_MESSAGE, message);
	    updateMainUi.sendBroadcast(intent);
	}
	
	

}
