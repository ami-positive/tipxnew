/**
 * 
 */
package com.positiveapps.tipx.network;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class DownloadMediaTask {

	public static final String TIPX_DIR_NAME = "/sys044908761123/temp";

	private File file;
	private String fileName;
	private String downloadUrl;
	private DownloadListener callback;
	private FileOutputStream fos;

	public DownloadMediaTask(String fileName, String url,
			DownloadListener callback) {
		this.fileName = fileName;
		this.downloadUrl = url;
		this.callback = callback;
	}

	public void download() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					File root = android.os.Environment
							.getExternalStorageDirectory();

					File dir = new File(root.getAbsolutePath() + TIPX_DIR_NAME);
					if (dir.exists() == false) {
						dir.mkdirs();
					}
					URL url = new URL(downloadUrl);
					System.out.println("TEST " + fileName);
					file = new File(dir, fileName);
					System.out.println("TEST " + file.getAbsolutePath());
					URLConnection ucon = url.openConnection();
					InputStream is = ucon.getInputStream();
					BufferedInputStream bis = new BufferedInputStream(is);
					ByteArrayBuffer baf = new ByteArrayBuffer(5000);
					int current = 0;
					while ((current = bis.read()) != -1) {
						baf.append((byte) current);
					}
					fos = new FileOutputStream(file);
					fos.write(baf.toByteArray());
					if (callback != null) {
						callback.onFileDownloaded(true, file,
								file.getAbsolutePath());
					}

				} catch (IOException e) {
					if (callback != null) {
						callback.onFileDownloaded(false, null, e.getMessage());
					}
				} finally {
					try {
						fos.flush();
						fos.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}

			}
		}).start();
	}

	public interface DownloadListener {
		public void onFileDownloaded(boolean success, File file, String phat);
	}

}
