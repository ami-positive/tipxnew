/**
 * 
 */
package com.positiveapps.tipx.network;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;

import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.util.DeviceUtil;


/**
 * @author Nati Gabay
 *
 * @description 
 */
public class NetworkManager { 
	
	
	/*********************
	 * ALL API REQUEST URLs
	 *********************/
	
	
		
	public static final String BASE_URL = "http://tipx.positive-apps.com/api/";
	//public static final String BASE_URL = "http://" +  TipxApp.generalSettings.getServerIP() +":"+
	//		TipxApp.generalSettings.getServerRequestPort() 
	//		+ "/api/";
	
	public static final String API_KEY = "3RG#3f9h3gHG%$45";
	
	/** user API **/
	private final String TIPX_CONFIGURATION = "http://tipx.positive-apps.com/api/general/get_xmpp_data";
	private final String CREATE = BASE_URL + "users/create";
	private final String VERIFY = BASE_URL + "users/verify";
	private final String DEACTIV_USER = BASE_URL +  "users/deactivateAppuser";
	private final String GET_USER_INFO = BASE_URL +  "users/get";
	private final String UPDATE_GCM_KEY = BASE_URL +  "users/updateGCM"; 
	private final String RESEND_VERIFICATION_CODE = BASE_URL +  "users/resendVerificationCode"; 
	private final String GET_CONTACTS_APP_INFO = BASE_URL +  "users/getContactsAppInfo";
	private final String UPDATE_USER = BASE_URL +"users/update";
	private final String DELETE_CONTACT = BASE_URL +"users/deleteContact";
	private final String GET_ONLINE_USERS_FRIENDS = BASE_URL + "users/getOnlineFriends";
	private final String GET_QUEUED_MESSAGES = BASE_URL + "users/queuedMessages";
	private final String UPDATE_QUEUED_MESSAGES = BASE_URL + "users/updateQueuedMessages";
	private final String UPDATE_USER_APPLICATION_STATUS = BASE_URL + "users/updateApplicationStatus";
	private final String SUBMIT_MEDIA_BASE_64 = BASE_URL + "messages/submitMediaBase64";
	public static  final String SUBMIT_MEDIA_FILE = BASE_URL + "messages/submitMedia";
	/** groups API **/
	private final String GET_MEMBER_GROUPS = BASE_URL +  "groups/memberGroups";
	
	private final String GET_MY_GROUPS = BASE_URL +  "groups/getMyGroups";
	private final String JOIN_TO_GROUP = BASE_URL +  "groups/join"; 
	private final String CREATE_GROUP = BASE_URL +  "groups/create"; 
	private final String UPDATE_GROUP = BASE_URL +  "groups/update"; 
	
	private final String ADD_MEMBER = BASE_URL +  "groups/addMember";
	private final String DELETE_GROUP = BASE_URL +  "groups/delete"; 
	private final String LEAVE_GROUP = BASE_URL +  "groups/removeMember";
	
	
	private final String SEND_DELETE_MESSAGE = BASE_URL +"messages/sendDelete";
	private final String CONFIRM_DELETATION = BASE_URL +"messages/confirmDelete";
	private final String DELETE_CHATE = BASE_URL +"messages/deleteChat";
	private final String UPDATE_MESSAGE_STATUS = BASE_URL + "users/updateMessagesStatus";
	private final String CONFIRM_MEDIA_DOWNLOADED = BASE_URL + "messages/confirmMediaDownloaded";
	
	
	
	
	/***********************
	 * ALL API REQUEST KEYs
	 ***********************/
	
	
	
	/** GENERAL KEYS **/
	public static final String KEY_API_KEY = "Api_Key";
	
	
	private final String CODE = "Code";
	public static final String ID = "ID";
	private final String DEVICE_UDID = "UDID";
	public static final String PHONE = "Phone";
	private final String APP_USER_ID = "AppuserID";
	private final String SECRET = "Secret";
	private final String CONTACTS_LIST = "ContactsList";
	
	public static final String DISPLAY_LAST_LOGIN ="DisplayLastLogin";	
	public static final String SHOW_PROFILE_IMAGE ="ShowProfileImage";	
	public static final String PRESENT_STATUS ="PresentStatus";	
	public static final String HIDDEN_CONTCTS ="HiddenContacts";	
	public static final String REMOVE_MESSAGE ="RemoveMessages";
	private final String USER_NAME = "Username";
	public static final String IMAGE = "Image";
	public static final String IMAGE_EXTENSION = "ImageExtension";
	
	private final String LAST_NAME = "LastName";
	private final String EMAIL = "Email";
	
	
	private final String GCM = "GCM";
	private final String PASSWORD = "Password";
	
	private final String PHONE_NUMBER = "Phone";
	private final String GROUPE_NAME = "Name";
	private final String TAG_LINE ="TagLine";
	private final String ICON = "Icon";
	private final String IS_PUBLIC = "deviceversion";
	private final String ADMIN_ID = "AdminID";
	private final String MEMBERS = "Members";
	
	private final String MEMBER_ID ="MemberID";
	private final String GROUP_ID ="GroupID";
	private final String MEMBER_EMAIL = "MemberEmail";
	
	
	
	public static final String YES = "yes";
	public static final String SET = "set";
	public static final String CONTENT = "content";
	private final String LAT = "Lat";
	private final String LNG = "Lng";
	
	
	public static final String KEYWORD = "Keyword";
	
	
	
	
	
	/**  PRIVATE KEYS **/
	
	public static final int MEDIA_AS_BASE_64 = 1;
	public static final int MEDIA_AS_MULTIPART_FILE = 2;
	
	
	
	
	
	
	
	
	
	
	
	
	// context of current application
	private Context cotext;
	
	// single instance of this class
	private static NetworkManager instance;
	
	// connectivity manager to check network state before send the requests
	private  ConnectivityManager connectivityManager;
	
	
	/**
	 * private constructor
	 * 
	 * @param context - context of current application
	 */
	private NetworkManager (Context context){
		this.cotext = context;
		this.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}
	
	
	/**
	 * singleton design 
	 * get the single instance of this class 
	 * 
	 * @param context - context of current application
	 * @return the single instance of this class
	 */
	public static NetworkManager getInstance (Context context){
		if (instance == null){
			instance = new NetworkManager(context);
		}
		return instance;
	}
	
	/**
	 * remove single instance when application is destroyed
	 */
	public void removeInstance (){
		instance = null;
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param dialog - progress dialog to showing in progress. this parameter is optional. is may be null
	 * @param message - string message to display on dialog. this parameter is optional. is may be null
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,JSONObject postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			RequestTask requestTask = new RequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		callback.networkUnavailable(cotext.getString(R.string.network_is_not_avalable));
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param dialog - progress dialog to showing in progress. this parameter is optional. is may be null
	 * @param message - string message to display on dialog. this parameter is optional. is may be null
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,List<NameValuePair> postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			RequestTask requestTask = new RequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_is_not_avalable));
		}
	}
	
	public void sendRequest (String URL,String postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			RequestTask requestTask = new RequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_is_not_avalable));
		}
	}
	
	public void sendSyncRequest (String URL,List<NameValuePair> postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			SyncRequestTask requestTask = new SyncRequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.start();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_is_not_avalable));
		}
	}
	
	
	
	/**
	 * checks the network state
	 * 
	 * @return false if network is not available at current time
	 */
	public  boolean isNetworkAvailable() {

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}

	}
	
	
	
	public void tipxConfiguration (NetworkCallback callback) {
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		sendRequest(TIPX_CONFIGURATION, postData,callback);
	}

	public void createUser (String phone,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		
		postData.add(new BasicNameValuePair(DEVICE_UDID, DeviceUtil.getDeviceUDID()));
		postData.add(new BasicNameValuePair(PHONE, phone));
		
		sendRequest(CREATE, postData,callback);
		
	}
	
	
	
	public void verifyVertificationCode (String code,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		
		postData.add(new BasicNameValuePair(ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(CODE, code));
		
		sendRequest(VERIFY, postData,callback);
		
	}

	public void deactiveUser (String id,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		
		postData.add(new BasicNameValuePair(ID,id));
		
		sendRequest(DEACTIV_USER, postData,callback);
		
	}
	
	public void resendVerificationCode (String id,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		
		postData.add(new BasicNameValuePair(ID,id));
		
		sendRequest(RESEND_VERIFICATION_CODE, postData,callback);
		
	}
	
	public void getUserInfo (NetworkCallback callback) {
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(SECRET,TipxApp.userProfil.getUserSecret()));
		sendRequest(GET_USER_INFO, postData,callback);
	}
	
	public void getContactsAppInfo (String contactsList,NetworkCallback callback) {
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(CONTACTS_LIST,contactsList));
		sendRequest(GET_CONTACTS_APP_INFO, postData,callback);
	}
	
	
	public void getOnlineUserFriends (String contactsList,NetworkCallback callback) {
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(SECRET,TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair("Friends",contactsList));
		sendRequest(GET_ONLINE_USERS_FRIENDS, postData,callback);
	}
	
	public void updateUserApplicationStatus (int status) {
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(SECRET,TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair("Status",String.valueOf(status)));
		sendRequest(UPDATE_USER_APPLICATION_STATUS, postData,null);
	}
	
	public void updateUser (String hiddenContacts,String imageBase64,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		String temp = "0";
		if (TipxApp.userSettings.getDisplayLastLoginTime()){
			temp="1";
		}
		postData.add(new BasicNameValuePair(DISPLAY_LAST_LOGIN, temp));
		temp = "0";
		if (TipxApp.userSettings.getShowProfilPicture()){
			temp="1";
		}
		postData.add(new BasicNameValuePair(SHOW_PROFILE_IMAGE, temp));
		temp = "0";
		if (TipxApp.userSettings.getPresentStatus()){
			temp="1";
		}
		postData.add(new BasicNameValuePair(PRESENT_STATUS, temp));
		temp = "0";
		if (TipxApp.userSettings.getRemoveMessages()){
			temp="1";
		}
		Log.e("autodlelete", "aouto - " + temp);
		postData.add(new BasicNameValuePair(REMOVE_MESSAGE, temp));
		if (hiddenContacts != null){
			postData.add(new BasicNameValuePair(HIDDEN_CONTCTS, hiddenContacts));
		}
		postData.add(new BasicNameValuePair(EMAIL, TipxApp.userProfil.getUserEmail()));
		
		postData.add(new BasicNameValuePair(USER_NAME, TipxApp.userProfil.getUserName()));
		if (imageBase64 != null){
			postData.add(new BasicNameValuePair(IMAGE,imageBase64 ));
			postData.add(new BasicNameValuePair(IMAGE_EXTENSION, "jpeg"));
		}
		sendRequest(UPDATE_USER, postData,callback);
	}
	
	
	
	public void updateGcmKey() {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(KEY_API_KEY, API_KEY));
		postData.add(new BasicNameValuePair(ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(GCM, TipxApp.generalSettings.getGCMKey()));

		sendRequest(UPDATE_GCM_KEY, postData, null);
	}
	
	
	public void submitMedia(String name,NetworkCallback callback) {

		//if (type == MEDIA_AS_BASE_64){
			List<NameValuePair> postData = new ArrayList<NameValuePair>();
			
			postData.add(new BasicNameValuePair("UserID", TipxApp.userProfil.getUserId()));
			postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
			postData.add(new BasicNameValuePair("Base64Content", name));
			sendRequest(SUBMIT_MEDIA_BASE_64, postData, callback);
		//}else if (type == MEDIA_AS_MULTIPART_FILE){
			/*if (isNetworkAvailable()){
				UploadMediaTask requestTask = new UploadMediaTask(SUBMIT_MEDIA_FILE, data,name,callback);
				requestTask.execute();
				return;
			}
			if (callback != null){
				callback.networkUnavailable(cotext.getString(R.string.network_is_not_avalable));
			}*/
		//	sendRequest(SUBMIT_MEDIA_FILE+"?UserID="+TipxApp.userProfil.getUserId(), name, callback);
		//}
		
	}
	
	
	
	
	public void createGroup(String groupName, String image, String members,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(GROUPE_NAME, groupName));
		postData.add(new BasicNameValuePair(ADMIN_ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(TAG_LINE, ""));
		postData.add(new BasicNameValuePair(IMAGE, image));
		postData.add(new BasicNameValuePair(IMAGE_EXTENSION, "jepg"));
		postData.add(new BasicNameValuePair(MEMBERS, members));
		
		sendRequest(CREATE_GROUP, postData, callback);
	}
	
	public void updateGroup(String groupId,String groupName, String image,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(GROUPE_NAME, groupName));
		postData.add(new BasicNameValuePair(ADMIN_ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(GROUP_ID, groupId));
		postData.add(new BasicNameValuePair(TAG_LINE, ""));
		if (image != null && !image.isEmpty()){
			postData.add(new BasicNameValuePair(IMAGE, image));
			postData.add(new BasicNameValuePair(IMAGE_EXTENSION, "jepg"));
		}
		
		sendRequest(UPDATE_GROUP, postData, callback);
	}
	
	public void joinToGroup(String groupCode,String memberID,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(KEY_API_KEY, API_KEY));
		postData.add(new BasicNameValuePair(CODE, groupCode));
		postData.add(new BasicNameValuePair(MEMBER_ID, memberID));
		
		sendRequest(JOIN_TO_GROUP, postData, callback);
	}
	
	public void getMyGroups(NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(KEY_API_KEY, API_KEY));
		postData.add(new BasicNameValuePair(APP_USER_ID, TipxApp.userProfil.getUserId()));
		
		sendRequest(GET_MY_GROUPS, postData, callback);
	}
	
	public void getMemberGroups(NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(MEMBER_ID,TipxApp.userProfil.getUserId()));
		
		sendRequest(GET_MEMBER_GROUPS, postData, callback);
	}
	
	public void deleteGroup(String groupID,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(ADMIN_ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(GROUP_ID,groupID));
		
		sendRequest(DELETE_GROUP, postData, callback);
	}
	
	public void leaveGroup(String memberId,String adminId,String groupID,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		if (adminId != null){
			postData.add(new BasicNameValuePair(ADMIN_ID,adminId));
		}
		
		postData.add(new BasicNameValuePair(MEMBER_ID,memberId));
		postData.add(new BasicNameValuePair(GROUP_ID,groupID));
		
		sendRequest(LEAVE_GROUP, postData, callback);
	}
	
	public void addMembers(String members,String groupID,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ADMIN_ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(MEMBER_ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(MEMBERS,members));
		postData.add(new BasicNameValuePair(GROUP_ID,groupID));
		
		sendRequest(ADD_MEMBER, postData, callback);
	}
	
	
	
	public void sendDeleteMessage(String messageID) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair("SenderID",TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("MessageID",messageID));
		
		sendRequest(SEND_DELETE_MESSAGE, postData,null);
	}

	
	public void confirmDeletation(String messageID,String recipientID) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair("RecipientID",TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("MessageID",messageID));
		
		sendRequest(CONFIRM_DELETATION, postData,null);
	}
	
	public void deleteChat(String recipienetID) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair("SenderID",TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("RecipientID",recipienetID));
		
		sendRequest(DELETE_CHATE, postData,null);
	}
	
	
	public void deleteContact(String contactID) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("ContactID",contactID));
		
		sendRequest(DELETE_CONTACT, postData,null);
	}
	
	
	public void getQueuedMessages(NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		
		sendRequest(GET_QUEUED_MESSAGES, postData,callback);
	}
	
	public void updateQueuedMessages(String messagesID,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil.getUserSecret()));
		postData.add(new BasicNameValuePair(ID,TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("Messages",messagesID));
		
		sendRequest(UPDATE_QUEUED_MESSAGES, postData,callback);
	}
	
	
			
	public void updateMessageStatus(final String messagesArrayToUpdate, final NetworkCallback callback) {

				
		final List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil
				.getUserSecret()));
		postData.add(new BasicNameValuePair(ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("Messages", messagesArrayToUpdate));
		
		sendRequest(UPDATE_MESSAGE_STATUS, postData, callback);
		
	}
	
	public void confirmMediaDownloaded(String messageID, final NetworkCallback callback) {

		
		final List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(SECRET, TipxApp.userProfil
				.getUserSecret()));
		postData.add(new BasicNameValuePair(ID, TipxApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair("MessageID", messageID));
		
		sendRequest(CONFIRM_MEDIA_DOWNLOADED, postData, callback);
		
	}

}
