package com.positiveapps.tipx.network;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.positiveapps.tipx.TipxApp;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;


@SuppressWarnings("deprecation")
public class RequestTask extends AsyncTask<ResponseObject, Void, ResponseObject>{

	private final String TAG = "RequestTaskLog";
	private String requestName = "anonimus name";
	private boolean hasPostData;
	private Context context;
	
	private String url;
	private JSONObject dataEntity;
	private List<NameValuePair> listDataEntitiy;
	private HttpEntity mediaEntity;
	private NetworkCallback callback;
	
	
	
	
	public RequestTask(Context context, String url,
			NetworkCallback callback) {
		super();
		this.context = context;
		this.url = url;
		this.callback = callback;
	}
	
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (JSONObject dataEntity){
		this.dataEntity = dataEntity;
		this.hasPostData = true;
	}
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (List<NameValuePair> dataEntity){
		this.listDataEntitiy = dataEntity;
		this.hasPostData = true;
	}
	
	public void setDataEntityToPost (String filePath){
		try {
			
			this.hasPostData = true;
			
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
					.create();
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			entityBuilder.addTextBody("UserId", TipxApp.userProfil.getUserId());
			File file = new File(filePath);
			if (file != null) {
				entityBuilder.addBinaryBody("file", file);
			}
			
			mediaEntity = entityBuilder.build();
			
			
		} catch (Exception e) {}catch (Error e) {
			Log.e("errorlog", "error = " + e.getMessage()); 
			e.printStackTrace();
		}
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		String[] requestSplit = url.split("/");
		if (requestSplit.length > 0){
			this.requestName = requestSplit[requestSplit.length-1];
		}
		Log.d(TAG, "Request name: " + requestName + " sent!" 
		+ "\n" + "Request url = " +  url);
	}
	
	
	

	@Override
	protected ResponseObject doInBackground(ResponseObject... params) {
		// TODO Auto-generated method stub
		
		ResponseObject result = null;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			HttpConnectionParams.setSoTimeout(httpParameters, 30000);
			
			HttpClient httpClient = new DefaultHttpClient(httpParameters);

			HttpPost post = new HttpPost(this.url);

			if (hasPostData) { 
				
				if (dataEntity != null){
					 post.setEntity(new StringEntity(dataEntity.toString(),HTTP.UTF_8)); 
				}else if (listDataEntitiy != null){
					post.setEntity(new UrlEncodedFormEntity(listDataEntitiy,HTTP.UTF_8));
				}else if (mediaEntity != null){
		            post.setEntity(mediaEntity);
				}
		       
				if (mediaEntity == null){
					String postData = EntityUtils.toString(post.getEntity()); 
			        Log.d(TAG, "data post over request: " + postData);
				}
		        
			}
			HttpResponse response = httpClient.execute(post);
			String responseString = "";
			
			responseString = EntityUtils.toString(response.getEntity());
			Log.e(TAG, "response string: " + responseString);
			JSONObject jsonObject = new JSONObject(responseString);
			if (this.callback != null){
				result = this.callback.parseResponse(jsonObject);
			}
			
			
		} catch (final Exception e) {
			Log.e(TAG, "exception: " + e.getMessage());
			e.printStackTrace();
		} catch (final Error er){
			Log.e(TAG, "error: " + er.getMessage());
			er.printStackTrace();
		}
		
		return result;
	}
	
	
	@Override
	protected void onPostExecute(ResponseObject result) {
		
		super.onPostExecute(result);
	
		if (result != null){
			Log.i(TAG, "result object" + result.toString());
			if (callback != null){
				callback.onDataRecived(result,result.isHasError(),result.getErrorDesc());
			}
		}else{
			Log.i(TAG, "result is null");
			if (callback != null){
				callback.onError();
			}
		}
	}
	

}
