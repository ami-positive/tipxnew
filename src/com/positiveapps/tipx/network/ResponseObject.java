/**
 * 
 */
package com.positiveapps.tipx.network;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @author natiapplications
 *
 */
public class ResponseObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ERROR = "error";
	public static final String ERROR_DESC = "errdesc";
	public static final String DATA = "data";
	
	
	private boolean isHasError;
	private int error;
	private String errorDesc;
	private String stringContent;
	private String data;
	
	public ResponseObject() {
		super();
	}
	
	public ResponseObject(String toParse) {
		super();
		stringContent = toParse;
	}
	
	public ResponseObject (JSONObject toParse){
		error = toParse.optInt(ERROR,0);
		this.stringContent = toParse.toString();
		if (error > 0){
			isHasError = true;
			errorDesc = toParse.optString(ERROR_DESC);
		}else{
			isHasError = false;
			JSONObject jsonData = toParse.optJSONObject(DATA);
			if (jsonData != null){
				this.data = jsonData.toString();
			}
		}
	}

	/**
	 * @return the isHasError
	 */
	public boolean isHasError() {
		return isHasError;
	}

	/**
	 * @param isHasError the isHasError to set
	 */
	public void setHasError(boolean isHasError) {
		this.isHasError = isHasError;
	}

	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	/**
	 * @return the jsonContent
	 */
	public String getJsonContent() {
		return stringContent;
	}

	/**
	 * @param jsonContent the jsonContent to set
	 */
	public void setJsonContent(String jsonContent) {
		this.stringContent = jsonContent;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the error
	 */
	public int getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(int error) {
		this.error = error;
	}
	
	
	

}
