package com.positiveapps.tipx;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.positiveapps.tipx.ui.TouchImageView;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ToastUtil;

public class ImagePrivewActivity extends FragmentActivity {

	
	public static final String EXTRA_PATH_TYPE = "ExtraPathType";
	public static final String EXTRA_PATH = "ExtraPath";
	
	
	public static final int PATH_TYPE_LOCAL = 0;
	public static final int PATH_TYPE_URL = 1;
	
	private int pathType;
	private String imagePath;
	private TouchImageView ivPrivewImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		AppUtil.makeFullScreenActivity(this);
		AppUtil.setPerventFromUserTakingScreenShote(this);
		setContentView(R.layout.activity_image_privew);
		
		
		
		ivPrivewImage = (TouchImageView)findViewById(R.id.iv_priview);
		
		if (loadExtras ()){
			fillImage();
		}else{
			ToastUtil.toster("No given image path found ! ", true);
			finish();
		}
	}

	/**
	 * 
	 */
	private void fillImage() {
		if (pathType ==  PATH_TYPE_LOCAL){
			BitmapUtil.loadImageIntoByStringPath(BitmapUtil.IMAGE_TYPE_FROM_CAMERA,
					imagePath, ivPrivewImage,0, 1,R.drawable.ic_action_picture);
		}else{
			BitmapUtil.loadImageIntoByUrl(imagePath, ivPrivewImage, R.drawable.ic_action_picture,
					R.drawable.ic_action_person, 0, 0, null);
		}
		
	}

	/**
	 * 
	 */
	private boolean loadExtras() {
		if (getIntent().getExtras() != null){
			try {
				this.pathType = getIntent().getExtras().getInt(EXTRA_PATH_TYPE);
				this.imagePath = getIntent().getExtras().getString(EXTRA_PATH);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	
}
