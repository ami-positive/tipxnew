/**
 * 
 */
package com.positiveapps.tipx.sms;

import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.fragments.login.RegistrationFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * @author natiapplications
 * 
 */
public class SMSReceiver extends BroadcastReceiver {

	final SmsManager sms = SmsManager.getDefault();

	@Override
	public void onReceive(Context context, Intent intent) {
		final Bundle bundle = intent.getExtras();

		try {

			if (bundle != null) {

				final Object[] pdusObj = (Object[]) bundle.get("pdus");

				for (int i = 0; i < pdusObj.length; i++) {

					SmsMessage currentMessage = SmsMessage
							.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage
							.getDisplayOriginatingAddress();

					String senderNum = phoneNumber;
					String message = currentMessage.getDisplayMessageBody();
					Log.e("smslog", "sender num - " + senderNum + " body - " + message) ;
					if (senderNum.contains("Tipx")){
						if (LoginActivity.currentFragment != null){
							if (LoginActivity.currentFragment instanceof RegistrationFragment){
								RegistrationFragment registrationFragment = (RegistrationFragment)LoginActivity.currentFragment;
								registrationFragment.showVerificationCodeRecived(message);
							}
						}
					}
					
				}
			}

		} catch (Exception e) {
		}

	}
}
