package com.positiveapps.tipx.database;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.content.Context;
import android.util.Log;


public class WriteToStorageThread extends Thread {
	
	
	private Context context;
	private FileOutputStream fos;
	private ObjectOutputStream oos;
	
	private String fileName;
	private Serializable toWrite;
	
	
	public WriteToStorageThread(Context context,Serializable toWrite,
			 String fileName) {
		super();
		this.context = context;
		this.fileName = fileName;	
		this.toWrite = toWrite;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		writeMessage ();
		
	}
	
	private  void writeMessage (){
		try {
			fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(toWrite);
			fos.flush();
			oos.flush();
			
		} catch (Exception e) {
			Log.i("storage", "error with writing thread" + e.getMessage());
		}finally{
			
			try {
				if (fos != null){
					fos.close();
				}if (oos != null){
					oos.close();
				}
			} catch (IOException e) {}
			
		}
	}
	
	

}
