/**
 * 
 */
package com.positiveapps.tipx.database;

import java.io.Serializable;

import com.positiveapps.tipx.TipxApp;

import android.util.Log;


/**
 * @author Nati Gabay
 *
 */
public class StorageManager {
	
	private final String TAG = "storatelog";
	private static  String BASE_NAME ;
	public static String ICONS_FILE_NAME = "icons_file_name";
	public static String CHAT_MESSAGES_LIST_FILE_NAME = "chat_messages_list_file_name";
	public static String WEATHER_FILE_NAME = "Weather_file_name";
	public static String SAVED_SITE_FILE_NAME = "Saved_site_file_name";
	public static String SITES_DATA_FILE_NAME = "Sites_data_file_name";
	
	public static String CONVERSATION_FILE_NAME = "Conversation_file_name";
	public static String APP_CONTACTS_LIST_FILE_NAME = "App_contacts_file_name";
	public static String GROUPS_FILE_NAME = "Groups_file_name";

	
	private static StorageManager instance;
	
	private StorageManager () {
		
	}
	
	public static StorageManager getInstance () {
		if (instance == null){
			instance = new StorageManager();
		}
		
		
		return instance;
	}
	
	public static void removeInstance () {
		instance = null;
	}
	
	public void readFromStorage (String fileName, StorageListener callback) {
		Log.d(TAG, fileName);
		new ReadFromStorageThread(TipxApp.appContext, fileName, callback).start();
	}
	
	public void writeToStorage (String fileName, Serializable toWrite){
		Log.e(TAG, fileName);
		new WriteToStorageThread(TipxApp.appContext, toWrite, fileName).start();
	}

}
