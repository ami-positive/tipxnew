package com.positiveapps.tipx.database;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import android.content.Context;
import android.os.Handler;
import android.util.Log;


public class ReadFromStorageThread extends Thread {
	
	
	private Context context;
	private FileInputStream fis;
	private ObjectInputStream ois;
	private StorageListener listener;
	
	
	private String fileName;
	private Serializable result;
	
	public ReadFromStorageThread(Context context,
			 String fileName,StorageListener listener) {
		super();
		this.context = context;
		this.fileName = fileName;
		this.listener = listener;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		read ();
	}
	
	private synchronized void read (){
		Handler mainHandler = new Handler(context.getMainLooper());

		try {
			fis = context.openFileInput(fileName);
			ois = new ObjectInputStream(fis);
			this.result = (Serializable) ois.readObject();
			mainHandler.post(new Runnable() {
				
				@Override
				public void run() {
					listener.onDataStorageRcived(fileName,result);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			Log.i("storage", "error with reading thread" + e.getMessage());
			mainHandler.post(new Runnable() {
				
				@Override
				public void run() {
					listener.DataNotFound(fileName);
				}
			});
		}finally{
			try {
				if (fis != null){
					fis.close();
				}if (ois != null){
					ois.close();
				}
			} catch (Exception e) {}
			
		}
	}
	
	
	
	
	

}
