package com.positiveapps.tipx;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.ToastUtil;

public class SplashActivity extends FragmentActivity implements OnClickListener {

	
	private TextView appVersionTxt;
	private boolean activityIsOn;
	private Button retry;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeFullScreenActivity(this);
		setContentView(R.layout.activity_splash);
		activityIsOn =  true;
		appVersionTxt = (TextView)findViewById(R.id.app_version);
		appVersionTxt.setText(appVersionTxt.getText().toString() + " " + TipxApp.appVersion);
		retry = (Button)findViewById(R.id.retry);
		retry.setVisibility(View.GONE);
		retry.setOnClickListener(this);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				
				
				if (TipxApp.userProfil.getUserId().isEmpty()||TipxApp.userProfil.getUserSecret().isEmpty()){
					if (activityIsOn){
						startActivity(new Intent(SplashActivity.this,LoginActivity.class));
						finish();
					} 
				}else{
					getTipxConfiguration();
				}
				
			}
		}, 2000);
		
		
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityIsOn = false;
	}

	
	private void getUserInfo(){
		TipxApp.networkManager.tipxConfiguration(null);
		TipxApp.networkManager.getUserInfo(new NetworkCallback(){ 
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				UserProfile responseObject = new UserProfile(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				Log.i("createuserlog", "response = " + response.getJsonContent());
				if (!isHasError){
					if (activityIsOn){
						
						startActivity(new Intent(SplashActivity.this,MainActivity.class));
						finish();
					} 
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				retry.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				retry.setVisibility(View.VISIBLE);
			}
		});
	}


	
	@Override
	public void onClick(View v) {
		retry.setVisibility(View.GONE);
		getUserInfo();
		
	}
	
	private void getTipxConfiguration (){
		TipxApp.networkManager.tipxConfiguration(new NetworkCallback(){
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				TipxApp.networkManager.tipxConfiguration(null);
				try {
					if (!isHasError){
						JSONObject data = new JSONObject(response.getData());
						TipxApp.generalSettings.setServerIP(data.getString("ServerIP"));
						TipxApp.generalSettings.setServerRequestPort(data.optString("APIPort"));
						TipxApp.generalSettings.setServerXmppPort(Integer.parseInt(data.optString("Port","5224")));
						TipxApp.generalSettings.setServerNewVerstion(data.optString("Version"));
						getUserInfo();
					}else{
						getUserInfo();
					}
					
				} catch (Exception e) {
					getUserInfo();
				}

			}
			
			@Override
			public void onError() {
				super.onError();
				getUserInfo();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				getUserInfo();
			}
		});
			
		
	}
	
}
