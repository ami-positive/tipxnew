/**
 * 
 */
package com.positiveapps.tipx.fragments;



import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.RecordingVideoActivity;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.adapters.ChatsPagerAdapter;
import com.positiveapps.tipx.adapters.ConversationsListAdapter;
import com.positiveapps.tipx.animations.DepthPageTransformer;
import com.positiveapps.tipx.chat.TipxChatListener;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.login.ProfileFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.NetworkManager;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.network.UploadMediaTask;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.ui.PagerTabIndicator;
import com.positiveapps.tipx.ui.PagerTabIndicator.onTabSelectedListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DateUtil;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;




/**
 * @author natiapplications
 *
 */
public class ChatContainerFragment extends BaseFragment implements OnPageChangeListener,TipxChatListener,onTabSelectedListener {
	
	public static final String EXTRA_CONVERSATION = "Conversation";

	
	public static boolean closeFragmentAfterPrformRemoving;
	
	
	private LinearLayout pagerTabIndicatorContainer;
	private ViewPager chatsPager;
	private Conversation conversation;
	public static ChatFragment currentChatFragment;
	private PagerTabIndicator pagerTabIndicator;
	private ChatsPagerAdapter adapter;
	public static ChatContainerFragment containerInstance;
	private ImageView infoImage;
	int pos;
	private ArrayList<String> chatNames;
	
	public ChatContainerFragment() {

	}

	public static ChatContainerFragment newInstance(Conversation conversation) {
		ChatContainerFragment instance = new ChatContainerFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_CONVERSATION, conversation);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_chat_container, container,
				false);
		if (conversation == null){
			conversation = (Conversation)getArguments().getSerializable(EXTRA_CONVERSATION);
		}
		if (chatNames == null){
			chatNames = TipxApp.conversationManager.getConversationNames();
		}
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		containerInstance = this;
		MainActivity.setScreenTitle("Chat",null,null);
		pagerTabIndicatorContainer = (LinearLayout)view.findViewById(R.id.indicator_holder);
		chatsPager = (ViewPager)view.findViewById(R.id.pager);
		chatsPager.setOffscreenPageLimit(TipxApp.conversationManager.getConversationNames().size());
		
		TipxApp.appPreference.getUserProfil().setNotificationCount(0);
		NotificationManager mNotificationManager = (NotificationManager) 
				MainActivity.mainInstance.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();
		
		if (closeFragmentAfterPrformRemoving){
			closeFragmentAfterPrformRemoving= false;
			getFragmentManager().popBackStack();
		}else{
			setUpActionBarButtons();
			setUpChatsPager();
		}
		
	}
	
	
	
	public void setUpChatsPager() {
		
		adapter = new ChatsPagerAdapter(getChildFragmentManager());
		chatsPager.setAdapter(adapter);
		
		//if (updatePager){
			chatNames = TipxApp.conversationManager.getConversationNames();
		//}
		if (currentChatFragment != null){
			conversation = currentChatFragment.getConversation();
		}
		Log.e("position log", "pos = " + pos);
		
		pos = conversation.getPostion();
		
		Log.d("position log", "pos = " + pos);
		
		if (pos>0&&pos>=adapter.getCount()){
			pos = adapter.getCount()-1;
			conversation = TipxApp.conversationManager.getConversionsAsList().get(pos);
		}
		
		pagerTabIndicatorContainer.removeAllViews();
		pagerTabIndicator = new PagerTabIndicator(getActivity(), chatsPager,chatNames, pos,this);
		pagerTabIndicatorContainer.addView(pagerTabIndicator.getView());
		
		
		chatsPager.setOnPageChangeListener(this);
		chatsPager.setCurrentItem(pos);
		onPageSelected(pos);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if (fragIsOn)
				pagerTabIndicator.setPosition(pos);
			}
		}, 500);
		
		
	}

	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		MainActivity.setChatListener(null);
		containerInstance = null;
		currentChatFragment = null;
		//updatePager = false;
		closeFragmentAfterPrformRemoving = false;
	}
	
	private void setUpActionBarButtons (){
		MainActivity.removeAllActionBarButtons();
		MainActivity.hideProgressBar();
		ActionBarButton contactBtn = new ActionBarButton(getActivity(), R.drawable.empty, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						conversation = currentChatFragment.getConversation();
						if (currentChatFragment.getConversation().getConversationType() == ChatMessage.GROUP_MESSAGE){
							GroupObject groupObject = currentChatFragment.getConversation().getGroupObject();
							if (groupObject != null){
								FragmentsUtil.openFragmentDownToUp(getFragmentManager(),
										GroupProfileFragment.newInstance(groupObject), R.id.main_container);
							}
						}else{
							AppContact contact = TipxApp.contactsManager.
									getContactByContactId(currentChatFragment.getConversation().getConversationID());
							if (contact != null){
								FragmentsUtil.openFragmentDownToUp(getFragmentManager(),
										ContactProfileFragment.newInstance(contact), R.id.main_container);
							}
						}
					}
				});
		infoImage = contactBtn.getImageView();
		MainActivity.addButtonToActionBar(contactBtn);
	}
	
	
	
	@Override
	public void onPageSelected(int arg0) {
		pagerTabIndicator.setPosition(arg0);
		
		MainActivity.setChatListener(this);
		ChatFragment frag = (ChatFragment)adapter.getItem(arg0);
		
		if (frag != null){
			String status = "status: ";
        	if (frag.getConversation().getConversationType() == ChatMessage.GROUP_MESSAGE){
        		status = frag.getConversation().getGroupObject().getMemnbersAsString();
        	}else{
        		AppContact currentContact = TipxApp.contactsManager.
                        getContactByContactId(conversation.getConversationID());
        		if (currentContact != null){
        			int stat = currentContact.getContactStatus();
            		if (stat == AppContact.STATUS_OFFLINE){
            			status = currentContact.getLastSeen();
            		}else{
                		status = AppContact.STATUS_DISPLAY[currentContact.getContactStatus()];
            		}
        		}
        	}
        	MainActivity.setScreenTitle(frag.getConversation().getConversationName(),status,null);
        	if (infoImage != null){
        		updateInfoImage(frag.getConversation());
        	}
		}
		try {
			ChatFragment.currentDisplayPopup.dismiss();
		} catch (Exception e) {}
		
	}
	
	
	private void updateInfoImage (Conversation conversation){
		int placeHolder = 0;
		if (conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
			placeHolder = R.drawable.ic_action_about_w;
		}else{
			placeHolder = R.drawable.ic_action_about_w;
		}
		
		String picturePath = conversation.getImagePath();
		File file = null;
		if (picturePath != null && !picturePath.isEmpty()){
			file = new File(picturePath);
		}else{
			picturePath = conversation.getIconUrl();
		}
		if (file != null){
			picturePath = conversation.getIconUrl();
			BitmapUtil.loadImageIntoByFile(file,infoImage,
					placeHolder,placeHolder,
					60, 60, null);
		}else{
			picturePath = conversation.getIconUrl();
			BitmapUtil.loadImageIntoByUrl(picturePath, infoImage,
					placeHolder,placeHolder,
					60, 60, null);
		}
	}
	
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		if (currentChatFragment != null){
			currentChatFragment.cancelRecordingIfNeeded();
		}
	}
	
	@Override
	public void onPageScrollStateChanged(int arg0) {}

	
	@Override
	public void onChatMessageReceived(ChatMessage msg) {
		currentChatFragment.onChatMessageReceived(msg);
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		MainActivity.appSentToBackground = false;
		if (data == null){
			data = getActivity().getIntent();
		}
		Log.i("onactivityresultlog", "onActivityResult");
		if(resultCode != getActivity().RESULT_OK){// the user chose do nothing
			return;
		}
		switch (requestCode) {
		case ChatFragment.SELECT_PHOTO_FROM_GALLERY:
			Uri dataUri = null;
			String path = "";
			if (data != null && data.getData() != null) {
				dataUri = data.getData();
				path = BitmapUtil.getPathOfImagUri(dataUri);
			}
			if (dataUri != null){
				Log.i("onactivityresultlog", "onActivityResult");
				Bitmap image = BitmapUtil.lowQualityOfImage(path, 2);
				image = BitmapUtil.rotate(BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,dataUri, image);
				String imageAsBase64 = BitmapUtil.getImageAsStringEncodedBase64
						(image);
				UploadMediaTask uploadImage = new UploadMediaTask(ChatMessage.TYPE_IMAGE, imageAsBase64,
						currentChatFragment.getConversation().getConversationType(),
						currentChatFragment.getConversation().getConversationID(),
						currentChatFragment);
				uploadImage.uploadAsBase64(path);
			}else{
				ToastUtil.toster("error while prossesing image", true);
			}
			break;
		case ChatFragment.CAPTURE_IMAGE:
			Uri cameraUri = ChatFragment.currentImageUri;
			String cameraPath = cameraUri.getPath();
			if (cameraUri != null){
				Log.i("onactivityresultlog", "onActivityResult");
				Bitmap image = BitmapUtil.lowQualityOfImage(cameraPath, 8);
				image = BitmapUtil.rotate(BitmapUtil.IMAGE_TYPE_FROM_CAMERA,cameraUri, image);/*******************/
				String imageAsBase64 = BitmapUtil.getImageAsStringEncodedBase64
						(image);
				UploadMediaTask uploadImage = new UploadMediaTask(ChatMessage.TYPE_IMAGE, imageAsBase64,
						currentChatFragment.getConversation().getConversationType(),
						currentChatFragment.getConversation().getConversationID(),
						currentChatFragment);
				uploadImage.uploadAsBase64(cameraPath);
			}else{
				ToastUtil.toster("error while prossesing image", true);
			}
			
			break;
		case ChatFragment.SELECT_VIDEO_RROM_GALLERY:
		case ChatFragment.RECORD_VIDEO:
			if (data == null ) {
				ToastUtil.toster("error while prossesing video", true);
				break;
			}
			
			String videoFileName = "";
			if (requestCode == ChatFragment.RECORD_VIDEO){
				videoFileName = data.getExtras().getString(RecordingVideoActivity.RESULT_DATA);
			}else{
				if (data.getData() == null){
					return;
				}
				videoFileName = BitmapUtil.getPathOfImagUri(data.getData());;
			}
			Log.i("onactivityresultlog", "onActivityResult path - " + videoFileName);
			
			UploadMediaTask upluadVideo = new UploadMediaTask(ChatMessage.TYPE_VIDEO, videoFileName,
					currentChatFragment.getConversation().getConversationType(),
					currentChatFragment.getConversation().getConversationID(),
					currentChatFragment);
			
			upluadVideo.uploudAsMultypart();
			
			ChatMessage temp = createTempMediaMessage(ChatMessage.TYPE_VIDEO,videoFileName);
			TipxApp.chatManager.handleReceivedMessage(temp);
			if (MainActivity.tipxChatListener != null) {
				MainActivity.tipxChatListener.onChatMessageReceived(temp);
			}
			break;
		case ChatFragment.SELECT_AUDIO_FROM_GALLERY:
		case ChatFragment.RECORD_AUDIO:
			if (data == null || data.getData() == null) {
				ToastUtil.toster("error while prossesing audio", true);
				break;
			}
			
			String audioFileName = BitmapUtil.getPathOfImagUri(data.getData());
			UploadMediaTask uploudAudio = new UploadMediaTask(ChatMessage.TYPE_AUDIO, audioFileName,
					currentChatFragment.getConversation().getConversationType(),
					currentChatFragment.getConversation().getConversationID(),
					currentChatFragment);
			
			uploudAudio.uploudAsMultypart();
			
			ChatMessage temp2 = createTempMediaMessage(ChatMessage.TYPE_AUDIO,audioFileName);
			TipxApp.chatManager.handleReceivedMessage(temp2);
			if (MainActivity.tipxChatListener != null) {
				MainActivity.tipxChatListener.onChatMessageReceived(temp2);
			}
			break;

		}
	}
	
	public static ChatMessage createTempMediaMessage (int type,String uri) {
		ChatMessage result = new ChatMessage();
		result.setMessageId(AppUtil.getRundomIntBetween(100000, 1000000));
		result.setMessagePhat(ChatMessage.PHAT_TO);
		result.setType(type);
		result.setContent(uri);
		result.setCreated(DateUtil.getCurrentTimeAsServerString());
		result.setFileLoaded(false);
		result.setChatType(currentChatFragment.getConversation().getConversationType());
		result.setLink("");
		result.setSenderId(TipxApp.userProfil.getUserId());
		result.setUserName(TipxApp.userProfil.getUserName());
		result.setStatus(ChatMessage.STATUS_SENT);
		result.setSide(ChatMessage.SIDE_RIGHT);
		result.setSenderPhone(TipxApp.userProfil.getUserPhone());
		if (currentChatFragment != null){
			if (currentChatFragment.getConversation().getConversationType() == ChatMessage.GROUP_MESSAGE){
				result.setGroupId(Integer.parseInt
						(currentChatFragment.getConversation().getConversationID()));
			}else{
				result.setRecipientId(Integer.parseInt
						(currentChatFragment.getConversation().getConversationID()));
			}
		}
		result.setUploudFileCanceled(false);
		result.setFilePath(uri);
		return result;
	}

	
	@Override
	public void onTabSelected(PagerTabIndicator tab, int position) {
		onPageSelected(position);
	}


}
