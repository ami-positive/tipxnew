/**
 * 
 */
package com.positiveapps.tipx.fragments;





import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.os.AsyncTaskCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.braunster.tutorialview.object.Tutorial;
import com.braunster.tutorialview.object.TutorialBuilder;
import com.braunster.tutorialview.object.TutorialIntentBuilder;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.dialogs.AppDialogButton;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.login.ProfileFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;




/**
 * @author natiapplications
 *
 */
public class AppContactsFragment extends BaseFragment implements TextWatcher,OnItemClickListener,OnItemLongClickListener,OnClickListener{
	
	private final int REQUEST_CONTACTS_LIST = 2000;
	

	private View listHeader;
	private TextView headerText;
	private EditText searchEt;
	private LinearLayout headerContainer;
	private ListView contactsListView;
	private AppContactsListAdapter adapter;
	
	private boolean isOnSearchResult;
	private ArrayList<AppContact> searchArray;
	private ArrayList<AppContact> currentArray;
	private TextView screenTitle;
	private ImageView addContactBtn;
	
	
	public AppContactsFragment() {

	}

	public static AppContactsFragment newInstance() {
		AppContactsFragment instance = new AppContactsFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_app_contacts, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		screenTitle = 
				MainActivity.setScreenTitle(TipxApp.appContext.getString(R.string.contacts_screen_title),null,this);
		contactsListView = (ListView)view.findViewById(R.id.contacts_list);
		listHeader = getActivity().getLayoutInflater().inflate(R.layout.layout_app_contacts_list_header, null);
		headerText = (TextView)listHeader.findViewById(R.id.header_text);
		searchEt = (EditText)view.findViewById(R.id.search_et);
		headerContainer = (LinearLayout)view.findViewById(R.id.header_container);
		getActivity().getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		searchEt.addTextChangedListener(this);
		
		
		contactsListView.setOnItemClickListener(this);
		contactsListView.setOnItemLongClickListener(this);
		contactsListView.addHeaderView(listHeader);
		setUpActionBarButtons();
		updateContactListView(TipxApp.contactsManager.getAppContactsList());
		setAppContactStatus(currentArray);
		getOnlineContacts(currentArray);
	}
	
	
	
	@Override
	public void startViewAnimations(View view) {
		super.startViewAnimations(view);
		showActionsExplanationIfNeeded();
	}
	
	/**
	 * 
	 */
	private void showActionsExplanationIfNeeded() {
		if (!TipxApp.generalSettings.isContactScreenExplanationShown()){
			TutorialIntentBuilder builder = new TutorialIntentBuilder(getActivity());
			TutorialBuilder tBuilder = new TutorialBuilder();
			tBuilder.setBackgroundColor(getResources().getColor(R.color.blue_trans));
			
			ArrayList<Tutorial> tutorials = new ArrayList<Tutorial>();
			tutorials.add(AppUtil.createTutorial(screenTitle,
					getString(R.string.explanation_contact_plus)));
			tutorials.add(AppUtil.createTutorial(addContactBtn,
					getString(R.string.explanation_contact_title)));
			builder.skipTutorialOnBackPressed(true);
			builder.setWalkThroughList(tutorials);
			TipxApp.generalSettings.setContactScreenExplanationShow(true);
			startActivity(builder.getIntent());
			getActivity().overridePendingTransition(R.anim.dummy, R.anim.dummy);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CONTACTS_LIST){
			if(resultCode == Activity.RESULT_OK){
				if (data.getData() != null){
					AppContact selected = ContentProviderUtil.getContactDetailseByUri(data.getData());
					if (selected == null){
						ToastUtil.toster("your selected contact has invalid phone number", false);
						return;
					}
					if (isContactAllradyAdded(selected)){
						ToastUtil.toster("your selected contact allrady added", false);
						return;
					}
					
					updateContactListView(TipxApp.contactsManager.addContact(selected));
					setAppContactStatus(currentArray);
				}
			}
		}
	}
	
	private boolean isContactAllradyAdded (AppContact contact){
		for (int i = 0; i < TipxApp.contactsManager.getAppContactsList().size(); i++) {
			if (contact.getContactPhone().replace("+", "")
					.equals(TipxApp.contactsManager.getAppContactsList().get(i).getContactPhone().replace("+", ""))){
				return true;
			}
		}
		return false;
	}
	
	private void setUpActionBarButtons (){
		MainActivity.removeAllActionBarButtons();
		MainActivity.showProgressBar();
		ActionBarButton saveBtn = new ActionBarButton(getActivity(), R.drawable.ic_action_new, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						showContactChooserDialog();
					}
				});
		addContactBtn = saveBtn.getImageView();
		MainActivity.addButtonToActionBar(saveBtn);
	}
	
	
	

	
	protected void setUpContacts() {
		TipxApp.contactsManager.readContactsList(new StorageListener() {
			@Override
			public void onDataStorageRcived(String type, Object object) {
				updateContactListView((ArrayList<AppContact>)object);
				setAppContactStatus(TipxApp.contactsManager.getAppContactsList());
			}
			@Override
			public void DataNotFound(String type) {}
		});
	}

	public void updateContactListView (ArrayList<AppContact> contacts){
		currentArray = contacts;
		
		if (isOnSearchResult){
			if(contacts.size() == 0){
				headerContainer.removeAllViews();
				try{
					headerContainer.addView(listHeader);
				}catch(Exception e){}catch (Error e) {}
				
				headerText.setText("No result found");
			}else{
				try {
					headerContainer.removeAllViews();
				} catch (Exception e) {}
			}
		}else{
			if (TipxApp.contactsManager.getAppContactsList().size() > 0){
				//setAppContactStatus(TipxApp.contactsManager.getAppContactsList());
				try {
					contactsListView.removeHeaderView(listHeader);
				} catch (Exception e) {}
			}
		}
		
		adapter = new AppContactsListAdapter(getActivity(), currentArray);
		contactsListView.setAdapter(adapter);
		
		
	}

	private void showContactChooserDialog() {
		String [] optionsText = getActivity().getResources().getStringArray(R.array.contacts_chooser_options);
		DialogManager.showDialogChooser(this,optionsText, new DialogCallback(){
			
			@Override
			protected void onDialogOptionPressed(int which,
					DialogFragment dialog) {
				super.onDialogOptionPressed(which, dialog);
				switch (which) {
				case 0:
					addAllContactsFromPhone();
					break;
				case 1:
					ContentProviderUtil.openContactListToChoose(AppContactsFragment.this, REQUEST_CONTACTS_LIST);
					break;
				case 2:
					showCreateContactDialog();
					break;
				}
			}
			
		});
		
	}
	
	private void addAllContactsFromPhone(){ 
					
					new AsyncTask<Void, Void, ArrayList<AppContact>>() {
						
						@Override
						protected void onPreExecute() {
							// TODO Auto-generated method stub
							super.onPreExecute();
							showProgressDialog("Loading contacts...");
						}

						@Override
						protected ArrayList<AppContact> doInBackground(Void... params) {
							ArrayList<AppContact> contacts = ContentProviderUtil.getAllContactsFromPhone();
							return contacts;
						}
						
						@Override
						protected void onPostExecute(ArrayList<AppContact> contacts) {
							// TODO Auto-generated method stub
							super.onPostExecute(contacts);
							updateContactListView(TipxApp.contactsManager.addMultipleContacts(contacts));
							currentArray = contacts;
							setAppContactStatus(currentArray); 
							dismisProgressDialog();
						}
					}.execute();
					
					
					
					
	}
	
	private void showCreateContactDialog () {
		DialogManager.showCreateNewContactDialog(getFragmentManager(), new DialogCallback(){
			
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog, Object Extra) {
				super.onDialogButtonPressed(buttonID, dialog, Extra);
				if (buttonID == R.id.btn_create){
					AppContact appContact = (AppContact)Extra;
					if (isContactAllradyAdded(appContact)){
						ToastUtil.toster("your selected contact allrady added", false);
						return;
					}
					updateContactListView(TipxApp.contactsManager.addContact(appContact));
					setAppContactStatus(currentArray);
				}
			}
		});
	}
	

	private void setAppContactStatus (ArrayList<AppContact> toSet){
		MainActivity.showProgressBar();
		AppContact.checkIfContactHasApplication(toSet,new NetworkCallback(){

			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				MainActivity.hideProgressBar();

				if (!isHasError){
					try {
						JSONObject data = new JSONObject(response.getData());
						JSONArray contactsResult = data.optJSONArray("Contacts");
						if (contactsResult.length() > 0){
							updateContactListView
							(TipxApp.contactsManager.updateContactsStatus(contactsResult));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				MainActivity.hideProgressBar();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				MainActivity.hideProgressBar();
			}
		});
	}

	
	private void getOnlineContacts (ArrayList<AppContact> toSet){
		MainActivity.showProgressBar();
		AppContact.getOnlineContacts(toSet,new NetworkCallback(){

			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				MainActivity.hideProgressBar();

				if (!isHasError){
					try {
						JSONObject data = new JSONObject(response.getData());
						JSONArray contactsResult = data.optJSONArray("ApplicationStatusFriends");
						
						updateContactListView
						(TipxApp.contactsManager.updateContactsPresentStatus(contactsResult));
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				MainActivity.hideProgressBar();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				MainActivity.hideProgressBar();
			}
		});
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {}

	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}

	
	@Override
	public void afterTextChanged(Editable s) {
		if (searchEt.getText().toString().isEmpty()){
			updateContactListView(TipxApp.contactsManager.getAppContactsList());
			isOnSearchResult = false;
		}else{
			isOnSearchResult = true;
			search(searchEt.getText().toString());
		}
		
	}

	public void search (String keyWord){
		searchArray = new ArrayList<AppContact>();
		ArrayList<AppContact> core = TipxApp.contactsManager.getAppContactsList();
		for (int i = 0; i < core.size(); i++) {
			AppContact temp = core.get(i);
			if(StringUtils.containsIgnoreCase(temp.getContactName(), keyWord)||
					StringUtils.containsIgnoreCase(temp.getContactPhone(), keyWord)){
				searchArray.add(temp);
			}
		}
		updateContactListView(searchArray);
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AppContact selected = currentArray.get(position);
		FragmentsUtil.openFragmentDownToUp(getFragmentManager(), ContactProfileFragment.newInstance(selected),
				R.id.main_container);
	}

	
	private void showChoosActionDialog(final AppContact contact) {
		
		final int CHAT = 1;
		final int EDIT_CONTACT = 2;
		final int DELET_CONVERSATION = 3;
		final int DELET_CONTACT = 4;
		final int CANCEL = 5;
		final int INVITE = 6;
		AppDialogButton[]  buttons = null;
		if (contact.isHasApplication()){
			buttons = new AppDialogButton[5];
			buttons[0] = new AppDialogButton
					(CHAT, R.drawable.blue_dialog_btn_selector, "Chat");
			buttons[1] = new AppDialogButton
					(EDIT_CONTACT, R.drawable.blue_dialog_btn_selector, "Edit Contact");
			buttons[2] = new AppDialogButton
					(DELET_CONVERSATION, R.drawable.blue_dialog_btn_selector, "Delete Conversation");
			buttons[3] = new AppDialogButton
					(DELET_CONTACT, R.drawable.blue_dialog_btn_selector, "Delete Contact");
			buttons[4] = new AppDialogButton
					(CANCEL, R.drawable.blue_dialog_btn_selector, "Cancel");
			
		}else{
			buttons = new AppDialogButton[4];
			buttons[0] = new AppDialogButton
					(INVITE, R.drawable.blue_dialog_btn_selector, "Invite");
			buttons[1] = new AppDialogButton
					(EDIT_CONTACT, R.drawable.blue_dialog_btn_selector, "Edit Contact");
			buttons[2] = new AppDialogButton
					(DELET_CONTACT, R.drawable.blue_dialog_btn_selector, "Delete Contact");
			buttons[3] = new AppDialogButton
					(CANCEL, R.drawable.blue_dialog_btn_selector, "Cancel");
		}
		
		
		DialogManager.showAppDialog(getFragmentManager(), "Choose Action", 
				"",R.drawable.blue_dialog_icon_shape, R.drawable.ic_action_person,
				buttons, true,new DialogCallback(){
			
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog) {
				super.onDialogButtonPressed(buttonID, dialog);
						switch (buttonID) {
						case CHAT:
							final ChatContainerFragment frag = ChatContainerFragment.newInstance
							(TipxApp.conversationManager.openConversationWithContact(contact,
									new ArrayList<ChatMessage>()));
							
							FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
									frag, R.id.main_container);
							
							break;
						case EDIT_CONTACT:
							FragmentsUtil.openFragmentDownToUp(getFragmentManager(), ContactProfileFragment.newInstance(contact),
									R.id.main_container);
							break;
						case DELET_CONVERSATION:
							TipxApp.conversationManager.removeConversation(contact.getContactId());
							ToastUtil.toster("conversation with " + contact.getContactName() + " has been removed", false);
							break;
						case DELET_CONTACT:
							TipxApp.contactsManager.removeContactByContactPhone(contact.getContactPhone());
							TipxApp.conversationManager.removeConversation(contact.getContactId());
							ToastUtil.toster(contact.getContactName() + " has been removed", false);
							adapter.notifyDataSetChanged();
							break;
						case CANCEL:break;
						case INVITE:
							String message = getString(R.string.invite_message).replace
							("*", "http://googl.play.stam.hitcavanti.app.store.stam.ze.hacolstam.com");
							ContentProviderUtil.openSMSScreen(getActivity(), message,contact.getContactPhone());
							break;
						}
			}
		});
		
	}
	
	
	@Override
	public boolean onBackPressed() {
		if(isOnSearchResult){
			isOnSearchResult = false;
			searchEt.setText("");
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		if (TipxApp.isFaceMood){
			return false;
		}
		AppContact selected = currentArray.get(position);
		showChoosActionDialog(selected);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		if (TipxApp.isFaceMood){
			return;
		}
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				ConversationsFragment.newInstance(), R.id.main_container);
		
	}
}
