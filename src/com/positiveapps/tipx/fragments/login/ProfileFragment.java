/**
 * 
 */
package com.positiveapps.tipx.fragments.login;

import java.util.ArrayList;

import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.PostActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.BaseFragment;
import com.positiveapps.tipx.fragments.ChoosMembersFragment;
import com.positiveapps.tipx.fragments.HidenContactFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.MemberObject;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DeviceUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;





/**
 * @author natiapplications
 *
 */
public class ProfileFragment extends BaseFragment implements OnClickListener,OnCheckedChangeListener {
	
	
	private static  final String EXTRA_FROM = "ExtraFrom";
		
	private static final int REQUEST_MEMBER_CHHOSER = 2001;
	private final int REQUEST_GALLERY = 1000;
	private final int REQUEST_CONTACTS_LIST = 2000;
	
	private LinearLayout lockPassItem;
	private LinearLayout hidenContactsItem;
	private LinearLayout logOutItem;
	private ImageView userPicture;
	private EditText userNameTv;
	private TextView editTv;
	private TextView connectionStatus;
	private TextView lockPassTv;
	private TextView appVersionTv;
	private EditText statusEt;
	private Switch displayLastTimeSwitch;
	private Switch showProfilPictureSwitch;
	private Switch presentStatusSwitch;
	private Switch removeMessageSwitch;
	private Switch sendReadConfirmation;
	private Button tryConnect;
	private String imageBase64;
	private String hiddenContact;
	private boolean isFromMainActivity;
	
	public ProfileFragment() {

	}

	public static ProfileFragment newInstance(boolean isfromMainActivity) {
		ProfileFragment instance = new ProfileFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean(EXTRA_FROM, isfromMainActivity);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_profile, container,
				false);
		isFromMainActivity = getArguments().getBoolean(EXTRA_FROM);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		if (isFromMainActivity){
			MainActivity.currentFragment = this;
			MainActivity.setScreenTitle(TipxApp.appContext.getString(R.string.profile_screen_title),null,null);
		}else{
			LoginActivity.currentFragment = this;
			LoginActivity.setScreenTitle(TipxApp.appContext.getString(R.string.profile_screen_title));
		}
		
		
		setUpActionBarButtons();
		
		lockPassItem = (LinearLayout)view.findViewById(R.id.lock_password_item);
		hidenContactsItem = (LinearLayout)view.findViewById(R.id.hiden_contacts_item);
		logOutItem = (LinearLayout)view.findViewById(R.id.log_out);
		userPicture = (ImageView)view.findViewById(R.id.image_profile);
		userNameTv = (EditText)view.findViewById(R.id.user_name_txt);
		editTv = (TextView)view.findViewById(R.id.edit_txt);
		lockPassTv = (TextView)view.findViewById(R.id.lock_pass_txt);
		connectionStatus = (TextView)view.findViewById(R.id.connection_status);
		appVersionTv = (TextView)view.findViewById(R.id.app_version);
		statusEt = (EditText)view.findViewById(R.id.status_txt);
		displayLastTimeSwitch = (Switch)view.findViewById(R.id.switch_display_login);
		showProfilPictureSwitch = (Switch)view.findViewById(R.id.switch_show_picture);
		presentStatusSwitch = (Switch)view.findViewById(R.id.switch_present_status);
		removeMessageSwitch = (Switch)view.findViewById(R.id.switch_remove_message);
		sendReadConfirmation = (Switch)view.findViewById(R.id.switch_send_read_confirmation);
		tryConnect = (Button)view.findViewById(R.id.try_connection);
		
		tryConnect.setOnClickListener(this);
		logOutItem.setOnClickListener(this);
		lockPassItem.setOnClickListener(this);
		hidenContactsItem.setOnClickListener(this);
		userPicture.setOnClickListener(this);
		editTv.setOnClickListener(this);
		
		fillScreenData();
		
		displayLastTimeSwitch.setOnCheckedChangeListener(this);
		showProfilPictureSwitch.setOnCheckedChangeListener(this);
		presentStatusSwitch.setOnCheckedChangeListener(this);
		removeMessageSwitch.setOnCheckedChangeListener(this);
		sendReadConfirmation.setOnCheckedChangeListener(this);
		
	}

	private void setUpActionBarButtons (){
		if (isFromMainActivity){
			MainActivity.removeAllActionBarButtons();
			ActionBarButton saveBtn = new ActionBarButton(getActivity(), R.drawable.empty, "Save",
					new OnActionBarButtonClickListener() {
						
						@Override
						public void onActionBarButtonClick(View v) {
							TipxApp.userProfil.setUserNaem(userNameTv.getText().toString());
							TipxApp.userProfil.setUserStatus(statusEt.getText().toString());
							saveProfileData();
						}
					});
			MainActivity.addButtonToActionBar(saveBtn);
		}else{
			LoginActivity.removeAllActionBarButtons();
			ActionBarButton saveBtn = new ActionBarButton(getActivity(), R.drawable.empty,TipxApp.appContext.getString(R.string.save),
					new OnActionBarButtonClickListener() {
						
						@Override
						public void onActionBarButtonClick(View v) {
							TipxApp.userProfil.setUserNaem(userNameTv.getText().toString());
							TipxApp.userProfil.setUserStatus(statusEt.getText().toString());
							saveProfileData();
						}
					});
			LoginActivity.addButtonToActionBar(saveBtn);
		}
		
	}
	
	private void saveProfileData (){
		showProgressDialog();
		TipxApp.networkManager.updateUser(hiddenContact, imageBase64, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				if (!isHasError){
					if (!isFromMainActivity){
						getActivity().startActivity(new Intent(getActivity(),MainActivity.class));
						getActivity().finish();
					}else{
						ToastUtil.toster("Your details have been updated successfully", false);
						getFragmentManager().popBackStack();
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				dismisProgressDialog();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}
	
	/**
	 * 
	 */
	private void fillScreenData() {
		appVersionTv.setText(appVersionTv.getText().toString() + " " 
				+AppUtil.getApplicationVersion(getActivity()));
		if (TipxApp.smackManager != null&&TipxApp.smackManager.isConnected()){
			connectionStatus.setTextColor(getResources().getColor(R.color.green_5));
			connectionStatus.setText("Connected");
			tryConnect.setVisibility(View.GONE);
		}else{
			connectionStatus.setTextColor(getResources().getColor(R.color.red_3));
			connectionStatus.setText("Disconnected");
			if (isFromMainActivity){
				tryConnect.setVisibility(View.VISIBLE);
			}else{
				tryConnect.setVisibility(View.GONE);
			}
		}
		String picturePath = TipxApp.userProfil.getUserImagePath();
		if (!picturePath.isEmpty()){
			BitmapUtil.loadImageIntoByStringPath(BitmapUtil.IMAGE_TYPE_FROM_CAMERA,picturePath,
					userPicture, 160, 2, R.drawable.ic_action_person);
		}else{
			picturePath = TipxApp.userProfil.getUserIconUrl();
			BitmapUtil.loadImageIntoByUrl(picturePath, userPicture,
					R.drawable.ic_action_person, R.drawable.ic_action_person,
					150, 150, null);
		}
		userNameTv.setText(TipxApp.userProfil.getUserName());
		statusEt.setText(TipxApp.userProfil.getUserStatus());
		if (!TipxApp.userSettings.getLockPasswordNumber().isEmpty()){
			lockPassTv.setText(getString(R.string.on));
		}else{
			lockPassTv.setText(getString(R.string.off));
		}
		displayLastTimeSwitch.setChecked(TipxApp.userSettings.getDisplayLastLoginTime());
		showProfilPictureSwitch.setChecked(TipxApp.userSettings.getShowProfilPicture());
		presentStatusSwitch.setChecked(TipxApp.userSettings.getPresentStatus());
		removeMessageSwitch.setChecked(TipxApp.userSettings.getRemoveMessages());
		sendReadConfirmation.setChecked(TipxApp.userSettings.getSendReadConfirmation());
		
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_GALLERY){
			if(resultCode == Activity.RESULT_OK){
				String path = BitmapUtil.getPathOfImagUri(data.getData());
				TipxApp.userProfil.setUserImagePath(path);
				imageBase64 = BitmapUtil.getImageAsStringEncodedBase64
						(BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,data.getData(),BitmapFactory.decodeFile(path));
				BitmapUtil.loadImgaeIntoBySelectedUri(BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,
						data.getData(), userPicture, 160, 2, R.drawable.ic_action_person);
			}
		}else if (requestCode == REQUEST_MEMBER_CHHOSER){
			if (resultCode == Activity.RESULT_OK){
				if (data != null){
					Bundle extras = data.getExtras();
					if  (extras != null){
						@SuppressWarnings("unchecked")
						ArrayList<AppContact> selectedContact = 
								(ArrayList<AppContact>)extras.getSerializable
								(ChoosMembersFragment.RESULT_DATA);
						if (selectedContact != null){
							Log.e("selectedContactLog", "contact no null");
							showProgressDialog();
							
							
						}
					}
				}
			}
		}
		
	}

	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		TipxApp.userProfil.setUserNaem(userNameTv.getText().toString());
		TipxApp.userProfil.setUserStatus(statusEt.getText().toString());
	}


	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		
		case R.id.lock_password_item:
			checkForLockPasscode();
			break;
		case R.id.try_connection:
			if (isFromMainActivity){
				TipxApp.smackManager.connect();
				showProgressDialog();
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						dismisProgressDialog();
						if (TipxApp.smackManager.isConnected()){
							connectionStatus.setTextColor(getResources().getColor(R.color.green_5));
							connectionStatus.setText("Connected");
							tryConnect.setVisibility(View.GONE);
						}else{
							connectionStatus.setTextColor(getResources().getColor(R.color.red_3));
							connectionStatus.setText("Disconnected");
							if (MainActivity.mainInstance != null){
								tryConnect.setVisibility(View.VISIBLE);
							}else{
								tryConnect.setVisibility(View.GONE);
							}
						}
						
					}
				}, 5000);
			}
			break;
		case R.id.log_out:
			logOut();
			break;
		case R.id.hiden_contacts_item:
			int idPlaceHolder = 0;
			if (isFromMainActivity){
				idPlaceHolder = R.id.main_container;
			}else{
				idPlaceHolder = R.id.login_container;
			}
			FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), HidenContactFragment.newInstance(),
					idPlaceHolder);
			break;
		case R.id.image_profile:
		case R.id.edit_txt:
			ContentProviderUtil.openGallery(this, REQUEST_GALLERY);
			break;
		}
		
	}
	

	/**
	 * 
	 */
	private void logOut() {
		TipxApp.userProfil.setUserId("");
		TipxApp.userProfil.setUserSecret("");
		startActivity(new Intent(getActivity(),LoginActivity.class));
		getActivity().finish();
	}

	private void checkForLockPasscode() {
		if (TipxApp.userSettings.getLockPasswordNumber().isEmpty()){
			if (!isFromMainActivity){
				FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
						SetLockPasswordFragment.newInstance(), R.id.login_container);
			}else{
				FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
						SetLockPasswordFragment.newInstance(), R.id.main_container);
			}
		}else{
			showLockPassDialog();
		}
		
	}
	
	private void showLockPassDialog(){
		DialogManager.showLockPasswordDialog(getFragmentManager(), LockPasswordDialog.SET_NEW_PASSCODE,
				new DialogCallback(){
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog) {
				super.onDialogButtonPressed(buttonID, dialog);
				if (!isFromMainActivity){
					FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
							SetLockPasswordFragment.newInstance(), R.id.login_container);
				}else{
					FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
							SetLockPasswordFragment.newInstance(), R.id.main_container);
				}
			}
		});
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.switch_display_login:
			TipxApp.userSettings.setDisplayLastLoginTime(isChecked);
			break;
		case R.id.switch_show_picture:
			TipxApp.userSettings.setShowProfilePicture(isChecked);
			break;
		case R.id.switch_present_status:
			TipxApp.userSettings.setPresentStatus(isChecked);
			break;
		case R.id.switch_remove_message:
			TipxApp.userSettings.setRemoveMessages(isChecked);
			break;
		case R.id.switch_send_read_confirmation:
			TipxApp.userSettings.setSendReadConfirmation(isChecked);
			break;
		}
		
	}

	


}
