/**
 * 
 */
package com.positiveapps.tipx.fragments;

import java.util.ArrayList;

import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import com.positiveapps.tipx.LoginActivity;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.PostActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.adapters.AppContactsListAdapter.OnRemovePressedLestener;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.BaseFragment;
import com.positiveapps.tipx.fragments.login.SetLockPasswordFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.objects.MemberObject;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DeviceUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;





/**
 * @author natiapplications
 *
 */
public class HidenContactFragment extends BaseFragment implements OnRemovePressedLestener{
	
	
	
	private final int REQUEST_MEMBER_CHHOSER = 2000;
	
	private TextView noHidenContactsTxt;
	private ListView hidenContactsListView;
	private AppContactsListAdapter adapter;
	
	
	public HidenContactFragment() {

	}

	public static HidenContactFragment newInstance() {
		HidenContactFragment instance = new HidenContactFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_hiden_contact, container,
				false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.currentFragment = this;
		MainActivity.setScreenTitle(TipxApp.appContext.getString(R.string.hiden_contact_screen_title),null,null);
		
		setUpActionBarButtons();
		hidenContactsListView = (ListView)view.findViewById(R.id.hiden_list_view);
		noHidenContactsTxt = (TextView)view.findViewById(R.id.no_hiden_txt);
		checkForLockPasscode();
	}

	private void setUpActionBarButtons() {

		MainActivity.removeAllActionBarButtons();
		ActionBarButton saveBtn = new ActionBarButton(getActivity(),
				R.drawable.ic_action_new, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						Intent intent = new Intent(getActivity(),PostActivity.class);
						intent.putExtra(PostActivity.EXTRA_TYPE, PostActivity.TYPE_MEMBER_CHOOSER);
						startActivityForResult(intent, REQUEST_MEMBER_CHHOSER);
					}
				});
		MainActivity.addButtonToActionBar(saveBtn);

	}
	
	private void checkForLockPasscode() {
		if (!TipxApp.userSettings.getLockPasswordNumber().isEmpty()){
			showLockPassDialog();
		}else{
			updateContactsListView(TipxApp.contactsManager.getHidenContacts());
		}
		
	}
	
	private void showLockPassDialog(){
		DialogManager.showLockPasswordDialog(getFragmentManager(), LockPasswordDialog.SET_NEW_PASSCODE,
				new DialogCallback(){
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog) {
				super.onDialogButtonPressed(buttonID, dialog);
				if (buttonID == R.id.close_app_btn){
					getFragmentManager().popBackStack();
				}else{
					updateContactsListView(TipxApp.contactsManager.getHidenContacts());
				}
			}
		});
	}
	
	public void updateContactsListView (ArrayList<AppContact> contacts){
		if (contacts == null){
			noHidenContactsTxt.setVisibility(View.VISIBLE);
			return;
		}
		if (contacts.size() == 0){
			noHidenContactsTxt.setVisibility(View.VISIBLE);
		}else {
			noHidenContactsTxt.setVisibility(View.GONE);
		}
		adapter = new AppContactsListAdapter(getActivity(), contacts);
		adapter.setListAsChooser("",null);
		adapter.setRemoveListner(this);	
		hidenContactsListView.setAdapter(adapter);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_MEMBER_CHHOSER){
			if (resultCode == Activity.RESULT_OK){
				if (data != null){
					Bundle extras = data.getExtras();
					if  (extras != null){
						ArrayList<AppContact> selectedContact = 
								(ArrayList<AppContact>)extras.getSerializable
								(ChoosMembersFragment.RESULT_DATA);
						if (selectedContact != null){
							for (int i = 0; i < selectedContact.size(); i++) {
								boolean save = false;
								if (i == selectedContact.size()-1){
									save = true;
								}
								TipxApp.contactsManager.setHidenContact(selectedContact.get(i).getContactPhone(), true,save);
							}
							updateContactsListView(TipxApp.contactsManager.getHidenContacts());
						}
					}
				}
			}
		}
		
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	
	@Override
	public void onRemovePressed(AppContact contact) {
		TipxApp.contactsManager.setHidenContact(contact.getContactPhone(), false,true);
		updateContactsListView(TipxApp.contactsManager.getHidenContacts());
	}



}
