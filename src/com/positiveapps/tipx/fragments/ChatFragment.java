/**
 * 
 */
package com.positiveapps.tipx.fragments;




import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.positiveapps.tipx.ImagePrivewActivity;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.animations.HeightAnimation;
import com.positiveapps.tipx.chat.TipxChatListener;
import com.positiveapps.tipx.chat.TipxChatManager;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.network.DownloadMediaTask;
import com.positiveapps.tipx.network.DownloadMediaTask.DownloadListener;
import com.positiveapps.tipx.network.UploadMediaTask;
import com.positiveapps.tipx.network.UploadMediaTask.OnFileUploaded;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.Conversation;
import com.positiveapps.tipx.objects.MemberObject;
import com.positiveapps.tipx.ui.ChatMessageView.OnDeletePressedListener;
import com.positiveapps.tipx.ui.ChatMessageView.onChatItemClickListener;
import com.positiveapps.tipx.ui.ChatMessageView;
import com.positiveapps.tipx.ui.InfoMessage;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DateUtil;
import com.positiveapps.tipx.util.SoundUtil;
import com.positiveapps.tipx.util.ToastUtil;





/**
 * @author natiapplications
 *
 */
public class ChatFragment extends Fragment implements TipxChatListener,OnClickListener,onChatItemClickListener,OnDeletePressedListener
,TextWatcher,OnFileUploaded{
	
	
	public static final String EXTRA_CONVERSATION = "Conversation";

	public static final int SELECT_PHOTO_FROM_GALLERY = 9000;
	protected static final int SELECT_VIDEO_RROM_GALLERY = 8000;
	protected static final int SELECT_AUDIO_FROM_GALLERY = 7000;
	protected static final int RECORD_AUDIO = 4000;
	protected static final int RECORD_VIDEO = 5000;
	protected static final int CAPTURE_IMAGE = 6000;
	
	/**
     * user interface properties
     */
	private ImageView chatBg;
	private LinearLayout loadOldMessages;
	private ScrollView messagesScroll;
	private LinearLayout messagesContainer;
    private EditText messageEt;
    private Button sendBtn;
    private ImageView addAttachmentBtn;
    private Button recordBtn;
    private ImageView recordingIcon;
    private TextView recordTime;
    private LinearLayout recordContainer;
    private ImageView cancelRecording;
    private Button sendRecord;
    
    /**
     * date box properties
     */
	private int index;
	private int oldDate;
	private int newDate;
	
	/**
     * delete pupup properties
     */
	public static PopupWindow currentDisplayPopup;
	
	
	/**
     * record sound properties
     */
	private boolean isRecording;
	private  String recordingFileName = null;
	private final int FULL_WIDTH = TipxApp.generalSettings.getScreenWidth();
	private final int MAX_RECORDING_LENGTH = 180;
    private MediaRecorder mRecorder = null;
    private CountDownTimer countDownTimer;
    
    /**
     * update typing status properties
     */
    private long idle_min = 2000;
    private long last_text_edit = 0;
    private Handler h = new Handler();
    private boolean already_queried = false;
    private boolean isSendTypingMessage;
	
    
    /**
     * fragment data properties
     */
    private Conversation conversation;
    private int lastMessagesCount;
	private ArrayList<ChatMessageView> viewArray = new ArrayList<ChatMessageView>();
	
	
	public HashMap<String, String> tempColors;
	
	private Handler partnerStatusHandler = new Handler();
	
	private Runnable updatePartnerStatus = new Runnable() {
		
		@Override
		public void run() {
			try {
				setStatusHeader();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	
	public ChatFragment() {

	}

	public static ChatFragment newInstance(Conversation conversation) {
		ChatFragment instance = new ChatFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_CONVERSATION, conversation);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_chat, container,
				false);
		conversation = (Conversation)getArguments().getSerializable(EXTRA_CONVERSATION);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		chatBg = (ImageView) view.findViewById(R.id.chat_bg);
		setBackground();
		
		addAttachmentBtn = (ImageView)view.findViewById(R.id.media_btn);
		sendBtn = (Button)view.findViewById(R.id.send_btn);
		messageEt = (EditText)view.findViewById(R.id.message_et);
		
		 
		
		messagesScroll = (ScrollView)view.findViewById(R.id.message_scroll);
		messagesContainer = (LinearLayout)view.findViewById(R.id.message_container);
		recordBtn = (Button)view.findViewById(R.id.record_btn);
		recordingIcon = (ImageView)view.findViewById(R.id.record_now_iv);
		recordContainer = (LinearLayout)view.findViewById(R.id.record_container);
		recordTime = (TextView)view.findViewById(R.id.record_time);
		cancelRecording = (ImageView)view.findViewById(R.id.cancel_recording);
		sendRecord = (Button)view.findViewById(R.id.send_record_btn);
		messageEt.addTextChangedListener(this);
		
		
		sendRecord.setOnClickListener(this);
		recordBtn.setOnClickListener(this);
		cancelRecording.setOnClickListener(this);
		messageEt.setOnClickListener(this);
		sendBtn.setOnClickListener(this);
		addAttachmentBtn.setOnClickListener(this);
		tempColors = conversation.getTempColors();
		//setUpActionBarButtons();
		
		
		loadMessagesIntoScrollList();
		
	}
	
	
	public void loadMessagesIntoScrollList () {
		
		index = 0;
		oldDate = 0;
		
		if (lastMessagesCount <=0){
			lastMessagesCount = 0;
			messagesContainer.setVisibility(View.INVISIBLE);
			new DrawItemRunnable(conversation.getMessages()).run();
		}else{
			for (int i = 0; i < conversation.getMessages().size(); i++) {
				ChatMessage temp = conversation.getMessages().get(i);
				updateScrollMessages(temp,true);
				if (temp.getType() == ChatMessage.TYPE_VIDEO||temp.getType() == ChatMessage.TYPE_AUDIO){
					if (temp.getMessageId() == 0&&temp.isUploudFileCanceled()){
						UploadMediaTask uploadMediaTask = new UploadMediaTask(ChatMessage.TYPE_VIDEO, temp.getContent(),
								getConversation().getConversationType(),
								getConversation().getConversationID(),
								this);
						uploadMediaTask.uploudAsMultypart();
					}
				}
			}
			try {
				messageEt.setText(TipxApp.generalSettings.getLastText(
						getConversation().getConversationID()));
				Log.i("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
						" message: " +  TipxApp.generalSettings.getLastText(
								getConversation().getConversationID()));
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
						" message: " +  TipxApp.generalSettings.getLastText(
								getConversation().getConversationID()));
			}
		}
	}

	private class DrawItemRunnable implements Runnable {
		private ArrayList<ChatMessage> toDraw;
		private int maxItems;
		private int counter;
		public DrawItemRunnable (ArrayList<ChatMessage> toDraw){
			this.toDraw = toDraw;
			this.maxItems = toDraw.size();
			this.counter = 0;
		}
		@Override
		public void run() {
			if (maxItems == 0){
				messagesContainer.setVisibility(View.VISIBLE);
				try {
					messageEt.setText(TipxApp.generalSettings.getLastText(
							getConversation().getConversationID()));
					Log.i("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
							" message: " +  TipxApp.generalSettings.getLastText(
									getConversation().getConversationID()));
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
							" message: " +  TipxApp.generalSettings.getLastText(
									getConversation().getConversationID()));
				}
				return;
			}
			ChatMessage temp = toDraw.get(counter);
			updateScrollMessages(temp,true);
			if (temp.getType() == ChatMessage.TYPE_VIDEO||temp.getType() == ChatMessage.TYPE_AUDIO){
				if (temp.isUploudFileCanceled()){
					UploadMediaTask uploadMediaTask = new UploadMediaTask(ChatMessage.TYPE_VIDEO, temp.getContent(),
							getConversation().getConversationType(),
							getConversation().getConversationID(),
							ChatFragment.this);
					uploadMediaTask.uploudAsMultypart();
				}
			}
			
			counter++;
			if (counter < maxItems) {
				new Handler().postDelayed(this,5);
			}else{
				messagesContainer.setVisibility(View.VISIBLE);
				TranslateAnimation ta = new TranslateAnimation(0, 0,-TipxApp.generalSettings.getScreenHight(),0);
				ta.setDuration(200);
				ta.setInterpolator(new OvershootInterpolator());
				messagesContainer.startAnimation(ta);
				
				try {
					
					messageEt.setText(TipxApp.generalSettings.getLastText(
							getConversation().getConversationID()));
					Log.i("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
							" message: " +  TipxApp.generalSettings.getLastText(
									getConversation().getConversationID()));
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
							" message: " +  TipxApp.generalSettings.getLastText(
									getConversation().getConversationID()));
				}
			}
			
		}
		
	}
	
	
	/**
	 * 
	 */
	private void setBackground() {
		if (conversation.isBackgroundIsImage()&&conversation.getBackroundPath() != null){
			BitmapUtil.loadImageIntoByFile(new File(conversation.getBackroundPath()), 
					chatBg, R.drawable.empty, R.drawable.empty, TipxApp.generalSettings.getScreenWidth(),
					TipxApp.generalSettings.getScreenHight(), null);
			chatBg.setAlpha(40f);
		}else{
			chatBg.setImageResource(R.drawable.empty);
			chatBg.setBackgroundColor
	    	(conversation.getColor());
			
		}
	}

	@Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
        	
        	ChatContainerFragment.currentChatFragment = this;
        	setStatusHeader();
        	if (ChatContainerFragment.currentChatFragment != null){
        		ChatContainerFragment.currentChatFragment.refreshChatIfNeeded();
        		TipxApp.conversationManager.updateConversationLastVissibleCountById
    			(ChatContainerFragment.currentChatFragment.getConversation().getConversationID());
        	
        		
			}
        	
        }
    }
	
	public void setStatusHeader () {
		
		Log.e("StatusChangelog", "setStatusHeader");
		
		String status = "status: ";
    	if (this.getConversation().getConversationType() == ChatMessage.GROUP_MESSAGE){
    		status = getConversation().getGroupObject().getMemnbersAsString();
    	}else{
    		AppContact currentContact = TipxApp.contactsManager.
                    getContactByContactId(conversation.getConversationID());
    		if (currentContact != null){
    			int stat = currentContact.getContactStatus();
        		if (stat == AppContact.STATUS_OFFLINE){
        			status = currentContact.getLastSeen();
        		}else{
            		status = AppContact.STATUS_DISPLAY[currentContact.getContactStatus()];
        		}
    		}
    	}
    	MainActivity.setScreenTitle(ChatContainerFragment.
    			currentChatFragment.getConversation().getConversationName(),status,null);
	}
	
	
	 
	
	
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.send_btn:
			String content = messageEt.getText().toString();
			if (!content.isEmpty()) {
				TipxApp.generalSettings.setLastText("",getConversation().getConversationID());
				sendNewMessage(ChatMessage.TYPE_TEXT, content,"");
			}
			break;
		case R.id.media_btn:
			showMediaChooserDialog();
			break;
		case R.id.load_messages:
			break;
		case R.id.message_et:
			scrollScreenToBottom();
			break;
		case R.id.record_btn:
			v.setSelected(true);
			SoundUtil.playSound(R.raw.record_sound);
			if (!isRecording) {
				startRecording();
				startDownCountTimer();
				cancelRecording.setVisibility(View.VISIBLE);
				animateRecordingContainer(recordContainer, 1,
						FULL_WIDTH);
			}
			break;
		case R.id.cancel_recording:
			cancelRecordingIfNeeded();
			break;
		case R.id.send_record_btn:
			if (isRecording) {
				cancelRecordingIfNeeded();
				submitRecordingAoudio(ChatMessage.TYPE_AUDIO, recordingFileName);
			}
			break;

		}

	}
	
	
	
	public void refreshChatIfNeeded () {
		try {
			if (TipxApp.conversationManager.getConversation(conversation.getConversationID()).getMessages().size() !=  lastMessagesCount){
				TipxApp.appPreference.getUserProfil().setNotificationCount(0);
				NotificationManager mNotificationManager = (NotificationManager) 
						MainActivity.mainInstance.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
				messagesContainer.removeAllViews();
				loadMessagesIntoScrollList();
			}
		} catch (Exception e) {}
	}
	
	public void sendNewMessage (int type,String content,String link){
		messagesContainer.setVisibility(View.VISIBLE);
		switch (type) {
		case ChatMessage.TYPE_TEXT:
			if(conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				Log.e("sendtextmessagelog", "id - " + conversation.getConversationID());
				TipxApp.chatManager.sendTextMessage("0",conversation.getConversationID(), content);
			}else{
				TipxApp.chatManager.sendTextMessage( conversation.getConversationID(),"0", content);
			}
			break;
		case ChatMessage.TYPE_LOCATION:
			if(conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				TipxApp.chatManager.sendLocationMessage("0", conversation.getConversationID(), content);
			}else{
				TipxApp.chatManager.sendLocationMessage( conversation.getConversationID(),"0", content);
			}
			break;
		case ChatMessage.TYPE_IMAGE:
			if(conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_IMAGE,"0", conversation.getConversationID(), link,link);
			}else{
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_IMAGE,conversation.getConversationID(),"0", link,link);
			}
			break;
		case ChatMessage.TYPE_VIDEO:
			if(conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_VIDEO,"0", conversation.getConversationID(), link,link);
			}else{
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_VIDEO,conversation.getConversationID(),"0", link,link);
			}
			break;
		case ChatMessage.TYPE_AUDIO:
			if(conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_AUDIO,"0", conversation.getConversationID(), link,link);
			}else{
				TipxApp.chatManager.sendMediaMessage(ChatMessage.MEDIA_CODE_AUDIO,conversation.getConversationID(),"0", link,link);
			}
			break;
		}
		
	}
	
	public void updateScrollMessages (ChatMessage msg,boolean clearField){
		Log.e("addmessagelog", "add message" + " type - " + msg.getType());
		
		addDateHeaderIfNeeded(msg);
		if (msg.getType() == ChatMessage.TYPE_ME_JOIN||msg.getType() == ChatMessage.TYPE_ME_LEAVE||
				msg.getType() == ChatMessage.TYPE_MEMBER_JOIN||msg.getType() == ChatMessage.TYPE_MEMBER_LEAVE){
			addInfoMessage(msg);
		}else{
			if (!TipxApp.contactsManager.isContactHiden(msg.getSenderPhone())){
				ChatMessageView messageView = new ChatMessageView(this, msg,index,this,this);
				viewArray.add(messageView);
				messagesContainer.addView(messageView.getView());
				isSendTypingMessage = false;
				
			}
		}
		
		Log.e("messageIdlog", "size = " + viewArray.size());
		scrollScreenToBottom();
		if (clearField){
			messageEt.setText("");
			Log.e("lastTextLog", "clear");
		}
		    
		index++;
		lastMessagesCount++;
	}
	
	
	public void addInfoMessage(ChatMessage msg){
		try {
			InfoMessage infoMessage = new InfoMessage(getActivity(),msg.getContent(), msg.getCreated());
			messagesContainer.addView(infoMessage.getView());
		} catch (Exception e) {
			Log.i("chatexlog", "ex = " + e.getMessage());
		}
		
	}
	
	
	public void addDateHeaderIfNeeded (ChatMessage msg){
		try {
			newDate = DateUtil.getDaysOfServerDate(msg.getCreated());
			Log.e("datelog" , "old = " + oldDate + " new = " + newDate);
			if (newDate != oldDate){
				
				InfoMessage infoMessage = new InfoMessage(getActivity(), msg.getCreated(), null);
				messagesContainer.addView(infoMessage.getView());
			}
			oldDate = newDate;
		} catch (Exception e) {
			Log.i("chatexlog", "ex = " + e.getMessage());
		}
		
	}
	
	private void scrollScreenToBottom (){
		messagesScroll.postDelayed(new Runnable() {
			public void run() {
				messagesScroll.fullScroll(View.FOCUS_DOWN);
			}
		}, 500);
		
		
		
	}
	
	
	private void showMediaChooserDialog () {
		String [] optionsText = getActivity().getResources().getStringArray(R.array.chat_chooser_options);
		DialogManager.showDialogChooser(this,optionsText, new DialogCallback(){
			
			@Override
			protected void onDialogOptionPressed(int which,
					DialogFragment dialog) {
				super.onDialogOptionPressed(which, dialog);
				switch (which) {
				case 0:
					ContentProviderUtil.openGalleryFromChildFragment(ChatFragment.this, SELECT_PHOTO_FROM_GALLERY);
					break;
				case 1:
					ContentProviderUtil.openVideoGalleryFromChildFragment(ChatFragment.this, SELECT_VIDEO_RROM_GALLERY);
					break;
				case 2:
					ContentProviderUtil.openAudioGalleryFromChildFragment(ChatFragment.this, SELECT_AUDIO_FROM_GALLERY);
					break;
				case 3:
					sendNewMessage(ChatMessage.TYPE_LOCATION, "","");
					break;
				case 4:
					ContentProviderUtil.openRecordVideoFromChildFragment(ChatFragment.this, RECORD_VIDEO,
							BitmapUtil.cretatStringPathForStoringMedia(DateUtil.getCurrentDateInMilli()+"_record_media.mp4"));
					break;
				case 5:
					ContentProviderUtil.openRecordAudioFromChildFragment(ChatFragment.this, RECORD_AUDIO);
					break;
				case 6:
					currentImageUri = BitmapUtil.createUriPathForStoringImage("temp_image");
					ContentProviderUtil.openCameraFromChildFragment(ChatFragment.this, CAPTURE_IMAGE,
							currentImageUri);
					break;
				}
			}
			
		});
	}

	public static Uri currentImageUri;
	
	
	@Override
	public boolean onItemClick(View view, int position,final ChatMessage selected) {
		
		
		Log.e("onitemclicklog", "slelctedType = " + selected.getType() + " link - " + selected.getLink() + " content = " + selected.getFilePath()); 
		if (selected.getType() == ChatMessage.TYPE_LOCATION){
			ContentProviderUtil.openLocationChooser(getActivity(), selected.getLat()+"", selected.getLng()+"");
		}else if (selected.getType() == ChatMessage.TYPE_IMAGE){
			////////////////////////////////////////////////////
			//ContentProviderUtil.playMediaByFilePath(ChatFragment.this,"image/*", selected.getFilePath());
			//ContentProviderUtil.playMediaByUrl(ChatFragment.this,"image/*", selected.getLink());
			Intent intent = new Intent(getActivity(),ImagePrivewActivity.class);
			intent.putExtra(ImagePrivewActivity.EXTRA_PATH, selected.getFilePath());
			Log.e("fileSelectedLog", "path: " + selected.getFilePath());
			if (selected.getFilePath().contains("http")){
				intent.putExtra(ImagePrivewActivity.EXTRA_PATH_TYPE, ImagePrivewActivity.PATH_TYPE_URL);
			}else{
				intent.putExtra(ImagePrivewActivity.EXTRA_PATH_TYPE, ImagePrivewActivity.PATH_TYPE_LOCAL);
			}
			startActivity(intent);
		}else if (selected.getType() == ChatMessage.TYPE_VIDEO){
			playMedea("video/*", selected);
		}else if (selected.getType() == ChatMessage.TYPE_AUDIO){
			playMedea("audio/*", selected);
		}
		return false;
	}

	
	private void playMedea (String mediaType,final ChatMessage selected){
		if (selected.getMessagePhat() ==ChatMessage.PHAT_TO){
			ContentProviderUtil.playMediaByUrl(ChatFragment.this,mediaType, selected.getFilePath());
		}else{
			if (selected.getFilePath() != null && 
					selected.getFilePath().contains(DownloadMediaTask.TIPX_DIR_NAME)){
				ContentProviderUtil.playMediaByUrl(ChatFragment.this,mediaType, selected.getFilePath());
			}else{
				ContentProviderUtil.playMediaByUrl(ChatFragment.this,mediaType, selected.getLink());
				if (selected.getDownloadStatus() != ChatMessage.DOWNLOAD_STATUS_IN_PROGRESS){
					selected.setDownloadStatus(ChatMessage.DOWNLOAD_STATUS_IN_PROGRESS);
					TipxApp.conversationManager.updateMessageById(conversation.getConversationID(),
							selected.getMessageId()+"", selected);
					DownloadMediaTask downloadMediaTask = new DownloadMediaTask(selected.createFileName(), selected.getLink(),
							new DownloadListener() {
								@Override
								public void onFileDownloaded(boolean success, File file, String phat) {
									if (success){
										selected.setFilePath(phat);
										selected.setDownloadStatus(ChatMessage.DOWNLOAD_STATUS_COMPLETED);
										TipxApp.conversationManager.updateMessageById(conversation.getConversationID(),
												selected.getMessageId()+"", selected);
										if (selected.getChatType() != ChatMessage.GROUP_MESSAGE){
											TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_CONFIRM_DOWNLOADED,
													selected.getMessageId()+"", "", !TipxApp.smackManager.isConnected());
										}
									}else{
										selected.setDownloadStatus(ChatMessage.DOWNLOAD_STATUS_WAIT);
										TipxApp.conversationManager.updateMessageById(conversation.getConversationID(),
												selected.getMessageId()+"", selected);
									}
								}
					});
					downloadMediaTask.download();
				}
				
			}
		}
	}
	
	@Override
	public void onChatMessageReceived(final ChatMessage msg) {
		Log.e("chatlistenerlog", "current chat - " + conversation.getConversationName() + " id = " + conversation.getConversationID());
		Log.e("chatlistenerlog", "messageType = " + msg.getType());
		Log.e("chatlistenerlog", "senderid = " + msg.getSenderId());
		Log.e("chatlistenerlog", "recipientId = " + msg.getRecipientId());
		Log.e("chatlistenerlog", "chatType = " + msg.getChatType());
		Log.e("chatlistenerlog", "path = " + msg.getMessagePhat());
		
		MainActivity.mainInstance.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				switch (msg.getType()) {
				case ChatMessage.TYPE_TEXT:
				case ChatMessage.TYPE_LOCATION:
				case ChatMessage.TYPE_IMAGE:
				case ChatMessage.TYPE_VIDEO:
				case ChatMessage.TYPE_AUDIO:
				case ChatMessage.TYPE_MEMBER_LEAVE:
				case ChatMessage.TYPE_MEMBER_JOIN:
				case ChatMessage.TYPE_ME_JOIN:
					handelNewMessage(msg);
					break;
				case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_SERVER:
				case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_USER:
				case ChatMessage.TYPE_MESSAGE_READED:
				case ChatMessage.TYPE_CONFIRM_DELETED:
					handelUpdateMessage(msg);
					break;
				case ChatMessage.TYPE_MESSAGE_DELETED:
					handelRemoveMessage(msg);
					break;
				case ChatMessage.TYPE_GROUP_DELETED:
				case ChatMessage.TYPE_ME_LEAVE:
				case ChatMessage.TYPE_DELET_CONVERSATION:
				case ChatMessage.TYPE_DELET_CONTACT:
					handelRemoveGroup();
					break;
				case ChatMessage.TYPE_PARTNER_START_TYPING:
				case ChatMessage.TYPE_PARTNER_STOP_TYPING:
					handelTypingStatusUpdated(msg);
					break;
				}
				
			}

			
		});
		
	}
	
	
	private void handelNewMessage(ChatMessage msg){
		messagesContainer.setVisibility(View.VISIBLE);
		String id = "";
		if (msg.getChatType() == ChatMessage.GROUP_MESSAGE){
			id = msg.getGroupId()+"";
		}else{
			if (msg.getMessagePhat() == ChatMessage.PHAT_TO){
				id = msg.getRecipientId()+"";
			}else{
				id = TipxApp.contactsManager.getContactIdByContactPhone(msg.getSenderPhone());
			}
		}
		
		if (!conversation.getConversationID().equalsIgnoreCase(id)){
			TipxApp.chatManager.notifyUserNewChatMessage(msg);
			return;
		}else{
			if (MainActivity.appSentToBackground){
				TipxApp.chatManager.notifyUserNewChatMessage(msg);
			}
			if (msg.getMessagePhat() == ChatMessage.PHAT_TO){
				updateScrollMessages(msg, true);
			}else{
				
				try {
					partnerStatusHandler.removeCallbacks(updatePartnerStatus);

				} catch (Exception e) {
				}
				setStatusHeader();
				
				updateScrollMessages(msg, false);
			}
			TipxApp.conversationManager.updateConversationLastVissibleCountById
			(getConversation().getConversationID());
		}
		
	}
	
	private void handelTypingStatusUpdated(ChatMessage message) {
		if (message.getMessagePhat() == ChatMessage.PHAT_TO){
			return;
		}
		String id = message.getConversationId();
		
		Log.e("typinglog", "id = " + id + " converId = " + conversation.getConversationID());
		TipxApp.contactsManager.updateContactPresentStatusById(message.getSenderId());
		if (!conversation.getConversationID().equalsIgnoreCase(id)){
			return;
		}
		if (message.getType() == ChatMessage.TYPE_PARTNER_START_TYPING){
			Log.e("typinglog", "ChatType = " + message.getChatType());
			if (conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
				MainActivity.setStatusTxt(TipxApp.contactsManager.getContactByContactId(message.getSenderId()).getContactName() + " "
				+ AppContact.STATUS_DISPLAY[AppContact.STATUS_TYPEING]);
				
			}else{
				MainActivity.setStatusTxt(AppContact.STATUS_DISPLAY[AppContact.STATUS_TYPEING]);
				try {
					partnerStatusHandler.removeCallbacks(updatePartnerStatus);
				} catch (Exception e) {
					e.printStackTrace();
				}
				partnerStatusHandler.postDelayed(updatePartnerStatus, 10000);
			}
		}else if (message.getType() == ChatMessage.TYPE_PARTNER_STOP_TYPING){
			Log.e("typinglog", "userStoptyping");
			setStatusHeader();
		}
	}
	
	/**
	 * 
	 */
	protected void handelRemoveGroup() {
		MainActivity.mainInstance.refreshApplication(); 
	}

	private void handelRemoveMessage(ChatMessage msg) {
		for (int i = 0; i < viewArray.size(); i++) {
			if (viewArray.get(i).getMessageId().equalsIgnoreCase(msg.getMessageId()+"")){
				messagesContainer.removeView(viewArray.get(i).getView());
				viewArray.remove(i);
				break;
			}
		}
		
	}

	private void removeMessageViewByContent(String content,boolean removeFromDB) {
		try {
			for (int i = 0; i < viewArray.size(); i++) {
				if (viewArray.get(i).getContent().equalsIgnoreCase(content)){
					if (removeFromDB){
						TipxApp.conversationManager.removeMessageByContent(content);
					}
					
					messagesContainer.removeView(viewArray.get(i).getView());
					viewArray.remove(i);
					
					break;
				}
			}
		} catch (Exception e) {}
	}
	
	private void handelUpdateMessage(ChatMessage msg) {
		int status = msg.getStatus();
		String id = msg.getCurrentMessageID();
		
		switch (msg.getType()) {
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_SERVER:
			status = ChatMessage.STATUS_RECIVED_IN_SERVER;
			id = msg.getTempId();
			for (int i = 0; i < viewArray.size(); i++) {
				String s = viewArray.get(i).getMessageId();
				Log.e("messageIdlog", "id = " + s + " c = " + id);
				if (viewArray.get(i).getMessageId().equalsIgnoreCase(id)){
					viewArray.get(i).setMessageStatus(status);
					viewArray.get(i).setMessageID(msg.getNewMessageId());
					break;
				}
			}
			break;
		case ChatMessage.TYPE_MESSAGE_ACCEPT_IN_USER:
			status = ChatMessage.STATUS_RECIVED_IN_RECIPIENT;
			
			id = msg.getCurrentMessageID();
			for (int i = 0; i < viewArray.size(); i++) {
				String s = viewArray.get(i).getMessageId();
				Log.e("messageIdlog", "id = " + s + " c = " + id);
				if (viewArray.get(i).getMessageId().equalsIgnoreCase(id)){
					if (viewArray.get(i).getStatus() == ChatMessage.STATUS_READED_BY_RECIPIENT){
						break;
					}
					viewArray.get(i).setMessageStatus(status);
					
					break;
				}
			}
			break;
		case ChatMessage.TYPE_MESSAGE_READED:
			status = ChatMessage.STATUS_READED_BY_RECIPIENT;
			id = msg.getCurrentMessageID();
			for (int i = 0; i < viewArray.size(); i++) {
				String s = viewArray.get(i).getMessageId();
				Log.e("messageIdlog", "id = " + s + " c = " + id);
				if (viewArray.get(i).getMessageId().equalsIgnoreCase(id)){
					viewArray.get(i).setMessageStatus(status);
					if (msg.getMessagePhat() == ChatMessage.PHAT_TO && msg.isAutoDelete()){
						msg.startRemoveTimer();
					}
					break;
				}
			}
			break;
		case ChatMessage.TYPE_CONFIRM_DELETED:
			status = ChatMessage.STATUS_CONFIRM_DELETATION;
			id = msg.getCurrentMessageID();
			Log.d("removeMessageViewLog", "id = " + id);
			for (int i = 0; i < viewArray.size(); i++) {
				final int pos = i;
				String s = viewArray.get(i).getMessageId();
				
				if (viewArray.get(i).getMessageId().equalsIgnoreCase(id)){
					viewArray.get(i).setMessageStatus(status);
					if (TipxApp.conversationManager.getMessageByConversationIdAndMessageId(conversation.getConversationID(),
							msg.getCurrentMessageID()) == null){
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								try {
									Log.e("removeMessageViewLog", "id = " + viewArray.get(pos).getMessageId());
									messagesContainer.removeView(viewArray.get(pos).getView());
									viewArray.remove(pos);
								} catch (Exception e) {}
							}
						}, 0);
						
					}
					break;
				}
			}
			
			break;
		}
		
		
	}

	public void setConversation (Conversation conversation){
		if (this.conversation == null)
		    this.conversation = conversation;
	}
	
	public Conversation getConversation (){
		
		return conversation;
	}

	
	@Override
	public void onDeletePressed(int type, View messageView, ChatMessage data,ChatMessageView chatMessageView) {
		// TODO Auto-generated method stub
		Log.e("popuplog", "type = " +  type ); 
		if (data.getMessagePhat() == ChatMessage.PHAT_FROM){
			deleteMessageFromCurrentDB(messageView, data.getMessageId()+"");
		}else{
			switch (type) {
			case ChatMessageView.DELETE_TYPE_ME:
				deleteMessageFromCurrentDB(messageView, data.getMessageId()+"");
				break;
			case ChatMessageView.DELETE_TYPE_OTHER:
				deleteMessageFromOtheDB(chatMessageView,data);
				break;
			case ChatMessageView.DELETE_TYPE_BOTH:
				deleteBoth(chatMessageView, data);
				break;
			}
		}
		
	}

	
	@Override
	public void onPopupShowing(PopupWindow popupWindow) {
		try {
			currentDisplayPopup.dismiss();
		} catch (Exception e) {
			// TODO: handle exception
		}
		currentDisplayPopup = popupWindow;
	}
	
	public void deleteMessageFromCurrentDB (View view,String msgId){
		messagesContainer.removeView(view);
		TipxApp.conversationManager.removeMessageByID(conversation.getConversationID(), msgId);
	}
	
	public void deleteMessageFromOtheDB (ChatMessageView view,ChatMessage msg){
		//TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_DELETED_MESSAGE, msgID);
		view.setMessageStatus(ChatMessage.STATUS_DELETED);
		msg.setStatus(ChatMessage.STATUS_DELETED);
		TipxApp.conversationManager.updateMessageStatusById(msg.getMessageId()+"", msg);
		TipxApp.networkManager.sendDeleteMessage(msg.getMessageId()+"");
		
	}
	
	public void deleteBoth (ChatMessageView view,ChatMessage msg){
		
		view.setMessageStatus(ChatMessage.STATUS_DELETED);
		
		msg.setStatus(ChatMessage.STATUS_DELETED);
		
		TipxApp.conversationManager.updateMessageStatusById(msg.getMessageId()+"", msg);
		
		TipxApp.chatManager.removeMessage(conversation.getConversationID(), msg.getMessageId()+"");
		removeMessageView(msg);
	}
	
	
	synchronized public void removeMessageView (final ChatMessage toRemove){
		Log.d("removeMessageViewLog", "remove message id = " + toRemove.getCurrentMessageID());
		if (viewArray == null){
			return;
		}
		String id = toRemove.getCurrentMessageID();
		for (int i = 0; i < viewArray.size(); i++) {
			final int pos = i;
			String s = viewArray.get(i).getMessageId();
			if (s.equalsIgnoreCase(id)){
				try {
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							//Log.e("removeMessageViewLog", "remove message id = " + viewArray.get(pos).getMessageId());
							messagesContainer.removeView(viewArray.get(pos).getView());
							viewArray.remove(toRemove);
						}
					});
				} catch (Exception e) {
					break;
				}
				break;
			}
		}
	}
	
	public static Handler chatHandeler = new Handler(){
		public static final int MSG_REMOVE_MESSAGE = 11;
		
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_REMOVE_MESSAGE:
				ChatMessage toRemove = (ChatMessage) msg.obj;
				if (toRemove != null){
					if (ChatContainerFragment.currentChatFragment != null)
					ChatContainerFragment.currentChatFragment.
					removeMessageView(toRemove);
				}
				break;
			}
		};
	};
	
	
	
	public void cancelRecordingIfNeeded () {
		try {
			if (isRecording){
				stopRecording();
				countDownTimer.cancel();
				recordingIcon.clearAnimation();
				animateRecordingContainer(recordContainer, FULL_WIDTH, 1);
				cancelRecording.setVisibility(View.GONE);
				recordBtn.setSelected(false);
			}
		} catch (Exception e) {}
		catch (Error e) {}
	}
	
	private void startRecording() {
		 recordingFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
	        recordingFileName += "/tipxaudiorecordtest.3gp";
       mRecorder = new MediaRecorder();
       mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
       mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
       mRecorder.setOutputFile(recordingFileName);
       mRecorder.setMaxDuration(MAX_RECORDING_LENGTH*1000);
       mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
       try {
           mRecorder.prepare();
       } catch (IOException e) {
           Log.e("recordinglog", "prepare() failed");
       }
       isRecording = true;
       try {
    	   mRecorder.start();
	} catch (Exception e) {
		// TODO: handle exception
	}
       
       
   }

   private void stopRecording() {
       mRecorder.stop();
       mRecorder.release();
       mRecorder = null;
       isRecording = false;
   }
   

   private void animateRecordingContainer (View view,int from,int to){
		HeightAnimation heightAnim = new HeightAnimation(view, from, to);
		heightAnim.setDuration(500);
		view.startAnimation(heightAnim);
		Animation myFadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.twin);
		recordingIcon.startAnimation(myFadeInAnimation);
	}
   
	private void startDownCountTimer() {
		if (countDownTimer != null) {
			countDownTimer.cancel();

		}
		countDownTimer = new CountDownTimer(MAX_RECORDING_LENGTH * 1000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(millisUntilFinished);
				final String toDisplay = new SimpleDateFormat("mm:ss")
						.format(calendar.getTime());
				try {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Log.e("recordingText", "text = " + toDisplay);
							recordTime.setText(toDisplay);
						}
					});
				} catch (Exception e) {}
			}

			@Override
			public void onFinish() {
				if (isRecording) {
					cancelRecordingIfNeeded();
					submitRecordingAoudio(ChatMessage.TYPE_AUDIO, recordingFileName);
				}
			}

		};
		countDownTimer.start();
	}

	
	public void submitRecordingAoudio (final int mediaType,final String fileName){
		
		UploadMediaTask uploadMediaTask = new UploadMediaTask(ChatMessage.TYPE_AUDIO, fileName,
				getConversation().getConversationType(),
				getConversation().getConversationID(),
				this);
		
		uploadMediaTask.uploudAsMultypart();
		ChatMessage temp = ChatContainerFragment.createTempMediaMessage(ChatMessage.TYPE_AUDIO,fileName);
		TipxApp.chatManager.handleReceivedMessage(temp);
		onChatMessageReceived(temp);
	}
	
	
	private Runnable input_finish_checker = new Runnable() {
	    public void run() {
	            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
	                 // user hasn't changed the EditText for longer than
	                 // the min delay (with half second buffer window)
	                 if (!already_queried&&isSendTypingMessage) { // don't do this stuff twice.
	                    //already_queried = true;
	                     isSendTypingMessage = false;
	     				boolean isGroup = false;
	     				if (conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
	     					isGroup = true;
	     				}
	     				TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_STOP_TYPING,
	     						conversation.getConversationID(), isGroup);
	                    
	                 }
	            }
	    }
	};
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
	}

	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		last_text_edit = System.currentTimeMillis();
		if (messageEt.getText().toString().length() > 0){
			if (!isSendTypingMessage){
				isSendTypingMessage = true;
				boolean isGroup = false;
				if (conversation.getConversationType() == ChatMessage.GROUP_MESSAGE){
					isGroup = true;
				}
				TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_START_TYPING,
						conversation.getConversationID(), isGroup);
			}
		}
		
		last_text_edit = System.currentTimeMillis();
		h.postDelayed(input_finish_checker, idle_min); 
	}

	
	@Override
	public void afterTextChanged(Editable s) {
		
		
		if (!messageEt.getText().toString().isEmpty()){
			TipxApp.generalSettings.setLastText
			(messageEt.getText().toString(),getConversation().getConversationID());
			Log.e("lastTextLog", "con Id : " + getConversation().getConversationID() + " " +
					" message: " +  TipxApp.generalSettings.getLastText(
							getConversation().getConversationID()));
		}
		
	}

	
	@Override
	public void onUpload(boolean success, String dataURL, String fileName,
			int mediaType) {
		Log.e("uploadMediaTaskLog", "onUpload. success ? " + success + 
				" isImage ? " + (mediaType == ChatMessage.TYPE_IMAGE));
		if (mediaType == ChatMessage.TYPE_IMAGE){
			return;
		}
		
		if (!success){
			try {
				removeMessageViewByContent(fileName,true);
				DialogManager.showErrorDialog(this, getString(R.string.upload_failded_txt), null);
			} catch (Exception e) {}
		}else{
			removeMessageViewByContent(fileName,false);
		}
		
	}

	
	@Override
	public void onReadMessageStatusClic() {
		if (conversation != null && conversation.getMessages() != null&&viewArray != null){
			for (int i = 0; i < viewArray.size(); i++) {
				ChatMessageView temp = viewArray.get(i);
				if (temp.getData().getMessagePhat() == ChatMessage.PHAT_FROM){
					if (temp.getData().getReadConfirmationStatus() != ChatMessage.READ_STATUS_SENT){
						if (temp.getReadMessageStatusImg() != null){
							temp.getReadMessageStatusImg().setImageResource(
									ChatMessageView.readConfirmationImage[ChatMessage.READ_STATUS_SENT]);
						}
						temp.getData().setReadConfirmationStatus(ChatMessage.READ_STATUS_SENT);
						TipxApp.conversationManager.updateMessageReadStatusById(temp.getData().getMessageId()+"", 
								temp.getData());
						TipxApp.chatManager.sendStatusMessage(
								TipxChatManager.ACTION_READED,
								temp.getData().getCurrentMessageID(),temp.getData().getSenderId(),true);
					}
				}
				
			}
		}
		
	}


	

}
