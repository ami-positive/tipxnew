/**
 * 
 */
package com.positiveapps.tipx.fragments;



import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.adapters.AppContactsListAdapter;
import com.positiveapps.tipx.adapters.AppContactsListAdapter.OnContactSelectedListener;
import com.positiveapps.tipx.contacts.AppContact;
import com.positiveapps.tipx.database.StorageListener;
import com.positiveapps.tipx.dialogs.AppDialogButton;
import com.positiveapps.tipx.dialogs.DialogCallback;
import com.positiveapps.tipx.dialogs.DialogManager;
import com.positiveapps.tipx.dialogs.LockPasswordDialog;
import com.positiveapps.tipx.fragments.login.ProfileFragment;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.objects.GroupObject;
import com.positiveapps.tipx.objects.UserProfile;
import com.positiveapps.tipx.ui.ActionBarButton;
import com.positiveapps.tipx.ui.ActionBarButton.OnActionBarButtonClickListener;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.ContentProviderUtil;
import com.positiveapps.tipx.util.DateUtil;
import com.positiveapps.tipx.util.DialogUtil;
import com.positiveapps.tipx.util.FragmentsUtil;
import com.positiveapps.tipx.util.ToastUtil;




/**
 * @author natiapplications
 *
 */
public class CreateGroupFragment extends BaseFragment implements TextWatcher,OnItemClickListener,OnItemLongClickListener,OnClickListener,OnContactSelectedListener{
	
	private final int REQUEST_GALLERY = 1000;

	
	private ImageView userPicture;
	private EditText userNameTv;
	private TextView editTv;
	private View listHeader;
	private TextView headerText;
	private EditText searchEt;
	private LinearLayout headerContainer;
	private ListView contactsListView;
	private AppContactsListAdapter adapter;
	
	private boolean isOnSearchResult;
	private ArrayList<AppContact> filterArray = new ArrayList<AppContact>();
	private ArrayList<AppContact> searchArray;
	private ArrayList<AppContact> currentArray;
	private ArrayList<AppContact> selectedArray = new ArrayList<AppContact>();
	private String imageBase64;
	private GroupObject groupObject = new GroupObject();
	
	
	public CreateGroupFragment() {

	}

	public static CreateGroupFragment newInstance() {
		CreateGroupFragment instance = new CreateGroupFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_create_group, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(TipxApp.appContext.getString(R.string.create_group_screen_title),null,null);
		
		userPicture = (ImageView)view.findViewById(R.id.image_profile);
		userNameTv = (EditText)view.findViewById(R.id.name_et);
		editTv = (TextView)view.findViewById(R.id.edit_txt);
		
		contactsListView = (ListView)view.findViewById(R.id.contacts_list);
		listHeader = getActivity().getLayoutInflater().inflate(R.layout.layout_app_contacts_list_header, null);
		headerText = (TextView)listHeader.findViewById(R.id.header_text);
		searchEt = (EditText)view.findViewById(R.id.search_et);
		headerContainer = (LinearLayout)view.findViewById(R.id.header_container);
		getActivity().getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		searchEt.addTextChangedListener(this);
		
		userPicture.setOnClickListener(this);
		editTv.setOnClickListener(this);
		contactsListView.setOnItemClickListener(this);
		contactsListView.setOnItemLongClickListener(this);
		contactsListView.addHeaderView(listHeader);
		setUpActionBarButtons();
		setUpContacts();
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_GALLERY){
			if(resultCode == Activity.RESULT_OK){
				String path = BitmapUtil.getPathOfImagUri(data.getData());
				groupObject.setImagePath(path);
				imageBase64 = BitmapUtil.getImageAsStringEncodedBase64(
						BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,data.getData(),BitmapFactory.decodeFile(path));
				BitmapUtil.loadImgaeIntoBySelectedUri(BitmapUtil.IMAGE_TYPE_FROM_GALLERAY,
						data.getData(), userPicture, 160, 2, R.drawable.ic_action_person);
			}
		}
		
	}
	
	
	
	private void setUpActionBarButtons (){
		MainActivity.removeAllActionBarButtons();
		ActionBarButton saveBtn = new ActionBarButton(getActivity(), R.drawable.ic_action_accept, "",
				new OnActionBarButtonClickListener() {
					@Override
					public void onActionBarButtonClick(View v) {
						if (selectedArray.size() == 0){
							ToastUtil.toster("You Must Choose Members", false);
							return;
						}
						
						createNewGroup();
					}
				});
		MainActivity.addButtonToActionBar(saveBtn);
		
	}
	
	

	
	protected void setUpContacts() {
		TipxApp.contactsManager.readContactsList(new StorageListener() {
			@Override
			public void onDataStorageRcived(String type, Object object) {
				filterArray = new ArrayList<AppContact>();
				for (int i = 0; i < ((ArrayList<AppContact>)object).size(); i++) {
					AppContact temp = ((ArrayList<AppContact>)object).get(i);
					if (temp.isHasApplication()){
						filterArray.add(temp);
					}
				}
				Log.e("creategrouplog", "size = " + filterArray.size());
				updateContactListView(filterArray);
			}
			@Override
			public void DataNotFound(String type) {}
		});
	}

	public void updateContactListView (ArrayList<AppContact> contacts){
		currentArray = contacts;
		
		if (isOnSearchResult){
			if(contacts.size() == 0){
				headerContainer.removeAllViews();
				headerContainer.addView(listHeader);
				headerText.setText("No result found");
			}else{
				try {
					headerContainer.removeAllViews();
				} catch (Exception e) {}
			}
		}else{
			if (currentArray.size() > 0){
				//setAppContactStatus(TipxApp.contactsManager.getAppContactsList());
				try {
					contactsListView.removeHeaderView(listHeader);
				} catch (Exception e) {}
			}
		}
		
		adapter = new AppContactsListAdapter(getActivity(), currentArray);
		adapter.setListAsChooser("",this);
		contactsListView.setAdapter(adapter);
		
	}

	private String getSelectedContactAsString (){
		
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < selectedArray.size(); i++) {
			builder.append("\"");
			builder.append(selectedArray.get(i).getContactId());
			builder.append("\"");
			if (i < selectedArray.size()-1){
				builder.append(",");
			}
		}
		builder.append("]");
		Log.e("selectedContactAsString", builder.toString());
		return builder.toString();
	}
	

	private void createNewGroup (){
		groupObject.setName(userNameTv.getText().toString());
		groupObject.setAdminID(TipxApp.userProfil.getUserId());
		groupObject.setCreatedAt(DateUtil.getCurrentTimeAsServerString());
		
		showProgressDialog();
		TipxApp.networkManager.createGroup(groupObject.getName(),imageBase64,
				getSelectedContactAsString(),new NetworkCallback(){

			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.i("createuserlog", "response = " + response.getJsonContent());
				if (!isHasError){
					try {
						JSONObject data = new JSONObject(response.getData());
						String id = String.valueOf(data.optInt("GroupID",0));
						if (id.equals("0")){
							id = data.optString("GroupID");
						}
						groupObject.setGroupId(id);
						groupObject.setGroupCode(data.optString("Code"));
						Log.i("imagephatlog", "path - " + groupObject.getImagePath());
						TipxApp.conversationManager.openGroupConversation(groupObject, new ArrayList<ChatMessage>());
						getFragmentManager().popBackStack();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
				
			}
		
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				dismisProgressDialog();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}

	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {}

	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}

	
	@Override
	public void afterTextChanged(Editable s) {
		if (searchEt.getText().toString().isEmpty()){
			updateContactListView(filterArray);
			isOnSearchResult = false;
		}else{
			isOnSearchResult = true;
			search(searchEt.getText().toString());
		}
		
	}

	public void search (String keyWord){
		searchArray = new ArrayList<AppContact>();
		for (int i = 0; i < filterArray.size(); i++) {
			AppContact temp = filterArray.get(i);
			if(StringUtils.containsIgnoreCase(temp.getContactName(), keyWord)||
					StringUtils.containsIgnoreCase(temp.getContactPhone(), keyWord)){
				searchArray.add(temp);
			}
		}
		updateContactListView(searchArray);
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AppContact selected = currentArray.get(position);
		CheckBox checkBox = (CheckBox)view.findViewById(R.id.checkContact);
		checkBox.toggle();
		onContactSelected(checkBox.isChecked(), selected);
		
	}

	
	
	
	
	@Override
	public boolean onBackPressed() {
		if(isOnSearchResult){
			isOnSearchResult = false;
			searchEt.setText("");
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public void onContactSelected(boolean selected, AppContact contact) {
		if (selected){
			selectedArray.add(contact);
		}else{
			try {
				selectedArray.remove(contact);
			} catch (Exception e) {}
		}
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.image_profile:
		case R.id.edit_txt:
			ContentProviderUtil.openGallery(this, REQUEST_GALLERY);
			break;
		}

	}

	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
