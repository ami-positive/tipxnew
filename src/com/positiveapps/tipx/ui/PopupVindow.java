/**
 * 
 */
package com.positiveapps.tipx.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import com.positiveapps.tipx.R;

/**
 * @author natiapplications
 *
 */
public class PopupVindow extends PopupWindow {  
	
	
	public PopupVindow(Context context, View content) {
	    super(context);
	    setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
	    setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
	    setTouchable(true);
	    setFocusable(true);
	    setOutsideTouchable(true);  
	    setTouchInterceptor(new View.OnTouchListener() {
	          @Override
	          public boolean onTouch(View v, MotionEvent event) {
	            if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
	            	PopupVindow.this.dismiss();                
	              return true;
	            }               
	            return false;
	          }
	        });
	}

	public void showUnderView(View view, View content) {
	    FrameLayout container = new FrameLayout(view.getContext());
	    container.setBackgroundColor(Color.TRANSPARENT);
	    container.addView(content);
	    setContentView(container);
	    int[] location = new int[2];
	    view.getLocationOnScreen(location);
	    container.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
	    int xoffset = view.getWidth() / 2 - container.getMeasuredWidth() / 2;
	    showAsDropDown(view, xoffset, -(view.getHeight()*2));
	}}