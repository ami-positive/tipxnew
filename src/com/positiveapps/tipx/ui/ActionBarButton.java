/**
 * 
 */
package com.positiveapps.tipx.ui;

import com.meg7.widget.CircleImageView;
import com.positiveapps.tipx.R;

import android.R.color;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class ActionBarButton {
	
	private View view;
	private CircleImageView button;
	private TextView textTv;
	
	public ActionBarButton (Activity activity,int drawable,String text,final OnActionBarButtonClickListener listener){
		view = activity.getLayoutInflater().inflate(R.layout.action_bar_button, null);
		button = (CircleImageView)view.findViewById(R.id.btn);
		textTv = (TextView)view.findViewById(R.id.text);
		if (drawable == -1){
			button.setBackgroundColor(color.transparent);
		}else{
			button.setBackgroundResource(drawable);
		}
		
		textTv.setText(text);
		
		if (listener != null){
			button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					listener.onActionBarButtonClick(v);
				}
			});
			/*textTv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					listener.onActionBarButtonClick(v);
				}
			});*/
		}
	}
	
	public View getView (){
		return view;
	}
	
	public ImageView getImageView (){
		return button;
	}
	
	public TextView getTextView (){
		return textTv;
	}
	
	public interface OnActionBarButtonClickListener{
		public void onActionBarButtonClick(View v);
	}

}
