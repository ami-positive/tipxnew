/**
 * 
 */
package com.positiveapps.tipx.ui;




import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.meg7.widget.CircleImageView;
import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.chat.TipxChatManager;
import com.positiveapps.tipx.fragments.ChatContainerFragment;
import com.positiveapps.tipx.fragments.ChatFragment;
import com.positiveapps.tipx.objects.ChatMessage;
import com.positiveapps.tipx.util.AppUtil;
import com.positiveapps.tipx.util.BitmapUtil;
import com.positiveapps.tipx.util.BitmapUtil.OnTransform;

/**
 * @author natiapplications
 *
 */
public class ChatMessageView implements OnClickListener,OnLongClickListener{
	
	public static final int DELETE_TYPE_ME =1;
	public static final int DELETE_TYPE_OTHER =2;
	public static final int DELETE_TYPE_BOTH =3;
	
	private Activity activity;
	private Fragment fragment;
	private ChatMessage data;
	private View view;
	private ImageView messageStatusIV;
	private ImageView typeIconIV;
	private Drawable bg;
	private int index;
	private onChatItemClickListener listener;
	private OnDeletePressedListener deleteListener;
	private String id;
	private int status;
	private PopupWindow popupMessage;
	private ProgressBar fileProgress;
	private String content;
	
	public static int[] statusImages = {R.drawable.ic_message_sent,R.drawable.ic_message_accept,R.drawable.ic_message_readed,
		R.drawable.ic_action_remove,R.drawable.x_confirm};
	public static int[] readConfirmationImage = {R.drawable.wait_to_send_confirmation,
		R.drawable.empty,R.drawable.send_read_confirmation};
	
	public ChatMessageView(Fragment fragment, ChatMessage data,int index,onChatItemClickListener listener,
			OnDeletePressedListener deleteListener) {
		super();
		this.fragment =  fragment;
		this.activity = MainActivity.mainInstance;
		this.data = data;
		this.index = index;
		this.listener = listener;
		this.id = data.getCurrentMessageID();
		this.status = data.getStatus();
		this.deleteListener = deleteListener;
		this.content = data.getContent();
		bindView();
	}


	/**
	 * 
	 */
	private void bindView() {
		
		
		switch (data.getType()) {
		case ChatMessage.TYPE_TEXT:
			if (data.getSide() == ChatMessage.SIDE_LEFT){
				view = activity.getLayoutInflater().inflate(R.layout.massage_from_text_item, null);
				bg = new BubbleMessageBoxLeft(R.color.grey1);
			}else{
				view = activity.getLayoutInflater().inflate(R.layout.message_to_text_item, null);
				bg = new BubbleMessageBoxRight(R.color.blue_3);
			}
			buildTextMessage();
			break;
		case ChatMessage.TYPE_IMAGE:
		case ChatMessage.TYPE_VIDEO:
		case ChatMessage.TYPE_AUDIO:
			if (data.getSide() == ChatMessage.SIDE_LEFT){
				view = activity.getLayoutInflater().inflate(R.layout.message_from_image_item, null);
				bg = new BubbleMessageBoxLeft(R.color.grey1);
			}else{
				view = activity.getLayoutInflater().inflate(R.layout.message_to_image_item, null);
				bg = new BubbleMessageBoxRight(R.color.blue_3);
			}
			fileProgress = (ProgressBar)view.findViewById(R.id.file_progress);
			typeIconIV = (ImageView)view.findViewById(R.id.type_icon);
			buildImageMessage();
			break;
		case ChatMessage.TYPE_LOCATION:
			if (data.getSide() == ChatMessage.SIDE_LEFT){
				view = activity.getLayoutInflater().inflate(R.layout.message_from_image_item, null);
				bg = new BubbleMessageBoxLeft(R.color.grey1);
			}else{
				view = activity.getLayoutInflater().inflate(R.layout.message_to_image_item, null);
				bg = new BubbleMessageBoxRight(R.color.blue_3);
			}
			buildLocationMessage();
			break;
		}
		
		messageStatusIV = (ImageView)view.findViewById(R.id.status_image);
		if (data.getMessagePhat() == ChatMessage.PHAT_TO){
			setMessageStatus(data.getStatus());
		}else{
			setMessageStatus(data.getReadConfirmationStatus());
		}
		
		TextView senderName = (TextView)view.findViewById(R.id.sender_name);
		senderName.setText(data.getDisplayName());
		setSenderNameTempColor(senderName);
		
		TextView date = (TextView)view.findViewById(R.id.date);
		date.setText(data.getCreated().split(" ")[1]);
		
		view.setOnClickListener(this);
		view.setOnLongClickListener(this);
		setSenderImage();
		
		
	}
	
	private void setSenderImage(){
		
		CircleImageView senderImage = (CircleImageView)view.findViewById(R.id.sender);
		if (senderImage != null){
			String picturePath = TipxApp.contactsManager.getContactImageByContactPhone(data.getSenderPhone());
			File file = null;
			if (picturePath != null && !picturePath.isEmpty()){
				file = new File(picturePath);
			}
			if (file != null&&file.exists()){
				BitmapUtil.loadImageIntoByFile(file, senderImage,
						R.drawable.ic_action_person,R.drawable.ic_action_person,
						60, 60, null);
			}else{
				BitmapUtil.loadImageIntoByUrl(picturePath, senderImage,
						R.drawable.ic_action_person,R.drawable.ic_action_person,
						60, 60, null);
			}
		}
		
	}
	
	private void setSenderNameTempColor (TextView senderName){
		
		if (data.getChatType() == ChatMessage.GROUP_MESSAGE &&
				data.getMessagePhat() == ChatMessage.PHAT_FROM&&
				ChatContainerFragment.currentChatFragment.tempColors != null){
			try {
				int color =  Color.parseColor
						(ChatContainerFragment.currentChatFragment.tempColors.get
								(TipxApp.contactsManager.getContactIdByContactPhone(data.getSenderPhone())));
				senderName.setTextColor(color);
			} catch (Exception e) {
				senderName.setTextColor(TipxApp.appContext.getResources().getColor(R.color.blue_3));
			}
		}else{
			senderName.setTextColor(TipxApp.appContext.getResources().getColor(R.color.blue_3));
		}
	}
	
	
	@SuppressWarnings("deprecation")
	private void buildImageMessage () {
		
		FrameLayout messageBox = (FrameLayout)view.findViewById(R.id.messageBox);
		messageBox.setBackgroundDrawable(bg);
		RoundedImageView imageView = (RoundedImageView)view.findViewById(R.id.image_message);
		imageView.setBackgroundResource(R.drawable.video_shape);
		
		if (data.getType() == ChatMessage.TYPE_VIDEO){
			if (data.getMessagePhat() == ChatMessage.PHAT_TO){
				if (data.isFileLoaded()){
					fileProgress.setVisibility(View.GONE);
					imageView.setImageResource(R.drawable.video_shape);
					typeIconIV.setVisibility(View.VISIBLE);
					typeIconIV.setImageResource(R.drawable.ic_action_play_over_video);
				}else{
					imageView.setImageResource(R.drawable.video_shape);
					fileProgress.setVisibility(View.VISIBLE);	
				}
			}else{
				if (fileProgress != null)
				   fileProgress.setVisibility(View.GONE);
				imageView.setImageResource(R.drawable.video_shape);
				typeIconIV.setVisibility(View.VISIBLE);
				typeIconIV.setImageResource(R.drawable.ic_action_play_over_video);
			}
		}else if (data.getType() == ChatMessage.TYPE_AUDIO){
			
			if (data.getMessagePhat() == ChatMessage.PHAT_TO){
				if (data.isFileLoaded()){
					fileProgress.setVisibility(View.GONE);
					imageView.setImageResource(R.drawable.video_shape);
					typeIconIV.setVisibility(View.VISIBLE);
					typeIconIV.setImageResource(R.drawable.audio_m);
				}else{
					imageView.setImageResource(R.drawable.video_shape);
					fileProgress.setVisibility(View.VISIBLE);	
				}
			}else{
				if (fileProgress != null)
				   fileProgress.setVisibility(View.GONE);
				imageView.setImageResource(R.drawable.video_shape);
				typeIconIV.setVisibility(View.VISIBLE);
				typeIconIV.setImageResource(R.drawable.audio_m);
			}
		}
		if (data.getType() == ChatMessage.TYPE_IMAGE){
			typeIconIV.setVisibility(View.GONE);
			int placeHolder = R.drawable.empty;
			Log.e("setImageLog", "onSetImage. path = " + data.getFilePath() + " url = " + data.getLink());
			if (data.getFilePath()!= null&&!data.getFilePath().isEmpty()&&!data.getFilePath().equalsIgnoreCase("Image")){
				Log.e("setImageLog", "load by file");
				/*BitmapUtil.loadImageIntoByFile(new File(data.getFilePath()),
						imageView, placeHolder, placeHolder, 300, 300, null);*/
				BitmapUtil.loadImageIntoBtFileUsingTransformation(new File(data.getFilePath()),
						imageView, placeHolder, placeHolder);
			}else{
				Log.e("setImageLog", "load by url");
				BitmapUtil.loadImageIntoBtUrlUsingTransformation
				(data.getLink(), imageView, placeHolder, 
						placeHolder,new OnTransform() {
							
							@Override
							public void onTransform(Bitmap source) {
								// TODO save Image Into Storage and Update imag file phat
								try {
									String imagePath = BitmapUtil.cretatStringPathForStoringMedia(data.createFileName()+".ccc");
									BitmapUtil.saveImageIntoStorage(source, imagePath);
									data.setFilePath(imagePath);
									data.setDownloadStatus(ChatMessage.DOWNLOAD_STATUS_COMPLETED);
									Log.d("setImageLog", "update message by id = " + data.getConversationId());
									TipxApp.conversationManager.updateMessageById(data.getConversationId(),
											data.getMessageId()+"", data);
									if (data.getChatType() != ChatMessage.GROUP_MESSAGE){
										TipxApp.chatManager.sendStatusMessage(TipxChatManager.ACTION_CONFIRM_DOWNLOADED,
												data.getMessageId()+"", "", !TipxApp.smackManager.isConnected());
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}
				});
			}
			
		}
	}
	
	@SuppressWarnings("deprecation")
	private void buildTextMessage () {
		LinearLayout messageBox = (LinearLayout)view.findViewById(R.id.messageBox);
		messageBox.setBackgroundDrawable(bg);
		
		TextView textView = (TextView) view.findViewById(R.id.text_message);
		textView.setText(data.getContent());

	}
	
	@SuppressWarnings("deprecation")
	private void buildLocationMessage () {
		FrameLayout messageBox = (FrameLayout)view.findViewById(R.id.messageBox);
		messageBox.setBackgroundDrawable(bg);
		
		RoundedImageView imageView = (RoundedImageView)view.findViewById(R.id.image_message);
		
		String url = "http://maps.google.com/maps/api/staticmap?center=" + data.getLat() + "," + data.getLng() +
				"&zoom=17&size=400x400&sensor=&markers=color:red%7Clabel:S%7Csize:mid%7C"+ data.getLat() + "," + data.getLng();
		BitmapUtil.loadImageIntoByUrl(url, imageView, R.drawable.ic_action_map,
				R.drawable.ic_action_map, 400, 400, null);
	}
	
	public void setMessageStatus (int status){
		Log.e("messageStatuslog", "status - " + status);
		if (data.getMessagePhat() == ChatMessage.PHAT_FROM){
			if (messageStatusIV != null){
				messageStatusIV.setImageResource(readConfirmationImage[status]);
				messageStatusIV.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (data.getReadConfirmationStatus() == ChatMessage.READ_STATUS_WAIT){
							messageStatusIV.setImageResource(readConfirmationImage[ChatMessage.READ_STATUS_CANCEL]);
							data.setReadConfirmationStatus(ChatMessage.READ_STATUS_CANCEL);
							TipxApp.conversationManager.updateMessageReadStatusById(data.getMessageId()+"", data);
						}else if (data.getReadConfirmationStatus() == ChatMessage.READ_STATUS_CANCEL){
							messageStatusIV.setImageResource(readConfirmationImage[ChatMessage.READ_STATUS_SENT]);
							data.setReadConfirmationStatus(ChatMessage.READ_STATUS_SENT);
							TipxApp.conversationManager.updateMessageReadStatusById(data.getMessageId()+"", data);
							TipxApp.chatManager.sendStatusMessage(
									TipxChatManager.ACTION_READED,
									data.getCurrentMessageID(),data.getSenderId(),true);
							if (listener != null){
								listener.onReadMessageStatusClic();
							}
						}
					}
				});
			}
			startReadTimer(fragment);
			return;
		}
		
		if (messageStatusIV != null&&status > 0){
			
			if (status > 0){
				if (status == ChatMessage.STATUS_READED_BY_RECIPIENT||status == ChatMessage.STATUS_RECIVED_IN_RECIPIENT){
					if (this.status == ChatMessage.STATUS_DELETED||this.status == ChatMessage.STATUS_CONFIRM_DELETATION){
						return;
					}
				}
				this.status = status;
				messageStatusIV.setVisibility(View.VISIBLE);
				messageStatusIV.setImageResource(statusImages[status-2]);
			}else{
				messageStatusIV.setVisibility(View.INVISIBLE);
			}
			
		}else{
			if (messageStatusIV != null){
				messageStatusIV.setVisibility(View.INVISIBLE);
			}
		}
		    
	}
	
	public ImageView getReadMessageStatusImg (){
		return this.messageStatusIV;
	}
	
	public void startReadTimer (final Fragment fragment) {
		
		if (data.getStatus() == ChatMessage.STATUS_READED_BY_RECIPIENT||
				data.getMessagePhat() == ChatMessage.PHAT_TO){
			return;
		}
		
		TipxApp.chatManager.sendStatusMessage
		(TipxChatManager.ACTION_ACCEPT_IN_USER, data.getCurrentMessageID(),data.getSenderId(),false);
		if (TipxApp.userSettings.getSendReadConfirmation()){
			if (data.getReadConfirmationStatus() == ChatMessage.READ_STATUS_WAIT) {

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						Log.d("timerlog", "run");
						if (data.getReadConfirmationStatus() != ChatMessage.READ_STATUS_CANCEL) {
							
							try {
								if (fragment != null && fragment.isVisible()){
									if (MainActivity.appSentToBackground){
										startReadTimer(fragment);
										return;
									}
									data.setReadConfirmationStatus(ChatMessage.READ_STATUS_SENT);
									TipxApp.conversationManager.updateMessageReadStatusById(data.getMessageId()+"", data);
									messageStatusIV.setImageResource(readConfirmationImage[ChatMessage.READ_STATUS_SENT]);
									TipxApp.chatManager.sendStatusMessage(
											TipxChatManager.ACTION_READED,
											data.getCurrentMessageID(),data.getSenderId(),true);
									if (data.getMessagePhat() == ChatMessage.PHAT_FROM && data.isAutoDelete()){
										data.startRemoveTimer();
									}
								}
							} catch (Exception e) {}
						}

					}
				}, 5000);
			}
		}
	}
	
	

	@Override
	public void onClick(View v) {
		try {
			ChatFragment.currentDisplayPopup.dismiss();
		} catch (Exception e) {}
		Log.e("onItemClicllog", "listener is null " + (listener != null));
		if (listener != null){
			listener.onItemClick(view, index, data);
		}
	}


	@SuppressLint("NewApi")
	@Override
	public boolean onLongClick(View v) {
		try {
			popupMessage.dismiss();
		} catch (Exception e) {}
		
	    View popupView = null;
	    if (data.getMessagePhat() == ChatMessage.PHAT_FROM){
	    	popupView = AppUtil.getAppInflater().inflate(R.layout.popup_window_left, null); 
	    }else{
	    	popupView = AppUtil.getAppInflater().inflate(R.layout.popup_window, null); 
	    }
	    		 
	    popupMessage = new PopupWindow(popupView, TipxApp.generalSettings.getScreenWidth()/2, LayoutParams.WRAP_CONTENT);  
	    popupMessage.setBackgroundDrawable(TipxApp.appContext.getResources().getDrawable(R.drawable.video_message_shape));
	    TextView deleteMe =  (TextView)popupView.findViewById(R.id.delete_me);
	    TextView deleteOther =  (TextView)popupView.findViewById(R.id.delete_other);
	    TextView deleteBoth =  (TextView)popupView.findViewById(R.id.delete_both);
	    LinearLayout divide1 =  (LinearLayout)popupView.findViewById(R.id.divide1);
	    LinearLayout divide2 =  (LinearLayout)popupView.findViewById(R.id.divid2);
	    if (data.getMessagePhat() == ChatMessage.PHAT_TO){
	    	if (data.getStatus() == ChatMessage.STATUS_CONFIRM_DELETATION/*||data.getStatus() == ChatMessage.STATUS_SENT
		    		||data.getStatus() == ChatMessage.STATUS_RECIVED_IN_SERVER*/){
				deleteBoth.setVisibility(View.GONE);
				deleteOther.setVisibility(View.GONE);
				divide1.setVisibility(View.GONE);
				divide2.setVisibility(View.GONE);
				
			}
	    }
	    OnPopupBottonClickListener popupListener = new OnPopupBottonClickListener();
	    deleteMe.setOnClickListener(popupListener);
	    deleteOther.setOnClickListener(popupListener);
	    deleteBoth.setOnClickListener(popupListener);
	    int xOfset = 50;
	    if (data.getMessagePhat() == ChatMessage.PHAT_TO){
	    	xOfset = ((TipxApp.generalSettings.getScreenWidth()/2) -50);
	    }
	    
	    int yOfset = -view.getHeight();
	    
	    popupMessage.showAsDropDown(view, xOfset, yOfset-50);
	    if (deleteListener != null){
	    	deleteListener.onPopupShowing(popupMessage);
	    }
		return true;
	}
	
	
	public void setMessageID (String  id){
		this.id = id;
	}
	
	public View getView (){
		return this.view;
	}	
	
	public String getMessageId(){
		return id;
	}
	
	public int getStatus() {
		return status;
	}

	public String getContent (){
		if (this.content != null){
			return content;
		}
		return "";
	}
	
	public ChatMessage getData(){
		return this.data;
	}
	
	
	/****************************
	 *  Inner Classes
	 ****************************/
	
	
	public interface onChatItemClickListener {
	    public boolean onItemClick (View view, int position,ChatMessage data);
	    public void onReadMessageStatusClic();
	}
	
	public interface OnDeletePressedListener {
		public void onPopupShowing (PopupWindow popupWindow);
		public void onDeletePressed (int type,View messageView,ChatMessage data,ChatMessageView chatMessageView);
	}
	
	
	
	public class OnPopupBottonClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			popupMessage.dismiss();
			int type =  0;
			switch (v.getId()) {
			
			case R.id.delete_me:
				type = DELETE_TYPE_ME;
				break;
			case R.id.delete_both:
				type = DELETE_TYPE_BOTH;
				break;
			case R.id.delete_other:
				type = DELETE_TYPE_OTHER;
				break;
			}
			if (deleteListener != null){
				deleteListener.onDeletePressed(type, view, data, ChatMessageView.this);
			}
			
		}
		
	}

}
