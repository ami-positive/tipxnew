/**
 * 
 */
package com.positiveapps.tipx.ui;

import com.positiveapps.tipx.R;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class InfoMessage {
	
	
	private Activity activity;
	private View view;
	private String info;
	private String date;
	
	public InfoMessage (Activity activity,String infoText,String date){
		this.activity = activity;
		this.info = infoText;
		this.date = date;
		buildView();
	}

	/**
	 * 
	 */
	private void buildView() {
		view = activity.getLayoutInflater().inflate(R.layout.caht_date_box, null);
		TextView dateTv = (TextView)view.findViewById(R.id.date);
		dateTv.setText(info);
		if (date != null && !date.isEmpty()){
			dateTv.setText(info + "\n" +date);
		}
	}

	/**
	 * @return the view
	 */
	public View getView() {
		return view;
	}

	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	
	

}
