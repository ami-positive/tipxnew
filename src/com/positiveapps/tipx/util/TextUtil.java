/**
 * 
 */
package com.positiveapps.tipx.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.fragments.ChatFragment;
import com.positiveapps.tipx.network.UploadMediaTask;
import com.positiveapps.tipx.objects.ChatMessage;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class TextUtil {
	
	
	public static boolean baseFieldsValidation (TextView... toValid){
		for (int i = 0; i < toValid.length; i++) {
			if (toValid[i].getText().toString().isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	public static String stringByAddingPercentEscapesUsingEncoding( String input, String charset ) throws UnsupportedEncodingException {
	    byte[] bytes = input.getBytes(charset);
	    StringBuilder sb = new StringBuilder(bytes.length);
	    for( int i = 0; i < bytes.length; ++i ) {
	        int cp = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
	        if( cp <= 0x20 || cp >= 0x7F || (
	            cp == 0x22 || cp == 0x25 || cp == 0x3C ||
	            cp == 0x3E || cp == 0x20 || cp == 0x5B ||
	            cp == 0x5C || cp == 0x5D || cp == 0x5E ||
	            cp == 0x60 || cp == 0x7b || cp == 0x7c ||
	            cp == 0x7d
	            )) {
	            sb.append( String.format( "%%%02X", cp ) );
	        }
	        else {
	            sb.append( (char)cp );
	        }
	    }
	    return sb.toString();
	}

	public static String stringByAddingPercentEscapesUsingEncoding( String input ) {
	    try {
	        return stringByAddingPercentEscapesUsingEncoding(input, "UTF-8");
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Java platforms are required to support UTF-8");
	        // will never happen
	    }
	}
	
	public static String getNumberFromString (String toParse){
		String result = "";
		for (int i = 0; i < toParse.length(); i++) {
			char temp = toParse.charAt(i);
			if (temp >='0'&&temp <='9'){
				result+=temp;
			}
		}
		return result;
	}
	
	public static String normalizePhoneNumber (String phone){
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		if (phone.contains("+")){
			
			return "+"+getNumberFromString(phone);
		}
		String clean = getNumberFromString(phone);
		PhoneNumber num = null;
		try {
		Log.e("locallog","locale = " + DeviceUtil.GetCountryID());
		   num = phoneUtil.parse(clean,  DeviceUtil.GetCountryID());
		} catch (NumberParseException e) {
		  System.err.println("NumberParseException was thrown: " + e.toString());
		  return null;
		}
		if(num == null){
			return null;
		}
		if (phoneUtil.isValidNumber(num)){
			return phoneUtil.format(num, PhoneNumberFormat.E164);
		}
		return null;
		
	}
	
	/*private class DrawItemRunnable implements Runnable {
		private ArrayList<ChatMessage> toDraw;
		private int maxItems;
		private int counter;
		public DrawItemRunnable (ArrayList<ChatMessage> toDraw){
			this.toDraw = toDraw;
			this.maxItems = toDraw.size();
			this.counter = 0;
		}
		@Override
		public void run() {
			if (maxItems == 0){
				return;
			}
			ChatMessage temp = toDraw.get(counter);
			updateScrollMessages(temp,true);
			if (temp.getType() == ChatMessage.TYPE_VIDEO||temp.getType() == ChatMessage.TYPE_AUDIO){
				if (temp.getMessageId() == 0&&temp.isUploudFileCanceled()){
					UploadMediaTask uploadMediaTask = new UploadMediaTask(ChatMessage.TYPE_VIDEO, temp.getContent(),
							getConversation().getConversationType(),
							getConversation().getConversationID(),
							ChatFragment.this);
					uploadMediaTask.uploudAsMultypart();
				}
			}
			
			counter++;
			if (counter < maxItems) {
				new Handler().postDelayed(this,5);
			}else{
				messagesContainer.setVisibility(View.VISIBLE);
				TranslateAnimation ta = new TranslateAnimation(0, 0,-TipxApp.generalSettings.getScreenHight(),0);
				ta.setDuration(200);
				ta.setInterpolator(new OvershootInterpolator());
				messagesContainer.startAnimation(ta);
			}
			
		}
		
	}*/

	public static String removeCharAt(String s, int pos) {
	 	   StringBuffer buf = new StringBuffer( s.length() - 1 );
	 	   buf.append( s.substring(0,pos) ).append( s.substring(pos+1) );
	 	   return buf.toString();
	 	}

}
