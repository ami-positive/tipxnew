package com.positiveapps.tipx.util;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.positiveapps.tipx.R;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.network.NetworkCallback;
import com.positiveapps.tipx.network.NetworkManager;
import com.positiveapps.tipx.network.ResponseObject;
import com.positiveapps.tipx.objects.ChatMessage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import android.widget.Toast;

public class ToastUtil {

	public static void toster(String message, boolean length) {
		try {
			if (length) {
				Toast.makeText(TipxApp.appContext, message, Toast.LENGTH_LONG)
						.show();
			} else {
				Toast.makeText(TipxApp.appContext, message, Toast.LENGTH_SHORT)
						.show();
			}
		} catch (Exception e) {
		}
	}

	public static void showRecordingToast(Activity activity) {

		try {
			LayoutInflater inflater = activity.getLayoutInflater();
			View layout = inflater.inflate(R.layout.recording_popup_window,
					null);

			Toast toast = new Toast(activity);
			toast.setGravity(Gravity.RIGHT|Gravity.BOTTOM, 0, 0);
			//toast.setMargin(10, 60);
		
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.setView(layout);
			toast.show();
			SoundUtil.playSound(R.raw.cancel_record_sound);
		} catch (Exception e) {}

	}
	
	
	//private boolean recordingCanceled;
		//private boolean userHoldPressedOnRecordBtn;
		//private int startX;
	    //private int startY;
	/*
	 * @Override	
	public boolean onTouch(final View view, final MotionEvent motionEvent) {

		final int X = (int) motionEvent.getRawX();
		final int Y = (int) motionEvent.getRawY();

		switch (motionEvent.getAction()) {
		case MotionEvent.ACTION_DOWN:
			startX = X;
			startY = Y;
			userHoldPressedOnRecordBtn = true;
			recordingCanceled = false;
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					if (userHoldPressedOnRecordBtn) {
						userHoldPressedOnRecordBtn = false;
						view.setSelected(true);
						SoundUtil.playSound(R.raw.record_sound);
						if (!isRecording) {
							startRecording();
							startDownCountTimer();
							cancelRecording.setVisibility(View.VISIBLE);
							animateRecordingContainer(recordContainer, 1,
									FULL_WIDTH);
						}
					}
				}
			}, 2000);
			break;
		case MotionEvent.ACTION_UP:
			userHoldPressedOnRecordBtn = false;
			view.setSelected(false);
			if (isRecording) {
				cancelRecordingIfNeeded();
				submitMediaFile(ChatMessage.TYPE_AUDIO, recordingFileName);
			}else{
				if (!recordingCanceled){
					ToastUtil.showRecordingToast(getActivity());
				}
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (Y + 50 < startY) {
				userHoldPressedOnRecordBtn = false;
				view.setSelected(true);
				cancelRecordingIfNeeded();
			}
			break;

		}
		return true;

	}*/
	
	/*
	 * 
	 * //final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
		final AsyncHttpClient request = new AsyncHttpClient();
		File myFile = new File(fileName);
		request.setMaxRetriesAndTimeout(1, 180000);
		RequestParams params = new RequestParams();
		try {
		    params.put("file", myFile);
		} catch(FileNotFoundException e) {}
		request.post(NetworkManager.SUBMIT_MEDIA_FILE+"?UserID="+TipxApp.userProfil.getUserId(), params, new JsonHttpResponseHandler(){
			
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				//DialogUtil.dismisDialog(dialog);
				try {
					Log.e("responseString", "response = " + response.toString());
					JSONObject data = response.optJSONObject("data");
					String url = data.optString("url");
					if (mediaType == ChatMessage.TYPE_VIDEO){
						TipxApp.conversationManager.removeMessageByContent(fileName);
						removeMessageViewByContent(fileName);
					}
					sendNewMessage(mediaType, "",url);
				} catch (Exception e) {
					Log.e("exceptionLog", "ex = " + e.getMessage());
					e.printStackTrace();
				}
			}
			
			@Override
			public void onRetry(int retryNo) {
				super.onRetry(retryNo);
				request.cancelRequests(getActivity(), false);
				//DialogUtil.dismisDialog(dialog);
			}
			
		});
		*/
	
	/*
	 * 
	 * public void submitMedia(final int requestType,final int mediaType,final String name,final byte[] data){
		final ProgressDialog dialog = DialogUtil.showProgressDialog(getActivity(), getString(R.string.deafult_dialog_messgae));
		TipxApp.networkManager.submitMedia(requestType,name, data, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				DialogUtil.dismisDialog(dialog);
				Log.e("medialog", "media response = " + response.getJsonContent());
				if (!isHasError){
					try {
						JSONObject data = new JSONObject(response.getData());
						String url = data.optString("url");
						sendNewMessage(mediaType, "",url);
					} catch (Exception e) {}
				}else{
					ToastUtil.toster(erroDescription, false);
				}
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				DialogUtil.dismisDialog(dialog);
			}
			
			@Override
			public void onError() {
				super.onError();
				DialogUtil.dismisDialog(dialog);
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
		});
	}*/
}