/**
 * 
 */
package com.positiveapps.tipx.util;



import com.positiveapps.tipx.R;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;



/**
 * @author natiapplications
 *
 */
public class FragmentsUtil {
	
	
	public static void addFragment(FragmentManager fm, Fragment fragment,
			int containerId) {
		FragmentTransaction transaction = fm.beginTransaction();
		transaction.replace(containerId, fragment).commit();
	}

	public static void openFragment(FragmentManager fm, Fragment fragment,
			int containerId) {

		FragmentTransaction transaction = fm.beginTransaction();
		transaction.replace(containerId, fragment).addToBackStack(null)
				.commit();
	}
	
	public static void openFragmentRghitToLeft (FragmentManager fm, Fragment fragment, int containerId){
		
		FragmentTransaction transaction = fm.beginTransaction();
		//transaction.setCustomAnimations
		//(R.anim.pop_right_in, R.anim.pop_right_out,R.anim.slide_right_in,R.anim.slide_right_out);
		transaction.replace
		(containerId,fragment).addToBackStack(null).
		commit();
	}
	
     public static void openFragmentDownToUp (FragmentManager fm, Fragment fragment, int containerId){
		
		FragmentTransaction transaction = fm.beginTransaction();
		transaction.setCustomAnimations
		(R.anim.slide_in_fragment, R.anim.slide_out_fragment,R.anim.pop_slid_in,R.anim.pop_slide_out);
		transaction.replace
		(containerId,fragment).addToBackStack(null).
		commit();
	}
	
	
	

}
