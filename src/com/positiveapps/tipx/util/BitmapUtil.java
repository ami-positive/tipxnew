package com.positiveapps.tipx.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import org.jivesoftware.smackx.pubsub.provider.ConfigEventProvider;

import wseemann.media.FFmpegMediaMetadataRetriever;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.positiveapps.tipx.MainActivity;
import com.positiveapps.tipx.TipxApp;
import com.positiveapps.tipx.network.DownloadMediaTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;








public class BitmapUtil {
	
	public static final int IMAGE_TYPE_FROM_CAMERA = 1;
	public static final int IMAGE_TYPE_FROM_GALLERAY = 2;
	/** CONSTANTS **/
	
	// name of the folder that file saved in
	private static  final String IMAGES_FLODER_NAME = "/Image";
	
	// name of the file
	private final String IMAGE_NAME = "image.jpeg";
	public static final String IMAGE_EXTENATION = "jpeg";
	
	
	/** CLASS METHODS **/


	
	@SuppressWarnings("finally")
	public static String getImageAsStringEncodedBase64(int type,Uri selectedUri, Bitmap bitmap) {
		if (bitmap == null){
			return "";
		}
		bitmap = rotate(type, selectedUri, bitmap);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
		byte[] byteArray = stream.toByteArray();
		try {
			stream.close();
		} catch (IOException e) {}finally{
			return encodeImageTobase64(byteArray);
		}
	}
	
	@SuppressWarnings("finally")
	public static String getImageAsStringEncodedBase64(Bitmap bitmap) {
		if (bitmap == null){
			return "";
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
		byte[] byteArray = stream.toByteArray();
		try {
			stream.close();
		} catch (IOException e) {}finally{
			return encodeImageTobase64(byteArray);
		}
	}
	
	@SuppressWarnings("finally")
	public static String getMediaAsStringEncodedBase64(byte[] data) {
		if (data == null){
			return "";
		}
		return encodeImageTobase64(data);
	}
	
	public  Bitmap decodeBase64(String input) 
	{
	    byte[] decodedByte = Base64.decode(input, 0);
	    if (decodedByte.length == 0){
	    	return null;
	    }
	    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}
	
	private static String encodeImageTobase64(byte[] imageBytes) {
		String test = "";
		try {  
		String encodedString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		test= "";
		Log.e("base64", encodedString);
		
			test = new String(encodedString.getBytes(),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			return "";
		}catch (Error e) {
				
				Log.e("error" , "error = " + e.getMessage());
				return "";
		}
		return test;
	}
	
	
	/*
	 * automatically generates path of the image file
	 * */
	public static Uri createUriPathForStoringImage(String imageName) {

		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		// add with image folder name
		File f = new File(root + IMAGES_FLODER_NAME);
		f.mkdirs();
		String image_fath = DateUtil.getCurrentTimeAsString("dd-MM-yyyy_hh:mm:ss") + imageName + "." + IMAGE_EXTENATION;

		// create the file after complete creating of the absolute file path
		File Image_file = new File(f, image_fath);
		
		// convert file to uri format
		Uri imageUri = Uri.fromFile(Image_file);
		return imageUri;

	}
	
	public static String cretatStringPathForStoringMedia (String fileName){
		
		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();


        File dir = new File (root + DownloadMediaTask.TIPX_DIR_NAME);
        if(dir.exists()==false) {
             dir.mkdirs();
        }
        File result = new File(dir, fileName);
        
		return result.getAbsolutePath();
	}

	public static void saveImageIntoStorage (Bitmap image,String filePath){
		 try {
		        FileOutputStream fos = new FileOutputStream(filePath);
		        image.compress(Bitmap.CompressFormat.PNG, 90, fos);
		        fos.close();
		    } catch (FileNotFoundException e) {
		        Log.d("saveImageLog", "File not found: " + e.getMessage());
		    } catch (IOException e) {
		        Log.d("saveImageLog", "Error accessing file: " + e.getMessage());
		    } 
	}
	
	
	public Bitmap loadImgaeIntoByUri(Uri uri,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
		
		// create bitmap by image uri with the option
		Bitmap image = BitmapFactory.decodeFile(uri.getPath(), options);
		if (into != null)
		    setImageInto(image, into, scale, deafult);
		return image;
	}
	
	public static Bitmap loadImgaeIntoBySelectedUri(int type,Uri selectedUri,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
				
		// create bitmap by image uri with the option
	    Bitmap image = BitmapFactory.decodeFile(getPathOfImagUri(selectedUri), options);
	    image = rotate(type, selectedUri, image);/******************************/
	    if (into != null)
		    setImageInto(image, into, scale, deafult);
	    return image;
	}
	
	public static  Bitmap loadImageIntoByStringPath(int type,String path,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
				
		// create bitmap by image uri with the option
	    Bitmap image = BitmapFactory.decodeFile(path, options);
	    image = rotate(type, Uri.fromFile(new File(path)), image);/*******************************/
	    if (into != null)
		    setImageInto(image, into, scale, deafult);
		return image;
	}
	
	
	public static Bitmap lowQualityOfImage(String path,int simpleSize) {
		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;

		// create bitmap by image uri with the option
		return BitmapFactory.decodeFile(path, options);
	}
	
	public static void setImageInto (Bitmap image,ImageView into,int scale,int deafult) {
		if (image != null){
	    	if (scale > 0){
	    		image = crupAndScale(image, scale);
	    	}
			
			into.setImageBitmap(image);
	    }else{
	    	into.setImageResource(deafult);
	    }
	}
	
	public static  Bitmap crupAndScale (Bitmap source,int scale){
		
		int factor = source.getHeight() <= source.getWidth() ? source.getHeight(): source.getWidth();
		int longer = source.getHeight() >= source.getWidth() ? source.getHeight(): source.getWidth();
		int x = source.getHeight() >= source.getWidth() ?0:(longer-factor)/2;
		int y = source.getHeight() <= source.getWidth() ?0:(longer-factor)/2;
		source = Bitmap.createBitmap(source, x, y, factor, factor);
		source = Bitmap.createScaledBitmap(source, scale, scale, false);
		return source;
		
	}
	
	public static  String getPathOfImagUri (Uri selectedUri){
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = TipxApp.appContext.getContentResolver().query(selectedUri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picturePath = cursor.getString(columnIndex);
		cursor.close();
		return picturePath;
	}
	


	/*
	 * rotate image
	 * */
	public static Bitmap rotate(int type,Uri imageUri,Bitmap image) {
		int oriantation =0;
		if (type == IMAGE_TYPE_FROM_CAMERA){
			// oriantation = getCameraPhotoOrientation(imageUri);
			return ExifUtil.rotateBitmap(imageUri.getPath(), image);
		}else if (type == IMAGE_TYPE_FROM_GALLERAY){
			 oriantation = getOrientation(imageUri);
		}
		
		Log.e("oriantationLog", "ori = " + oriantation);
		if (oriantation  <= 0){
			return image;
		}
		int w = image.getWidth();
		int h = image.getHeight();

		Matrix mtx = new Matrix();
		mtx.postRotate(oriantation);

		
		return Bitmap.createBitmap(image, 0, 0, w, h, mtx, true);
		
	}

	
	public static int getOrientation(Uri photoUri) {
	    Cursor cursor = TipxApp.appContext.getContentResolver().query(photoUri,
	            new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
	            null, null, null);

	    try {
	        if (cursor.moveToFirst()) {
	            return cursor.getInt(0);
	        } else {
	            return -1;
	        }
	    } finally {
	        cursor.close();
	    }
	}
	
	
	public static int getCameraPhotoOrientation(Uri imageUri) {
		int rotate = 0;
		try {
			TipxApp.appContext.getContentResolver().notifyChange(imageUri, null);

			ExifInterface exif = new ExifInterface(imageUri.getPath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rotate;
	}
	
	public static boolean loadFirstFrameOfVideoIntobyUri (String link,ImageView iv){
		Bitmap bmThumbnail;

		FFmpegMediaMetadataRetriever
		mmr = new FFmpegMediaMetadataRetriever();
	
		mmr.setDataSource(link);
		mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM);
		mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
		bmThumbnail = mmr.getFrameAtTime(2000000, FFmpegMediaMetadataRetriever.OPTION_CLOSEST); // frame at 2 seconds
		
		mmr.release();
		
		

		if (bmThumbnail != null) {
			bmThumbnail = ThumbnailUtils.extractThumbnail(bmThumbnail, 150, 150);
			iv.setImageBitmap(bmThumbnail);
			return true;
		}else{
			Log.e("bitmapIsNull", "bitmapISNull");
		}
		return false;
		
	}
	
	public static void getVideoFrame(String path,ImageView imageView) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(new File(Uri.parse(path).getPath()).getAbsolutePath());
            Bitmap bitmap = retriever.getFrameAtTime();
            if (bitmap != null){
            	imageView.setImageBitmap(bitmap);
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
    }
	
	public static void loadImageAndSaveItOnStorage (final String imageName,final String url,final LoadImageCallback callback){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
			Bitmap bitmap = null;
			 try {
				String fileName = cretatStringPathForStoringMedia(imageName); 
				bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
				if (bitmap == null){
					return;
				}
				FileOutputStream out = new FileOutputStream(fileName);
			    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); 
			    if (callback != null){
			    	callback.onLoadImage(true, bitmap);
			    }
			} catch (Exception e) {
				if (callback != null){
			    	callback.onLoadImage(false, bitmap);
			    }
			}
			}
		}).start();
	}
	
	
	
	public static void loadImageIntoByUrl(String url, final ImageView target, 
			final int imageForPlaceHolder, final int imageWhenError, final int newWidth, final int newHeight, 
			final Callback callback){

    	if(url == null || url.isEmpty()){
    		url = "no_image";
    	}
    	
    	final String newUrl = url;
    	
		Picasso.with(TipxApp.appContext)
		.load(newUrl)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target, new Callback() {
			
			@Override
			public void onSuccess() {
				if(callback != null){
					callback.onSuccess();
				}
				
				loadImage( newUrl, target, imageForPlaceHolder, imageWhenError, newWidth, newHeight);
			}
			
			@Override
			public void onError() {
				
			}
		});
    }
	
	public static void loadImageIntoByFile(final File file, final ImageView target, 
			final int imageForPlaceHolder, final int imageWhenError, final int newWidth, final int newHeight, 
			final Callback callback){

    	
    	
    	
		Picasso.with(TipxApp.appContext)
		.load(file)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target, new Callback() {
			
			@Override
			public void onSuccess() {
				if(callback != null){
					callback.onSuccess();
				}
				
				loadImage( file, target, imageForPlaceHolder, imageWhenError, newWidth, newHeight);
			}
			
			@Override
			public void onError() {
				
			}
		});
    }
	
	
	public static void loadOrginalImageIntoBtUrl( String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError){
		
		Picasso.with(TipxApp.appContext)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.into(target);
		
	}
	
	private static void loadImage( String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError, 
			int newWidth, int newHeight){
		
		Picasso.with(TipxApp.appContext)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target);
		
	}
	
	
	public static void loadImageIntoBtFileUsingTransformation(final File file, 
			final ImageView target,final int imageForPlaceHolder, final int imageWhenError){
		
		Picasso.with(TipxApp.appContext)
		.load(file)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.transform(new Transformation() {
			
			@Override
			public Bitmap transform(Bitmap source) {
				 int size = Math.min(source.getWidth(), source.getHeight());
				    int x = (source.getWidth() - size) / 2;
				    int y = (source.getHeight() - size) / 2;
				    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
				  
				    if (result != source) {
				      source.recycle();
				    }
				    
				    return result;
			}
			
			@Override
			public String key() {
				
				return "square()";
			}
		}).into(target,new Callback() {
			
			@Override
			public void onSuccess() {
				loadImageByFileUsingTrans(file, target, imageForPlaceHolder, imageWhenError);
			}
			
			@Override
			public void onError() {}
		});
		
	}
	
	private static void loadImageByFileUsingTrans( File file, 
			ImageView target, int imageForPlaceHolder, int imageWhenError){
		
		Picasso.with(TipxApp.appContext)
		.load(file)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.transform(new Transformation() {
			
			@Override
			public Bitmap transform(Bitmap source) {
				 int size = Math.min(source.getWidth(), source.getHeight());
				    int x = (source.getWidth() - size) / 2;
				    int y = (source.getHeight() - size) / 2;
				    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
				   // Bitmap result = crupAndScale(source, 150);
				    
				    if (result != source) {
				    	
				      source.recycle();
				    }
				    return result;
			}
			
			@Override
			public String key() {
				
				return "square()";
			}
		}).into(target);
		
	}
	
	public static void loadImageIntoBtUrlUsingTransformation(final String url, 
			final ImageView target,final int imageForPlaceHolder, final int imageWhenError,
			final OnTransform callback){
		
		Picasso.with(TipxApp.appContext)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.transform(new Transformation() {
			
			@Override
			public Bitmap transform(Bitmap source) {
				 int size = Math.min(source.getWidth(), source.getHeight());
				    int x = (source.getWidth() - size) / 2;
				    int y = (source.getHeight() - size) / 2;
				    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
				    if (callback != null){
				    	  callback.onTransform(source);
				    }
				    if (result != source) {
				      source.recycle();
				    }
				    
				    return result;
			}
			
			@Override
			public String key() {
				
				return "square()";
			}
		}).into(target,new Callback() {
			
			@Override
			public void onSuccess() {
				loadImageUsingTrans(url, target, imageForPlaceHolder, imageWhenError,callback);
			}
			
			@Override
			public void onError() {}
		});
		
	}
	
	
	
	private static void loadImageUsingTrans( String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError
			,final OnTransform callback){
		
		Picasso.with(TipxApp.appContext)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.transform(new Transformation() {
			
			@Override
			public Bitmap transform(Bitmap source) {
				 int size = Math.min(source.getWidth(), source.getHeight());
				    int x = (source.getWidth() - size) / 2;
				    int y = (source.getHeight() - size) / 2;
				    Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
				   // Bitmap result = crupAndScale(source, 150);
				    
				    if (result != source) {
				    	
				      source.recycle();
				    }
				    return result;
			}
			
			@Override
			public String key() {
				
				return "square()";
			}
		}).into(target);
		
	}
	
	private static void loadImage( File file, 
			ImageView target, int imageForPlaceHolder, int imageWhenError, 
			int newWidth, int newHeight){
		
		Picasso.with(TipxApp.appContext)
		.load(file)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target);
		
	}
	
	
	public static byte[] getMediaBytesDataByUri(Uri uri)  {
	       
		InputStream inputStream = null;
		ByteArrayOutputStream byteBuffer = null;
		try {
			inputStream = TipxApp.appContext.getContentResolver().openInputStream(uri);

			byteBuffer = new ByteArrayOutputStream();
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
				byteBuffer.write(buffer, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}catch (OutOfMemoryError error){
			return null;
		}
		return byteBuffer.toByteArray();
     }
	
	
	public interface LoadImageCallback {
		public void onLoadImage (boolean success,Bitmap image);
	}

	public interface OnTransform {
		public void onTransform(Bitmap source);
	}
}
