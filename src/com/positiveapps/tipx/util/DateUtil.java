/**
 * 
 */
package com.positiveapps.tipx.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.util.Log;

/**
 * @author Nati Gabay
 *
 */
public class DateUtil {
	
	
	private static String[] months = {"January","February","March","April",
		"May","June","July","August","September","October","November","December"};
	
	
	
	public static String getCurrentTimeAsString (String format) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}
	
	public static long getCurrentDateInMilli () {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTimeInMillis();
	}
	
	public static long getJastDateInMilli () {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTimeInMillis();
	}
	
	public static String getCurrentTimeAsServerString () {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		Log.e("timelog", "currentTime = " + calendar.getTimeInMillis());
		return sdf.format(calendar.getTime());
	}
	
	public static String getDateAsStringByMilli (String dilimeter,long milli,boolean time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf= null;
		if (time){
			sdf = new SimpleDateFormat("dd"+dilimeter+"MM"+dilimeter+"yyyy");
		}else{
			sdf = new SimpleDateFormat("dd"+dilimeter+"MM"+dilimeter+"yyyy HH:mm:ss" );
		}
		
		return sdf.format(calendar.getTime());
	}
	
	public static String getTimeAsStringByMilli (long milli,String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(calendar.getTime());
	}
	
	
	
	public static String getMonthName (int month){
		return months[month-1];
	}
	
	public static String getTimePickerAsString (int hour, int mintue){
		String h = hour+"";
		String m = mintue+"";
		if (hour < 10){
			h = "0"+h;
		}
		if (mintue < 10){
			m = "0"+m;
		}
		String time = h + ":" + m;
		return time;
	}
	
	public static String getDatePickerAsString (int day, int month, int yeers){
		String d = day+"";
		String m = month+"";
		String y = yeers+"";
		if (day < 10){
			d = "0" + d;
		}
		if (month < 10){
			m = "0"+ m;
		}
		String date = d+"/"+m+"/"+y;
		return date;
	}
	
	public static long getDatePickerInMilli(int day, int month, int yeers){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yeers);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		
		return calendar.getTimeInMillis();
	}
	
	
	public static long getTimePickerInMilli(int hower, int seconds){
		long h = hower*360000;
		long s = seconds*60000;
		return h+s;
	}
	
	public static int[] getBestDate (String date){
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		String curretnSetDate[] = date.split("/");
		if (curretnSetDate.length == 3){
			year = Integer.parseInt(curretnSetDate[2]);
			month = Integer.parseInt(curretnSetDate[1])-1;
			day = Integer.parseInt(curretnSetDate[0]);
		}
		
		int[] result = {day,month,year};
		return result;
	}
	
	public static int[] getBestTime (String time){
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		
		String[] curretnSetTime = time.split(":");
		if (curretnSetTime.length == 2){
			hour = Integer.parseInt(curretnSetTime[0]);
			minute = Integer.parseInt(curretnSetTime[1]);
		}
		int[] result = {hour,minute};
		return result;
	}
	
	
	
	public static String getDateDescriptionByDate (Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String month = months[calendar.get(Calendar.MONTH)];
		
		return month + " " + calendar.get(Calendar.DAY_OF_MONTH)
				+ "." + calendar.get(Calendar.YEAR);
	}
	
	public static String getRangBetweenTwoDatesDescription (Date date1,Date date2) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

		String from = sdf.format(date1);
		String to = sdf.format(date2);
		return from + " - " + to;
	}
	
	public static String getRangBetweenTwoDates (Date date1,Date date2) {
		long from = date1.getTime();
		long to = date2.getTime();
		long disTance = to - from;
		if (disTance <=0){
			return String.valueOf(0);
		}
		long result = disTance/60000;
		return String.valueOf(result);
	}
	
	public static long getDateMillisByServerDate(String serverDate,boolean withTime) {
		long result = 0;
		Calendar calendar = Calendar.getInstance();
		String[] date = serverDate.split(" ");
		if (date.length > 0){
			String[] jastDate = date[0].split("-");
			if (jastDate.length >=3){
				int years = Integer.parseInt(jastDate[0]);
				int munts = Integer.parseInt(jastDate[1]);
				int days = Integer.parseInt(jastDate[2]);
				calendar.set(years, munts-1, days);
			}
			if (withTime){
				if (date.length > 1){
					String[] jasttime = date[1].split(":");
					if (jasttime.length >= 3){
						int h = Integer.parseInt(jasttime[0]);
						int m = Integer.parseInt(jasttime[1]);
						int s = Integer.parseInt(jasttime[2]);
						calendar.set(Calendar.HOUR, h);
						calendar.set(Calendar.MINUTE, m);
						calendar.set(Calendar.SECOND, s);
					}
				}
			}
			
			result = calendar.getTimeInMillis();
		}
		return result;
	}
	
	public static String formatServerDate(String serverDate) {
		String result = "";
		boolean justTime = false;
		
		Calendar calendar = Calendar.getInstance();
		int dayOfMunth = calendar.get(Calendar.DAY_OF_MONTH);
		calendar.clear();
		String[] date = serverDate.split(" ");
		if (date.length > 0){
			String[] jastDate = date[0].split("-");
			if (jastDate.length >=3){
				int years = Integer.parseInt(jastDate[0]);
				int munts = Integer.parseInt(jastDate[1]);
				int days = Integer.parseInt(jastDate[2]);
				calendar.set(years, munts-1, days);
				if (days == dayOfMunth){
					justTime = true;
				}
			}
			
			if (date.length > 1) {
				String[] jasttime = date[1].split(":");
				if (jasttime.length >= 3) {
					int h = Integer.parseInt(jasttime[0]);
					int m = Integer.parseInt(jasttime[1]);
					int s = Integer.parseInt(jasttime[2]);
					calendar.set(Calendar.HOUR, h);
					calendar.set(Calendar.MINUTE, m);
					calendar.set(Calendar.SECOND, s);
				}
				
			}
			
			if (justTime){
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				sdf.setTimeZone(TimeZone.getDefault());
				result = sdf.format(calendar.getTime());
			}else{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM - HH:mm");
				sdf.setTimeZone(TimeZone.getDefault());
				result = sdf.format(calendar.getTime());
			}
			
		}
		return result;
	}
	
	
	public static String formatServerDateWithTime(String serverDate) {
		String result = "";
		
		SimpleDateFormat serverTime = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		try {
			serverTime.setTimeZone(TimeZone.getTimeZone("GMT+0"));
			Date dataFormat = serverTime.parse(serverDate);
			serverTime.setTimeZone(TimeZone.getDefault());
			result = serverTime.format(dataFormat);
			
		} catch (ParseException e) {
		}
		return result;
	}
	
	public static String formatServerDateForLastSeen(String serverDate) {
		String result = "";
		
		SimpleDateFormat serverTime = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		try {
			serverTime.setTimeZone(TimeZone.getTimeZone("GMT+0"));
			Date dataFormat = serverTime.parse(serverDate);
			serverTime.setTimeZone(TimeZone.getDefault());
			String newFormat = "HH:mm";
			Calendar d = Calendar.getInstance();
			d.setTime(dataFormat);
			if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) != d.get(Calendar.DAY_OF_MONTH)){
				newFormat = "dd/MM - HH:mm";
			}
			serverTime.applyPattern(newFormat);
			result = serverTime.format(dataFormat);
			
		} catch (ParseException e) {
		}
		return result;
	}
	
	
	public static int getDaysOfServerDate(String serverDate) {
		int result = 0;
		String[] date = serverDate.split(" ");
		if (date.length > 0){
			String[] jastDate = date[0].split("-");
			result = Integer.parseInt(jastDate[2]);
		}
		return result;
	}
	
	
	public static String getCurrentTimeAsServerStringGMT_0 () {
		Calendar calendar = Calendar.getInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		return sdf.format(calendar.getTime());
	}
	
	
	
	

}
